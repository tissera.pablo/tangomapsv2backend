const express = require('express');
let app = express();
const baseURL = '/api/v1';

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get(`${baseURL}/map/getAll`, function(req, res) {
    const result = { "type": "FeatureCollection",
"features": [{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tutti Frutti</strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 20:30 - 01:00<br /><strong>price:</strong> 7&euro;<br /><strong>contact:</strong> phoen: 0677559475 - email: info@tanguediaparis.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397726,48.85325],
"type": "Point"
},
"id": "00fd6ff729e860b62d282cfca9e4fe1b"
},
{
"type": "Feature",
"properties": {
"description": "<strong>San Jeronimo</strong><p><strong>address:</strong> San Jeronimo y Obispo Salguero, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.179227,-31.418669],
"type": "Point"
},
"id": "0254ab9e2c95b59097518011e0097af4"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pause Tango Milonga</strong><p><strong>address:</strong> 26 rue Fran?ois MAURIAC 13010 Marseille <br /><strong>time:</strong> 21:00 - 02:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0667430235 - email: pause.tango@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.417415,43.275937],
"type": "Point"
},
"id": "0697284f66ae4aee80f9423d3a014ab2"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Yapa</strong><p><strong>address:</strong> Carrer Valencia 234, Barcelona, Barcelona 08007, Spain<br /><strong>time:</strong> 22:30 - 01:30<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> - email: milongadelcafe@hotmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.160256,41.390259],
"type": "Point"
},
"id": "075b04d893955ef258752c46b3edc598"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Rayuela</strong><p><strong>address:</strong> Laprida 453, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.192579,-31.421498],
"type": "Point"
},
"id": "0833e6077c0d4b1eabc7259e5e49a722"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Argentino e.V.</strong><p><strong>address:</strong> Schmidtstedter Str. 34, 99084 Erfurt<br /><strong>time:</strong> 22:00 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: +49-(0)361-26 28 93 25</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [11.0401,
50.974206],
"type": "Point"
},
"id": "09a06801c93b87fa08ddd6abad5dfda3"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga tu el Cielo y tu</strong><p><strong>address:</strong> 76 avenue du Docteur Fleming 13500 Martigues<br /><strong>time:</strong> 21:00 - 02:00<br /><strong>price:</strong> 12&euro;<br /><strong>contact:</strong> phone: 0614422964 - email: tangogeraldine@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.057347,43.414284],
"type": "Point"
},
"id": "0a748e82061f64818684dde179c4dc1f"
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Aljibe</strong><p><strong>address:</strong> Avenida Castro Barros 940, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.199846,-31.396788],
"type": "Point"
},
"id": "0a7b9ebf57979b4251938e7348358f5b"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Contradanza XXL</strong><p><strong>address:</strong> 21 rue Boyer 75020 Paris<br /><strong>time:</strong> 20:00 - 00:00<br /><strong>price:</strong> 15&euro;<br /><strong>contact:</strong> phone: 0681792644 - email: contact@geomuse.fr</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.392159,48.868414],
"type": "Point"
},
"id": "0bcf4052808286db4af86cc277719906"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique de la Sablonniere</strong><p><strong>address:</strong> 62 rue Cambronne 75015 Paris<br /><strong>time:</strong> 11:45 to 13:45<br /><strong>price:</strong> 3&euro;<br /><strong>contact:</strong> phone: 0660784330 - email: tangosable@gmail.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.302049,48.843961],
"type": "Point"
},
"id": "109c4e118acbc9c4eac5d84c1e474273"
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Desbande</strong><p><strong>address:</strong> Mare de D&eacute;u dels Desamparats, 5 - 2nd floor<br /><strong>time:</strong> 23:00 - 02:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 635 803 375 - - email: info@barnatango.com - website:  www.barnatango.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.159464,41.401456],
"type": "Point"
},
"id": "134987d584e1d483e9936a3da3e122f1"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga el Colectivo</strong><p><strong>address:</strong> 46 rue des Rigoles 75020 Paris<br /><strong>time:</strong> 19:30 - 02:00<br /><strong>price:</strong> 10&euro;<br /><strong>contact:</strong> phone: 0663233570 / 0679013958 - email: milongalecolectivo@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.392347,48.872705],
"type": "Point"
},
"id": "13e5b4d811b5a29268c81535a50331a3"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Beltango</strong><p><strong>address:</strong> Kaulenberg 6, 06108 Halle<br /><strong>time:</strong> 21:00 - 00:00<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0162 293 79 74</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [11.968942,
51.487157],
"type": "Point"
},
"id": "14a5cdd0cde30aa5f7c77e4487a9f931"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Dej De La Defense</strong><p><strong>address:</strong> 112 rue de Rennes<br /><strong>time:</strong> 12:45 to 14:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> salsatango.fr</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.237889,48.89129],
"type": "Point"
},
"id": "1b10c6044404d46f6225c0cc39609468"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tres Tangos</strong><p><strong>address:</strong> Dornbl?thstra?e 16, 01277 Dresden<br /><strong>time:</strong> 21:00 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: (0351) 3116789</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [13.800186,
51.045382],
"type": "Point"
},
"id": "2185454949e447740b2071c0b8879167"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Katedral</strong><p><strong>address:</strong> 219 Boulevard Macdonald 75019 Paris<br /><strong>time:</strong> 20:00 - 23:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 0680338851</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.371858,48.898726],
"type": "Point"
},
"id": "27672472d878e1d05fb822d310a7fbe8"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cafe Tango</strong><p><strong>address:</strong> 51 rue Edouard Vaillant 93100 Montreuil<br /><strong>time:</strong> 13:00 - 18:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0614053178 - email: lechantier.tangofever@gmail.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.432117,48.854255],
"type": "Point"
},
"id": "28a808ddc05e38868a4f52b4f8027b1b"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratica du 1er Mercredi Ouvre</strong><p><strong>address:</strong> 10 villa Riberolle 75020 Paris<br /><strong>time:</strong> 20:00 - 22:00<br /><strong>price:</strong> 3&euro;<br /><strong>contact:</strong> phone: 0630083500 - email: assotango@gmail.com</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.396932,48.857764],
"type": "Point"
},
"id": "2aacb330c6b81bf038c9f353f1790455"
},
{
"type": "Feature",
"properties": {
"description": "<strong>TangoBar</strong><p><strong>address:</strong> Avinguda del Marqu?s de l'Argentera 27, Barcelona, Barcelona 08003, Spain (downstairs)<br /><strong>time:</strong> 22:00 - 02:00<br /><strong>price:</strong> 7&euro;<br /><strong>contact:</strong> phone: +34 688 302 594 ? - email: susurro.barcelona@gmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.185443,41.385472],
"type": "Point"
},
"id": "2b263e944a557e22409681d9cd307da4"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique des Planches</strong><p><strong>address:</strong> 2 rue Mozart 38000 Grenoble<br /><strong>time:</strong> 20:00 - 23:00<br /><strong>price:</strong> 4&euro;<br /><strong>contact:</strong> phone: 0607152141 - email: pratiquelesplanches@gmail.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.709346,45.186416],
"type": "Point"
},
"id": "2cf6bb96ca8f35640c430d83d65128fd"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Brujita Nocturna</strong><p><strong>address:</strong> 8 rue du lieutenant Meschi 13005 Marseille<br /><strong>time:</strong> 21:30 - 02:00<br /><strong>price:</strong> 10&euro;<br /><strong>contact:</strong> phoen: 0603030766 - email: pascal_botella@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.401705,43.29519],
"type": "Point"
},
"id": "2e3329158e4ce54fe0ac672950ddcf0f"
},
{
"type": "Feature",
"properties": {
"description": "<strong>MilOnda 1905</strong><p><strong>address:</strong> 46 rue des Rigoles 75020 Paris<br /><strong>time:</strong> 21:00 - 02:30<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0679013958 - email: milonda1905@gmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.392376,48.872725],
"type": "Point"
},
"id": "2f1cf23dd631ff83dfb5a963fe5ce0ff"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Tango Cha</strong><p><strong>address:</strong> 112 rue de Rennes 75006 Paris<br /><strong>time:</strong> 12:15 to 14:15<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0786510199 - email: danserletango@gmail.com</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.327521,48.84786],
"type": "Point"
},
"id": "33bc703c7e11dacdda20e2dcbda14a1a"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Corrientes y Gambettes</strong><p><strong>address:</strong> 4 parvis Corentin Celton 92130 Issy les Moulineaux<br /><strong>time:</strong> 18:00 - 20:00<br /><strong>price:</strong> 2&euro;<br /><strong>contact:</strong> phone: 0662748301 - email: beltango@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.279489,48.827826],
"type": "Point"
},
"id": "37113f30f6d5fa1d41c2ad9c1a262ff0"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Caminos de Tango</strong><p><strong>address:</strong> 20 place St Bruno 38000 Grenoble<br /><strong>time:</strong> 19:30 - 00:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: 0689067937 - email: contact@caminosdetango</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.712492,45.187609],
"type": "Point"
},
"id": "396843270e4aee8c31f93311f1c852c2"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Queer la Fabrica</strong><p><strong>address:</strong> 8 rue du lieutenant Meschi 13005 Marseille<br /><strong>time:</strong> 20:30 - 02:00<br /><strong>price:</strong> 9&euro;<br /><strong>contact:</strong> phone: 0603030766 - email: pascal_botella@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.401705,43.295147],
"type": "Point"
},
"id": "39d9890c2cf4029882436ef9162140fe"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Week End Tchikiboum Tango</strong><p><strong>address:</strong> 138 Cours Berriat 38000 Grenoble<br /><strong>time:</strong> 21:00 - 23:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0650702740 - email: tchikiboumtango@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.708795,45.188715],
"type": "Point"
},
"id": "39f952fb82f3618b265792ba901ea83d"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Juntada Milonguera</strong><p><strong>address:</strong> Plaza Colon, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.195684,-31.40929],
"type": "Point"
},
"id": "3a15ae7c3193640d3dbcc7089f84b763"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Le Loft</strong><p><strong>address:</strong> 6 rue Lechapelais 75017 Paris<br /><strong>time:</strong> 22:30 - 02:00<br /><strong>price:</strong> 4&euro;<br /><strong>contact:</strong> - email: ariane@tangoart.fr</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.325581,48.886045],
"type": "Point"
},
"id": "3e746d88504c4680c3543dd2e54b3620"
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Huracan</strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 20:00 - 05:00<br /><strong>price:</strong> 7/8/10&euro;<br /><strong>contact:</strong> phone: 0662707562 - email: abrasens@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397734,48.853241],
"type": "Point"
},
"id": "400e56ae767e0d86311635aa8c6cea6d"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Corazoneando</strong><p><strong>address:</strong> San Luis 410, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.192565,-31.421504],
"type": "Point"
},
"id": "417c7f9622e503bdd1e5e8849e5d5d13"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Retro Boom Boom</strong><p><strong>address:</strong> 18 rue du Faubourg du Temple 75011 Paris<br /><strong>time:</strong> 20:30 - 23:30<br /><strong>price:</strong> 4&euro;<br /><strong>contact:</strong> phone: 0664801625 - email: pratique.du.lundi@gmail.com</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.365975,48.868417],
"type": "Point"
},
"id": "4315727da9beb72819ae2d3ed004fb55"
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Corazon des Abbesses</strong><p><strong>address:</strong> 22 rue Andre Antoine 75018 Paris<br /><strong>time:</strong> 19:00 - 00:00<br /><strong>price:</strong> 10&euro;<br /><strong>contact:</strong> phone: 0664070543 - email: laboratorioc@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.337627,48.884202],
"type": "Point"
},
"id": "462dfef1a59c86f904c1444b7d6dd2ee"
},
{
"type": "Feature",
"properties": {
"description": "<strong>danceTLV Practica</strong><p><strong>address:</strong> 4th floor, Dizengoff 98, Tel Aviv-Yafo<br /><strong>time:</strong> 22:00 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> ?</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.774069,
32.07898],
"type": "Point"
},
"id": "485a8f6803016d219756daa1215b0c97"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tuesday Practilonga</strong><p><strong>address:</strong> HaRav Kosovsky St 69, Tel Aviv-Yafo<br /><strong>time:</strong> 21:30 - 00:30<br /><strong>price:</strong> 25 NIS<br /><strong>contact:</strong> ?</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.803286,
32.097759],
"type": "Point"
},
"id": "48c52b38dadf6660a3d47535cf0aea13"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Abrazo Tango</strong><p><strong>address:</strong> 105 rue de Tolbiac 75013 Paris<br /><strong>time:</strong> 15:00 - 20:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0145841587 - email: intensive.danse@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.364692,48.826609],
"type": "Point"
},
"id": "4ad8bac70fa7a5443339b21cf0d41214"
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Living</strong><p><strong>address:</strong> Carrer de Pallars 65-7, barcelona (Attic 4th floor - A)<br /><strong>time:</strong> 22:00 - 02:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 699 32 70 26 ? - email: martinyandreatango@gmail.com - website: martinyandreatango.blogspot.com.es - NOTE: only 2nd and last Friday of month</p>",
"business":"milonga",
"day": "Friday", 
"frequency": "twice a month"
},
"geometry": {
"coordinates": [2.203817,41.405693],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Chat lo Ves Afterwork</strong><p><strong>address:</strong> 12 avenue Jean Aicard 75011 Paris<br /><strong>time:</strong> 19:00 - 02:00<br /><strong>price:</strong> 10&euro<br /><strong>contact:</strong> - email: yory7@yahoo.com phone: 0698255749</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.381479,48.865267],
"type": "Point"
},
"id": "4ef96f01f333d8f03dfd0896130aa667"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Cambalache</strong><p><strong>address:</strong> 125 rue du Chemin Vert 75011<br /><strong>time:</strong> 19:00 - 22:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0610268467 - email: atelierdiscepolin@gmail.com</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.383734,48.862039],
"type": "Point"
},
"id": "4ffb3656662f42832c640ec91f102d31"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Silvia's Practica</strong><p><strong>address:</strong> Shalma Rd 46, Tel Aviv-Yafo, 66073<br /><strong>time:</strong> 22:00 - 01:30<br /><strong>price:</strong> 30 NIS<br /><strong>contact:</strong> phone: 0547206286 - email: silviar@bezeqint.net</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.767395,
32.054885],
"type": "Point"
},
"id": "5659f0bced64dc3c1edc55936330ee7b"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Virutita</strong><p><strong>address:</strong> Carrer de Roger 25/27, Barcelona, Barcelona 08028, Spain (basement A)<br /><strong>time:</strong> 19:00 - 01:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 647 388 133 ? - email: escuelaelalmacen@gmail.com - website: www.domingorey.es</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.12634,41.376601],
"type": "Point"
},
"id": "567fb874de3aeb197581628869cb6247"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Yumba</strong><p><strong>address:</strong> Carrer de Cal?bria 89, Barcelona, Barcelona 08015, Spain<br /><strong>time:</strong> 22:30 - 02:00<br /><strong>price:</strong> 7&euro;<br /><strong>contact:</strong> phone: +34 626 107 415 ?email: infotangos@gmail.com - website: www.layumbatango.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.155824,41.378954],
"type": "Point"
},
"id": "57b2e6bf0ab3121e8849cd4de6663152"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Le Caminito</strong><p><strong>address:</strong> 1 ter rue deguerry 75011 Paris<br /><strong>time:</strong> 19:00 - 00:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0617168826 - email: studio18paris@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.372476,48.868459],
"type": "Point"
},
"id": "58c2d65690cb3ccbfecdac07d8805631"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Der gem?tliche Freitag</strong><p><strong>address:</strong> Juri-Gagarin-Ring 116, 99084 Erfurt<br /><strong>time:</strong> 20:30 - 00:30<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> - email: daniel@dos-abrazos.de</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [11.038943,
50.976647],
"type": "Point"
},
"id": "59e0b32b223e04230dede3122a857f3e"
},
{
"type": "Feature",
"properties": {
"description": "<strong>L un Des Sens Tango en Bal </strong><p><strong>address:</strong> 48 rue du Cardinal Lemoine 75005 Paris<br /><strong>time:</strong> 21:30 - 00:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 0687012125 - email: yannicktango@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.351314,48.846524],
"type": "Point"
},
"id": "5a752b5b1a7cc7fdd997049452c1ed3f"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Ramat Gan</strong><p><strong>address:</strong> Sderot HaYeled 8, Ramat Gan<br /><strong>time:</strong> 22:30 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0543015212</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.81348,
32.086042],
"type": "Point"
},
"id": "5abdb81ba354c756367e3d3f0e4760f1"
},
{
"type": "Feature",
"properties": {
"description": "<strong>WeTango? Ronen & Dorit Practica</strong><p><strong>address:</strong> Carlebach 2, Tel Aviv-Yafo<br /><strong>time:</strong> 22:00 - 24:00<br /><strong>price:</strong> 25 NIS<br /><strong>contact:</strong> facebook: WeTango Ronen & Dorit</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.783886,
32.066837],
"type": "Point"
},
"id": "5e030f3c204c802de462c146d5ff4ad5"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga la Nocturna</strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 21:30 - 02:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0140407360  - email: message@lacasadeltango.net  website: lacasadeltango.net</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.39775,48.853254],
"type": "Point"
},
"id": "5e39113e6eaceaf787d5f5cb7b2e0f18"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Des Lucioles</strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 19:00 - 01:00<br /><strong>price:</strong> 7&euro;<br /><strong>contact:</strong> phone: 0677559475 - email: info@tanguediaparis.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397701,48.853243],
"type": "Point"
},
"id": "6086c7de6b04d0df9b8e63699786956d"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga en Tus Brazos</strong><p><strong>address:</strong> 25 Bis Avenue de la Republique 92120 Montrouge<br /><strong>time:</strong> 20:30 - 01:00<br /><strong>price:</strong> 9&euro;<br /><strong>contact:</strong> phone: 0661211865 - email: entusbrazos@artsessentiels.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.320811,48.820245],
"type": "Point"
},
"id": "62a6d7034f15cd3a53f06d3e394488d5"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milongon</strong><p><strong>address:</strong> 1 passage du buisson Saint Louis 75010 Paris<br /><strong>time:</strong> 19:30 - 01:00<br /><strong>price:</strong> 12&euro;<br /><strong>contact:</strong> phone: 0610777107 - email: conciertoypractica.chico@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.372683,48.872121],
"type": "Point"
},
"id": "6448750e36c0fd65472e31c8993e32ea"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Domingo al Sur</strong><p><strong>address:</strong> 5 rue du Moulin Vert 75014 Paris<br /><strong>time:</strong> 15:00 - 20:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0662110522 - email: contact@tangoaparis.fr</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.325686,48.829158],
"type": "Point"
},
"id": "66c54676b969bfb1f34faea7bdb39519"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Jerusalem Tango ZIVIA & Shlomo</strong><p><strong>address:</strong> HaUman St 9, Jerusalem<br /><strong>time:</strong> 22:00 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0544785509</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [35.211537,
31.74828],
"type": "Point"
},
"id": "66d00fa046f26398e0cc472021adab1f"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Trio Tango a Varenne</strong><p><strong>address:</strong> 20 rue de varenne 75007 Paris<br /><strong>time:</strong> 15:00 - 20:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0623480316</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.325489,48.853636],
"type": "Point"
},
"id": "66fee3d77c151a755c23a5989aebf6a9"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique la Ruche</strong><p><strong>address:</strong> 17 avenue de la Republique 94800 Villejuif<br /><strong>time:</strong> 22:15 to 01:00<br /><strong>price:</strong> 4&euro;<br /><strong>contact:</strong> phone: 0619864002 - email: contact@tango-argentin.org</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.364732,48.788455],
"type": "Point"
},
"id": "6774c701b75f95078fcd8a19817ba958"
},
{
"type": "Feature",
"properties": {
"description": "<strong>YAPA Practica</strong><p><strong>address:</strong> Beit Alfa St 13, Tel Aviv-Yafo<br /><strong>time:</strong> 22:00 - 01:00<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: +972 54-598-4737 - email: yapatangoclubtlv@gmail.com</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.789084,
32.065582],
"type": "Point"
},
"id": "6c5e7e0bc21e2935084b49458c3fe5a9"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Au Pavillon</strong><p><strong>address:</strong> 54 rue Gabriel Peri 93200 Saint Denis<br /><strong>time:</strong> 19:30 - 23:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 0613825263 - email: christine7693@gmail.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.355879,48.933999],
"type": "Point"
},
"id": "704d404bebeebe223b2fdc4cf622553c"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cafe Tango</strong><p><strong>address:</strong> 51 rue Edouard Vaillant 93100 Montreuil<br /><strong>time:</strong> 13:00 - 18:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0614053178 - email: lechantier.tangofever@gmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.432105,48.854226],
"type": "Point"
},
"id": "73fb1cfe23eaf9dd2615c2567325a442"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Mordida</strong><p><strong>address:</strong> 1 rue Fleury 75018 Paris<br /><strong>time:</strong> 18:00 - 21:30<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: 0788816747 - email: veronique@mordidadetango.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.354046,48.884342],
"type": "Point"
},
"id": "7688a8154a23c1ed8ec69309aa68b12d"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Tango Ostinato</strong><p><strong>address:</strong> 16 rue Geoffroy L'Asnier 75004 Paris<br /><strong>time:</strong> 20:00 - 22:00<br /><strong>price:</strong> 5 / 0&euro;<br /><strong>contact:</strong> phone: 0648770042 - email: administration@tango-ostinato.com</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.356232,48.854545],
"type": "Point"
},
"id": "7877d92d582f9faf38b41699aefcb37b"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Tango Cha</strong><p><strong>address:</strong> 112 rue de Rennes<br /><strong>time:</strong> 14:15 to 16:15<strong>price</strong> 5<br /><strong>contact:</strong> phone: 0786510199 - email: danserletango@gmail.com website: danserletango.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.32752,48.84786],
"type": "Point"
},
"id": "7881255890225380a03bdf42f18b72c3"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Josep y Teresa</strong><p><strong>address:</strong> Carrer de Roger 25/27, Barcelona, Barcelona 08028, Spain (basement A)<br /><strong>time:</strong> 19:00 - 01:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 605 993 415 ? - email: tangojosepyteresa@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.12634,41.376601],
"type": "Point"
},
"id": "7a93f46d580dfdfc4bcaa9ed6d974c3a"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique La Sourdiere</strong><p><strong>address:</strong> 23 rue de la Sourdi?re 75001 Paris<br /><strong>time:</strong> 17:00 - 19:45<br /><strong>price:</strong> 4/5&euro;<br /><strong>contact:</strong> phone: 0676486378 - email: contact@letempsdutango.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.332498,48.866457],
"type": "Point"
},
"id": "7ce03c8e9803ba470138433278112ce3"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Casa del Tango</strong><p><strong>address:</strong> 11 all?e Darius Milhaud 75019 Paris<br /><strong>time:</strong> 16:00 - 19:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: 0140407360  - email: message@lacasadeltango.net  website: lacasadeltango.net</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.38573,48.883694],
"type": "Point"
},
"id": "7d332edaac512a6c648e8c768022b646"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Le Latina</strong><p><strong>address:</strong> 25 Bis Avenue de la R?publique 92120 Montrouge<br /><strong>time:</strong> 20:30 - 02:00<br /><strong>price:</strong> 10&euro;<br /><strong>contact:</strong> phone: 0644867630 - email: tangotango2_4@yahoo.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.320809,48.820243],
"type": "Point"
},
"id": "7d9674ce54f0d9a489c1cb214da80821"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Meta Fierro</strong><p><strong>address:</strong> 9 rue Jean Vaujany 38100 Grenoble<br /><strong>time:</strong> 21:30 - 02:00<br /><strong>price:</strong> 12&euro;<br /><strong>contact:</strong> phone: 0618916592 - email: info@ecosdelplata.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.708192,45.160904],
"type": "Point"
},
"id": "7f040d8458d97bd67cf8133f9c74445b"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Susurro</strong><p><strong>address:</strong> Carrer de Quevedo 29, Barcelona, Barcelona 08012, Spain<br /><strong>time:</strong> 22:00 - 02:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 688 302 594 ?  - email: susurro.barcelona@gmail.com - facebook: /tangosusurro</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.161712,41.40298],
"type": "Point"
},
"id": "88211996e038d0a781c837120ab1e372"
},
{
"type": "Feature",
"properties": {
"description": "<strong>el Baile de Los Domingos </strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 19:30 - 23:30<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0613143886 - email: info@fuegodetango.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397755,48.853248],
"type": "Point"
},
"id": "892342f4772bd6f6da13d22db503f345"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de los Viernes</strong><p><strong>address:</strong> 7 Boulevard Marius Thomas, 13007 Marseille<br /><strong>time:</strong> 20:00 - 00:30<br /><strong>price:</strong> 7&euro;<br /><strong>contact:</strong>0687200434 - email: contact13@lestrottoirs.fr</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.360831,43.284936],
"type": "Point"
},
"id": "8c41504e17e8f2e0be56b89023267f5c"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Danse avec moi</strong><p><strong>address:</strong> 17 avenue Pierre de Coubertin 75013 Paris<br /><strong>time:</strong> 17:00 - 21:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0660596472 - email: machonb@gmail.com</p>",
"business":"milonga",
"day": "n/a",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.344454,48.818101],
"type": "Point"
},
"id": "92e1738fb14a3e82629b94d427ff8ae9"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Abrazo Tango du Lundi</strong><p><strong>address:</strong> 105 rue de Tolbiac 75013 Paris <br /><strong>time:</strong> 19:00 - 02:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0660371685 - email: intensive.danse@gmail.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.364686,48.826615],
"type": "Point"
},
"id": "9889a28700b75353938562e47829d0fc"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Alma de Tango (Casa Radical)</strong><p><strong>address:</strong> Blvd. San Juan y Avenida Velez Sarsfield, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.188929,-31.420343],
"type": "Point"
},
"id": "99ad1f017ee26fc27f2b22f1b5548821"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cofradia (Tsunami)</strong><p><strong>address:</strong> Laprida 453, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.193588,-31.422502],
"type": "Point"
},
"id": "9cf250cf6482288b8d8eb14d326d5bcc"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Casa Valencia</strong><p><strong>address:</strong> Carrer de Còrsega, 335, 08037 Barcelona<br /><strong>time:</strong> 22:00 - 01:30<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 934 234 817 ? - email: info@antoniatango.com - website: www.antoniatango.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.160621,41.397633],
"type": "Point"
},
"id": "9d9c6ef045b6d43c3bb258ae8e0ef4f7"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Gare de la Lune</strong><p><strong>address:</strong> Pillnitzer Landstra?e 148, 01326 Dresden<br /><strong>time:</strong> 21:00 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: (0351) 267855410</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [13.830691,
51.037724],
"type": "Point"
},
"id": "9dab09bdf0873f9668e4ecd8ff714454"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Praktilonga Del Barrio 108</strong><p><strong>address:</strong> 108 Boulevard Baille 13003 Marseille<br /><strong>time:</strong> 20:30 - 00:00<br /><strong>price:</strong> 4&euro;<br /><strong>contact:</strong> phone: 0682972457 - email: elvisarsic@yahoo.fr</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.391092,43.287744],
"type": "Point"
},
"id": "9e5da3ac5442610462d7986b75415cef"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Le Bal des Timides</strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 20:00 - 00:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0677559475 - email: info@tanguediaparis.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397694,48.853229],
"type": "Point"
},
"id": "9e8cb532068b3a264ffb28ba8a0f8353"
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Bailecito</strong><p><strong>address:</strong> 31 Bd Jeanne d Arc 93100 Montreuil<br /><strong>time:</strong> 19:30 - 22:30<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0662782425 - email: kryptonniques@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.445077,48.854518],
"type": "Point"
},
"id": "a01cddd5149005b6145c85515682c6fd"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Oh la la</strong><p><strong>address:</strong> 9 Rue Saint Bruno 75018 Paris<br /><strong>time:</strong> 22:00 - 02:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0645579096 - email: tangoyoungparis@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.355044,48.885832],
"type": "Point"
},
"id": "a0944ebcdea37e1b26f135975dee8633"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practice at Querido with Kelly</strong><p><strong>address:</strong> HaRav Kosovsky St 69, Tel Aviv-Yafo<br /><strong>time:</strong> 21:00 - 00:30<br /><strong>price:</strong> 25 NIS<br /><strong>contact:</strong> -</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.803326,
32.097732],
"type": "Point"
},
"id": "a14c72ae82258da4c7b0a37615f65dba"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Esquina del Tango</strong><p><strong>address:</strong> Schl?sserstra?e 5, 99084 Erfurt<br /><strong>time:</strong> 22:00 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: +49(0)361 21250004</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [11.033318,
50.976668],
"type": "Point"
},
"id": "a22bd0d459172222bfc35fca66048e15"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Debutants</strong><p><strong>address:</strong> 105 rue de Tolbiac 75013 Paris <br /><strong>time:</strong> 21:30 - 00:00<br /><strong>price:</strong> 3&euro;<br /><strong>contact:</strong> phone: 0629802933 - email: sabkil@yahoo.fr</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.364677,48.826605],
"type": "Point"
},
"id": "a4b855c97514d2730cf3c2331dd771e3"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Dolce Vita</strong><p><strong>address:</strong> 16A Boulevard de Reuilly 75012 Paris<br /><strong>time:</strong> 18:30 - 00:30<br /><strong>price:</strong> 10&euro;<br /><strong>contact:</strong> phone: 0614714239 / 0661727998 - email: milongaladolcevita@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.391501,48.838807],
"type": "Point"
},
"id": "a73c3e4c2764eabb1fa7dbc5e6d12668"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Pantera</strong><p><strong>address:</strong> Carrer de Berga 34, Barcelona, Barcelona 08012, Spain<br /><strong>time:</strong> 22:00 - 02:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 626 805 584  - facebook: /Amigos La Pantera</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.152614,41.401005],
"type": "Point"
},
"id": "aaa06a106b6b86fd0f83e5574efbc0d2"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Oxygene</strong><p><strong>address:</strong> 4 impasse Cordon Boussard 75020 Paris<br /><strong>time:</strong> 17:00 - 22:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0617763117 - email: imed.tango@free.fr</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397402,48.865675],
"type": "Point"
},
"id": "aad702d1800430f4f438b3a612ba41d3"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cafe de Los Angelitos</strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 15:00 - 18:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: 0601742190 - email: nunesrodriguescarlos@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397707,48.853234],
"type": "Point"
},
"id": "ad09bae14b35a2483d87d4897ea230b9"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Milonga el Bulin</strong><p><strong>address:</strong> 65 Quai d'Orsay, 75007 Paris<br /><strong>time:</strong> 18:00 - 22:00<br /><strong>price:</strong> 7&euro;<br /><strong>contact:</strong> phone: 0663771325 - email: sandrinenavarro@ymail.com</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.306595,48.862262],
"type": "Point"
},
"id": "ad70caff75ed57a3800bedb975e3e584"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Studio 24</strong><p><strong>address:</strong> Gro?enhainer Str. 29, 01097 Dresden<br /><strong>time:</strong> 21:30 - ??:??<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0172- 3622455</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [13.734886,
51.073394],
"type": "Point"
},
"id": "af55e2038efa91cecf0d6cefdd375d91"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Angel</strong><p><strong>address:</strong> Carrer de Bail?n 102/104, Barcelona, Barcelona 08009, Spain<br /><strong>time:</strong> 20:00 - 01:30<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 647 059 470 ? - email: milongadelangel@movistar.es</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.170442,41.397061],
"type": "Point"
},
"id": "b3fa5afbccc77cb916c1cb13c8f50391"
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Abrazo</strong><p><strong>address:</strong> Lavalleja 851, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.18762,-31.404856],
"type": "Point"
},
"id": "b50b339aa52d8c4f2e79c30517e8fbca"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Macondo</strong><p><strong>address:</strong> 44 Rue Bouret 75019 Paris<br /><strong>time:</strong> 20:00 - 01:00<br /><strong>price:</strong> 5/8&euro;<br /><strong>contact:</strong> phone: 0643032112 - email: milonga@tangobrut.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.373022,48.88222],
"type": "Point"
},
"id": "b935003a22f3a50ee8eaa949c269a0f1"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tango Barge</strong><p><strong>address:</strong> 5 Port de la Rap?e, 75012 Paris<br /><strong>time:</strong> 20:30 - 01:00<br /><strong>price:</strong> 10/12&euro;<br /><strong>contact:</strong> phone: 0140020909 - email: resa@cafebarge.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.372375,48.841495],
"type": "Point"
},
"id": "ba11ae97caefc2b766758f072ad98886"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga im Projekt Eins</strong><p><strong>address:</strong> Zerbster Str. 2, 06844 Dessau-Rohlau<br /><strong>time:</strong> 20:30 - ??:??, every first and third week of the month<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> - email: info@tango-dessau.de</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [12.246921,
51.833819],
"type": "Point"
},
"id": "c05af289e64175658ec6342e37b1e750"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga la Fabrica</strong><p><strong>address:</strong> 8 rue du lieutenant Meschi 13005 Marseille<br /><strong>time:</strong> 18:00 - 23:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0603030766 - email: pascal_botella@hotmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.401629,43.29517],
"type": "Point"
},
"id": "c0e723794dbf29b4f38e4a6775989780"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique de Gotan</strong><p><strong>address:</strong> 34 rue du Soleil 75020 Paris<br /><strong>time:</strong> 18:30 - 21:30<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0767295015 - email: contact@gotancity.com</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.395464,48.873495],
"type": "Point"
},
"id": "c1fc877273c5a692371ba18064ba580d"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Pomme d Eve</strong><p><strong>address:</strong> 1 rue Laplace 75005 Paris<br /><strong>time:</strong> 21:00 - 00:00<br /><strong>price:</strong> 0/5&euro;<br /><strong>contact:</strong> phone: 0699454499 email. luis.sanz.tango@gmail.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.347963,48.847166],
"type": "Point"
},
"id": "c2c3b4234fda53d2be2dcec4d6d83905"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique la Bombonera</strong><p><strong>address:</strong> 3 rue des Vignoles 75020 Paris<br /><strong>time:</strong> 19:00 - 00:00<br /><strong>price:</strong> 7&euro;<br /><strong>contact:</strong> phone: 0143723378 - email: info@tanguediaparis.com</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397687,48.85324],
"type": "Point"
},
"id": "c61c1925ef515d1259398e46e9e6e9e7"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Flor Nocturna Menilmontant</strong><p><strong>address:</strong> 145 rue Oberkampf 75011 Paris<br /><strong>time:</strong> 21:00 - 00:00<br /><strong>price:</strong> 5/7&euro;<br /><strong>contact:</strong> phone: 0669340286 - email: elezequiel@hotmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.381235,48.866689],
"type": "Point"
},
"id": "c7ff6e92ac8dc64b3b1a2d7b84478648"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Media luna</strong><p><strong>address:</strong> Jabotinsky St 65, Rishon LeTsiyon<br /><strong>time:</strong> 21:30 - 00:00<br /><strong>price:</strong> 30 NIS<br /><strong>contact:</strong> phone: 0546962665 / 0544997499</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.799453,
31.966986],
"type": "Point"
},
"id": "caea7ee1bec8a7e7b0b2cb03a2dd7e07"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Notariat</strong><p><strong>address:</strong> Carrer del Notariat 1, Barcelona, Barcelona 08001, Spain<br /><strong>time:</strong> 20:00 - 24:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 635 08 89 10</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.169284,41.382804],
"type": "Point"
},
"id": "cc241553aba977cfadc11658cbdcb3c4"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Azul</strong><p><strong>address:</strong> 2 rue Perr?e 75003 Paris<br /><strong>time:</strong> 20:30 - 22:00<br /><strong>price:</strong> 2&euro;<br /><strong>contact:</strong> phone: 0662748301 - email: jeux2tango@gmail.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.362698,48.863882],
"type": "Point"
},
"id": "d06ff2746148340a0c30537175fd96fd"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Esta Noche De Luna </strong><p><strong>address:</strong> 12 avenue Jean Aicard 75011 Paris<br /><strong>time:</strong> 20:30 - 03:00<br /><strong>price:</strong> 10&euro;<br /><strong>contact:</strong> phone: 0613538953 - email: lemiragetango@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.381471,48.865275],
"type": "Point"
},
"id": "d084af7bb08abae81e38b957fe13e387"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Saint Barnabe</strong><p><strong>address:</strong> 29 Rue S?rie 13012 Marseille<br /><strong>time:</strong> 17:00 - 21:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: 0677576819 - email: resa.dmm.tango@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.417884,43.302961],
"type": "Point"
},
"id": "d08790425f9a66fea2ca67014605517a"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tsunami Tango (Tsunami)</strong><p><strong>address:</strong> Laprida 453, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.193525,-31.422522],
"type": "Point"
},
"id": "d1cbe65a283fdbcd18764a7217fde847"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Le Balbutiant</strong><p><strong>address:</strong> 51 rue Edouard Vaillant 93100 Montreuil<br /><strong>time:</strong> 19:30 - 02:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0699251622 - email: lechantier.tangofever@gmail.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.432094,48.854208],
"type": "Point"
},
"id": "d23843898ae7c6775b3370869fdf3dc7"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Arrabal Milonga</strong><p><strong>address:</strong> Carrer de Terol 26, Barcelona, Barcelona 08012, Spain<br /><strong>time:</strong> 22:00 - 01:30<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: +34 622 034 135 ? - email: fercorrado@gmail.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.158524,41.403289],
"type": "Point"
},
"id": "d33d0f9fbcc7b2e7db15b926d9e6503b"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique Republique</strong><p><strong>address:</strong> 17 rue Faubourg du Temple 75010 Paris<br /><strong>time:</strong> 20:15 to 23:35<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0612564033 / 0685684441 - email: collignon-muriel@orange.fr</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.365956,48.868411],
"type": "Point"
},
"id": "d4613994cba486ec5463e1a685f435a4"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga du Vieux Port</strong><p><strong>address:</strong> 17 quai de Rive Neuve 13007 Marseille<br /><strong>time:</strong> 21:00 - 00:01<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phoen: 0632871753 - email: tangoporvos@gmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.370523,43.292989],
"type": "Point"
},
"id": "d6326b787a9c3dbbbf0928250371ce25"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Casa del Tango</strong><p><strong>address:</strong> 11 all?e Darius Milhaud 75019 Paris<br /><strong>time:</strong> 16:00 - 19:00<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: 0140407360 - email: message@lacasadeltango.net</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.385734,48.883698],
"type": "Point"
},
"id": "d6e1dc7d4cb0773f3a3f5ba63510c969"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Victor</strong><p><strong>address:</strong> 35 rue Jussieu 75005 Paris<br /><strong>time:</strong> 19:30 - 22:30<br /><strong>price:</strong> 6&euro;<br /><strong>contact:</strong> phone: 0612068751 - email: tangopolis@free.fr</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.353556,48.847012],
"type": "Point"
},
"id": "de5c4aac64d14e06e1adff92dc2f0a42"
},
{
"type": "Feature",
"properties": {
"description": "<strong>TangoNegro+Long Practice</strong><p><strong>address:</strong> Yitshak Sadeh Street 29, Tel Aviv-Yafo<br /><strong>time:</strong> 22:00 - 01:00<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0544427696</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.787783,
32.065257],
"type": "Point"
},
"id": "df6b753580f05be283e7f26268ea88e2"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Luna de Tango</strong><p><strong>address:</strong> Lavalleja 851, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.18762,-31.404866],
"type": "Point"
},
"id": "e001db552078462049fcc43d2062ab74"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Negro-Technique & Music</strong><p><strong>address:</strong> Yitshak Sadeh Street 29, Tel Aviv-Yafo<br /><strong>time:</strong> 22:00 - 01:00<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0544427696</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.787839,
32.065241],
"type": "Point"
},
"id": "e0a1954509ebc4229114eb0c07b7c9d7"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milongathon</strong><p><strong>address:</strong> 80 Boulevard Jean Jaur?s, 92110 Clichy<br /><strong>time:</strong> 15:00 - 20:00<br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0607538515 - email: labeltango@laposte.net</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.304252,48.902467],
"type": "Point"
},
"id": "e62ce47571bcbc9321ad0784d3ac4919"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Triunfal</strong><p><strong>address:</strong> Lavalleja 851, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.18762,-31.40485],
"type": "Point"
},
"id": "e6568c8f2d1d3e840ab92e99988117fa"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Amor De Tango</strong><p><strong>address:</strong> 121 Avenue Achille Peretti, 92200 Neuilly-sur-Seine<br /><strong>time:</strong> 21:00 - 00:30<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0613607225 - email: tangoturquito@yahoo.fr</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.270096,48.884025],
"type": "Point"
},
"id": "e75a75675698a9fa0ef95850320b7d21"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Abrazo Tango</strong><p><strong>address:</strong> 105 rue de Tolbiac 75013 Paris <br /><strong>time:</strong> 15:00 - 20:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> phone: 0145841587 - email: intensive.danse@gmail.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.364667,48.826608],
"type": "Point"
},
"id": "e930c734cf083bdb1886cca3f0a9e697"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Luis au Studio Gambetta</strong><p><strong>address:</strong> 64 rue Orfila 75020 Paris<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: 0699454499 - email: luis.sanz.tango@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.397779,48.866883],
"type": "Point"
},
"id": "ea26e1b3b6c2bfca710bd149fad3546e"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Slava Tango</strong><p><strong>address:</strong> Sderot Rothschild 8, Ashdod<br /><strong>time:</strong> 21.00 - 22.00<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0547826780</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [34.628918,
31.772087],
"type": "Point"
},
"id": "eacd5a32f5e6f8ec15d9404edabf84c4"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pratique la Pallud </strong><p><strong>address:</strong> Chemin de La Pallud 38700 la Tronche<br /><strong>time:</strong> 20:00 - 23:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 0476501906 - email: kruger.nicole@wanadoo.fr</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [5.742372,45.2048],
"type": "Point"
},
"id": "ef805017fcf9074d8dfc7fb3ab8af894"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Farolitos (Tsunami)</strong><p><strong>address:</strong> Laprida 453, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.193559,-31.422512],
"type": "Point"
},
"id": "efdb8399fdaa79c0f09c27b8a882e03a"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Anna Jerusalem</strong><p><strong>address:</strong> Hillel St 27, Jerusalem<br /><strong>time:</strong> 21.00 - 22.00<br /><strong>price:</strong> &euro;<br /><strong>contact:</strong> phone: 0545744327 website: www.tangoanna.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [35.218841,
31.780417],
"type": "Point"
},
"id": "f55ab6e7857e9959d6bb81386777b046"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Nueva Milonga</strong><p><strong>address:</strong> Sarachanga 812, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.185466,-31.390752],
"type": "Point"
},
"id": "fb506b2e14a8786aaa6cbbc85dc4dd9c"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Plaza Colon</strong><p><strong>address:</strong> Plaza Colon, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.195924,-31.40929],
"type": "Point"
},
"id": "fbc467588d4b8679d3ab833332cca644"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Plaza San Martin</strong><p><strong>address:</strong> Plaza San Martin, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.183718,-31.416695],
"type": "Point"
},
"id": "fd14a99a47e3142b4c367fdc356bcc3a"
},
{
"type": "Feature",
"properties": {
"description": "<strong> Pratique Tango Queer</strong><p><strong>address:</strong> 12 rue Henri Ribiere 75019 Paris<br /><strong>time:</strong> 19:00 - 22:00<br /><strong>price:</strong> 5&euro;<br /><strong>contact:</strong> - email: tangoqueer.tdv@gmail.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.395526,48.876703],
"type": "Point"
},
"id": "fd283a910bd81aeb74ac5d38ba7db903"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Caminito</strong><p><strong>address:</strong> La Rioja 447, Córdoba, Argentina<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> ?&euro;<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-64.18807,-31.409596],
"type": "Point"
},
"id": "fecdc1df2ce463a523d37afe5dfac083"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Nina's Sunday Milonga</strong><p><strong>address:</strong> 10 Fifeshire Ave, Te Aro, Wellington 6011<br /><strong>time:</strong> 19:30 - 22:30 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> phone: 021 - 0588 047 - email: ninavanduynhoven@hotmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [174.7807951,-41.2975500,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Fringe Lunchtime Milonga</strong><p><strong>address:</strong> 26-32 Allen St, Te Aro, Wellington 6011<br /><strong>time:</strong> 12:00 - 14:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> web site: http://www.tangodelalma.co.nz</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [174.7820584,-41.2932709,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Ricardos Milonga</strong><p><strong>address:</strong> 10 Fifeshire Ave, Te Aro, Wellington 6011<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> phone: (021)147 4301</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [174.7807951,-41.2975318,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Saturday Practica</strong><p><strong>address:</strong> 1st floor 22 Webb St, Te Aro, Wellington 6011 <br /><strong>time:</strong> 16:30 - 19:00 <br /><strong>price:</strong> 8$<br /><strong>contact:</strong> phone: (021)1328103</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [174.7747636,-41.2980629,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Electrica</strong><p><strong>address:</strong> 1416 Electric Ave, Venice, CA 90291<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> 12$<br /><strong>contact:</strong> phone: (310)740-2007</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.4650925,33.9909743,0],
"type": "Point"                      					
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica Porteña</strong><p><strong>address:</strong> Gallery 381, 381 W 6th St, San Pedro, CA 90731<br /><strong>time:</strong> 20:30 - 22:30 <br /><strong>price:</strong> 7$<br /><strong>contact:</strong> phone: (310)902-8503 - email: silvia@tangosanpedro.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"                       					
},

"geometry": {
"coordinates": [ -118.2852651,33.7384962,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Mio on LaBrea</strong><p><strong>address:</strong> 831 South La Brea Ave, Inglewood, CA 90301<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> phone: (323) 653-5883 -  facebook: https://www.facebook.com/profile.php?id=100013876116219 <br />-  email: info@latanguero.com  </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.352726,33.9534716,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tuesday Night Tango in Ventura</strong><p><strong>address:</strong> 454 E Main St, Ventura, CA 93001, USA<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> 5$<br /><strong>contact:</strong> web site: www.quantumtango.net</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [-119.2938342,34.2805661],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Luis au Studio Gambetta</strong><p><strong>address:</strong> 8558 W 3rd St, Los Angeles, CA 90048<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> 8&euro;<br /><strong>contact:</strong> phone: (310)275-4683</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.378875,34.0734289,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Experience</strong><p><strong>address:</strong> 541 Standard St, El Segundo, CA 90245<br /><strong>time:</strong> 19:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (310)621-0622</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.414978,33.923331,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club El Baron with Live Music</strong><p><strong>address:</strong> 8641 Washington Blvd, Culver City, CA 90232<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (818) 231-2565</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.382534,34.030409,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Different in Newport Beach</strong><p><strong>address:</strong> 4253 Martingale Way, Newport Beach, CA 92660<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> website: www.tapasflavorsofspain.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -117.865795,33.6659383,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Room Practica</strong><p><strong>address:</strong> 4346 Woodman Ave, Sherman Oaks, CA 91423<br /><strong>time:</strong> 22:15 - 00:30 <br /><strong>price:</strong> 5$<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.4307774,34.1494076,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga 'El Abrojito'</strong><p><strong>address:</strong> 2100 N Glenoaks Blvd, Burbank, CA 91504<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.3260956,34.1978967,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Monica's Milonga</strong><p><strong>address:</strong> 2100 N Glenoaks Blvd, Burbank, CA 91504<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (213) 880-2552 or (310) 273-6650</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.3260956,34.1978967,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Bombón</strong><p><strong>address:</strong> 1618 Cotner Ave, Los Angeles, CA 90025<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.4464911,34.0479052,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Libertad Practica-longa</strong><p><strong>address:</strong> 3141 Glendale Blvd, Los Angeles, CA 90039<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> - email: akaramouni@aol.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.2622706,34.1170371,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Sonata</strong><p><strong>address:</strong> 210 N 1st Ave, Arcadia, CA 91006<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> website: www.sonataroom.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.0281761,34.142757,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga Avant Garde</strong><p><strong>address:</strong> 1502 E Warner Ave, Santa Ana, CA 92705<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> 12$<br /><strong>contact:</strong> website: www.facebook.com/LaMilongaNewportBeach</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -117.8493782,33.7154504,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Caltech Milonga</strong><p><strong>address:</strong> 1200 E California Blvd, Pasadena, CA 91106<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.1248773,34.1342922,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Luna</strong><p><strong>address:</strong> 4040 Eagle Rock Blvd, Los Angeles, CA 90065<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong>  - email: yuli-b.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},

"geometry": {
"coordinates": [ -118.227198,34.1204392,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Querida</strong><p><strong>address:</strong> 4040 Eagle Rock Blvd, Los Angeles, CA 90065<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> website: yuli-b.com</p>",
"business":"milonga",
"day":"Friday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.227198,34.1204392,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Michael Espinoza's Westwood Milonga</strong><p><strong>address:</strong> 1941 Westwood Blvd, Los Angeles, CA 90025<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (818) 996-1228</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.4351389,34.0473259,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Hollywood Tango Practica</strong><p><strong>address:</strong> 817 N Highland Ave, Los Angeles, CA 90038<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.3389172,34.0857998,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Flame Milonga</strong><p><strong>address:</strong> 11330 Santa Monica Blvd, Los Angeles, CA 90025<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> 12/15$<br /><strong>contact:</strong> phone: (714) 470-3838, (310) 845-5993</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.4492186,34.0459698,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Fiesta Tango Milonga</strong><p><strong>address:</strong> 6732 Van Nuys Blvd, Van Nuys, CA 91405<br /><strong>time:</strong>  n/a <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> - email: gdgdtango@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.4482986,34.1931698,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Rincon</strong><p><strong>address:</strong> 13931 Carroll Way, Tustin, CA 92780<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> 12$<br /><strong>contact:</strong> phone: 714.206.2220 or 949.510.8919</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -117.8303005,33.7608941,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Encuentro</strong><p><strong>address:</strong> 4346 Woodman Ave, Sherman Oaks, CA 91423<br /><strong>time:</strong> 21:30 - 03:00 <br /><strong>price:</strong> 14/17$<br /><strong>contact:</strong> phone: (818) 606-5895</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.4307774,34.1494076,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Noche De Tango</strong><p><strong>address:</strong> 1502 E Warner Ave, Santa Ana, CA 92705<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> 12$<br /><strong>contact:</strong> phone: 949.442.7600</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -117.8493782,33.7154504,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Luis au Studio Gambetta</strong><p><strong>address:</strong> 17 S 1st St, Alhambra, CA 91801<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (323) 653-5883</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.1275353,34.0940086,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Los Amigos!</strong><p><strong>address:</strong> 25 Allen Ave, Pasadena, CA 91106<br /><strong>time:</strong> 20:30 - 01:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (323) 653-5883</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},

"geometry": {
"coordinates": [ -118.1133777,34.1463462,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Porteña</strong><p><strong>address:</strong> 381 W 6th St, San Pedro, CA 90731<br /><strong>time:</strong> 21:00 - 01:30 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> website: tangosanpedro.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.2852651,33.7384962,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga A Media Luz</strong><p><strong>address:</strong> 13931 Carroll Way, Tustin, CA 92780<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> phone: (562) 481 1596 - email: info@CelebtaionDanceStudio.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -117.8303005,33.7608941,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Nuestra</strong><p><strong>address:</strong> 2100 N Glenoaks Blvd, Burbank, CA 91504<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> phone: 814-826-4642 - email: milonganuestra@gmail.com <br/>- facebook:: https://www.facebook.com/events/226174091282783/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.3260956,34.1978967,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga @ LAX</strong><p><strong>address:</strong> 8025 W Manchester Ave, Playa Del Rey, CA 90293<br /><strong>time:</strong> 18:00 - 23:30 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> phone: 310-621-0622 - email: livingtango@live.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [ -118.4365839,33.959554,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga CIELO TANGUERO</strong><p><strong>address:</strong> 875 Fairway Dr, Walnut, CA 91789<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> 15$<br /><strong>contact:</strong> website: www.dancetango.org/milonga-cielo-tanguero</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},

"geometry": {
"coordinates": [ -117.872484,34.000258,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club Danzarin</strong><p><strong>address:</strong> 3625 W MacArthur Blvd #307, Santa Ana, CA 92704<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (949) 837-0440</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -117.9175548,33.7031595,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica - Jerry & Christine</strong><p><strong>address:</strong> 4346 Woodman Ave, Sherman Oaks, CA 91423<br /><strong>time:</strong> 22:00 - 23:00<br /><strong>price:</strong> 20$<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [ -118.4307774,34.1494076,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tabor</strong><p><strong>address:</strong> Kotnikova ulica 4, 1000 Ljubljana<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +386 31 389 284 - email: <br>majas@tango.si</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [14.5132128,46.0537224,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Brunch milonga</strong><p><strong>address:</strong> Letališka cesta 29, 1000 Ljubljana<br /><strong>time:</strong> 12:00 - 16:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: tango-motion.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [14.5650791,46.0629823,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tango Popular</strong><p><strong>address:</strong> Masarykova cesta 24, 1000 Ljubljana<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: https://www.facebook.com/events/1952980464716163/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [14.5172911,46.0573297,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Srecna Neolonga</strong><p><strong>address:</strong> Vilharjeva cesta 3, 1000 Ljubljana<br /><strong>time:</strong> 21:00 - 03:00 <br /><strong>price:</strong> 10€<br /><strong>contact:</strong> website: https://www.facebook.com/events/1302204253259745/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [14.5083743,46.05988,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Belivol</strong><p><strong>address:</strong> Stanetova ulica 3, 3000 Celje, Slovenia<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> website: https://www.facebook.com/events/1953007998293103/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [15.2648719,46.2291608,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Calesita</strong><p><strong>address:</strong> Miklošiceva cesta 1, 1000 Ljubljana<br /><strong>time:</strong> 19:00 - 02:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> website: https://www.facebook.com/tangoljubljana/events/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [14.5063857,46.0529786,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonguita Ljubljana</strong><p><strong>address:</strong> Mesarska cesta 4, 1000 Ljubljana<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: https://www.facebook.com/TangoDJAlenka/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [14.5238016,46.0483308,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Bar</strong><p><strong>address:</strong> 36 Bell St, Glasgow G1 1LG<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> - email: stuiji@hotmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [-4.2446658,55.8579305,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Libre</strong><p><strong>address:</strong> Harland House, 1120 South St, Glasgow G14 0AP<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> - email: dancewithattitude@btinternet.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [-4.3519523,55.8768382,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Abrazo Tango Club</strong><p><strong>address:</strong> 350 Sauchiehall St, Glasgow G2 3JD<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> £5<br /><strong>contact:</strong> phone: 0796 2865 324</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [-4.2651325,55.8657696,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Saturday Midnight<br> Milonga</strong><p><strong>address:</strong> The Renfield Centre, 260 Bath St, Glasgow G2 4JP<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> 5/7£<br /><strong>contact:</strong> - email: glasgowtangocollective@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [-4.2675123,55.8653714,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Early Evening Milonga</strong><p><strong>address:</strong> 64 Balmoral St, Glasgow G14<br /><strong>time:</strong> 18:15 - 21:15 <br /><strong>price:</strong> £6<br /><strong>contact:</strong> phone: 0796 2865 324</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [-4.3534225,55.878546,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Tea</strong><p><strong>address:</strong> 685 Alexandra Parade, Glasgow G31 3LN<br /><strong>time:</strong> 14:30 - 17:30 <br /><strong>price:</strong> £5<br /><strong>contact:</strong> - email: info@glasgowtango.org.uk</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},

"geometry": {
"coordinates": [-4.2083994,55.8629396,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milongas in Stirling 1</strong><p><strong>address:</strong> 4 Viewfield Pl, Stirling FK8 1NQ<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> £4<br /><strong>contact:</strong> phone: 07720 212500</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [-3.9385865,56.1219496,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milongas in Stirling 2</strong><p><strong>address:</strong> King St, Stirling FK8 1AX<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> 5/7£<br /><strong>contact:</strong> phone: 07720 212500</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [-3.9380295,56.1183358,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milongas in Stirling 3</strong><p><strong>address:</strong> Keir St, Bridge of Allan, Stirling FK9 4NW<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> £6<br /><strong>contact:</strong> - email: stirlingtango@yahoo.co.uk</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [-3.9484858,56.1537884,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga at Paddington Uniting Church</strong><p><strong>address:</strong> 395 Oxford St, Paddington NSW 2021<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},

"geometry": {
"coordinates":[151.2297406,-33.887412,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Columbian Hotel Milonga</strong><p><strong>address:</strong> 117 Oxford St, Paddington NSW 2021<br /><strong>time:</strong> 19:30 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},

"geometry": {
"coordinates":[151.2220399,-33.8837038,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Bar Cleveland Milonga</strong><p><strong>address:</strong> 433 Cleveland St, Surry Hills NSW 2010<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> $8<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates":[151.214272,-33.8918743,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>St John’s Church Hall Milonga</strong><p><strong>address:</strong> 120 Darlinghurst Rd, Darlinghurst NSW 2010<br /><strong>time:</strong> 20:30 - 23:15 <br /><strong>price:</strong> $15 <br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},

"geometry": {
"coordinates":[151.2213093,-33.8772383,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Glebe Town Hall Milonga</strong><p><strong>address:</strong> 160 St Johns Rd, Glebe NSW 2037<br /><strong>time:</strong> 20:30 - 23:45 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},

"geometry": {
"coordinates":[151.1850708,-33.8825506,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club Willoughby Milonga</strong><p><strong>address:</strong> 26 Crabbes Ave, North Willoughby NSW 2068<br /><strong>time:</strong> 20:00 - 23:45 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates":[151.1977995,-33.7955191,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>St John’s Church Hall Milonga</strong><p><strong>address:</strong> 120 Darlinghurst Rd, Darlinghurst NSW 2010<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates":[151.2213093,-33.8772383,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Polish Club Milonga</strong><p><strong>address:</strong> 73 Norton St, Ashfield NSW 2131<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates":[151.126659,-33.8905833,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>City Tattersalls Milonga</strong><p><strong>address:</strong> 196-204 Pitt St, Sydney NSW 2000<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates":[151.2084921,-33.8711815,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tennis Club</strong><p><strong>address:</strong> 33 Centennial St, Marrickville NSW 2204<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates":[151.1576876,-33.9031649,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club Five Dock Milonga</strong><p><strong>address:</strong> 66 Great N Rd, Five Dock NSW 2046<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates":[151.12949,-33.869867,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Bexley RSL Milonga</strong><p><strong>address:</strong> 24 Stoney Creek Rd, Bexley NSW 2207<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> $25<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates":[151.124764,-33.949803,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tennis Club Milonga</strong><p><strong>address:</strong> 33 Centennial St, Marrickville NSW 2204<br /><strong>time:</strong> 19:30 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},

"geometry": {
"coordinates":[151.1576876,-33.9031649,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Café Limon Milonga</strong><p><strong>address:</strong> 145 Quay St, Auckland, 1010<br /><strong>time:</strong> 21:30 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},

"geometry": {
"coordinates": [174.7649216,-36.8410573,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Carl Milonga</strong><p><strong>address:</strong> 4 Poynton Terrace, Auckland, 1010<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: +64 9 630 6306 - email: carl@eltango.co.nz </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [174.7599139,-36.8565885,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Sheldon y Carolina Milonga</strong><p><strong>address:</strong> Korma Ln, Panmure, Auckland 1072<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: +64 9 369 1419 - email: sheldonYcarolina@gmail.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [174.8536182,-36.8998108,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Casatango</strong><p><strong>address:</strong> 10 Newton Rd, Grey Lynn, Auckland 1010<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> $15*<br /><strong>contact:</strong> phone: +64 9 482 3394 </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [174.7536438,-36.8604882,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pasión Por Tango</strong><p><strong>address:</strong> 4 Poynton Terrace, Auckland, 1010<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: +64 21 038 5230 - email: elsapalmer@gmail.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [174.7599139,-36.8565885,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Zanaboni</strong><p><strong>address:</strong> 545 Parnell Rd, Parnell, Auckland 1052<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: +64 9 575 8245 </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},

"geometry": {
"coordinates": [174.780136,-36.863365,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Vida</strong><p><strong>address:</strong> 40 Pushkin St, Yerevan 0010<br /><strong>time:</strong> 20:00 - 23:45 <br /><strong>price:</strong> 1000 AMD<br /><strong>contact:</strong> phone: +37495889933 email: arman.dashan@gmail.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [44.5129779,40.1827587,0]
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Petricor</strong><p><strong>address:</strong> 39 Hanrapetutyan St, Yerevan 0010<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> 1000 AMD<br /><strong>contact:</strong> email: milongapetricor@gmail.com </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [44.5146394,40.1762158,0]
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Cafelibro</strong><p><strong>address:</strong> Ignacio de Veintimilla, Quito 170143<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-78.4926906,-0.2044047,0]
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Cafelibro</strong><p><strong>address:</strong> Ignacio de Veintimilla, Quito 170143<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-78.4926906,-0.2044047,0]
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del restaurante la Cabaña</strong><p><strong>address:</strong> El Batan, Quito<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-78.475335,-0.1800504,0]
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Centro Uruguayo</strong><p><strong>address:</strong> Avenida 5 Los Castaños, Caracas, Miranda<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-66.825623,10.501571,0]
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Centro Uruguayo</strong><p><strong>address:</strong> Avenida 5 Los Castaños, Caracas, Miranda<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-66.8256175,10.5015709,0]
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Las Piernas</strong><p><strong>address:</strong> 1252 Rue de Bleury<br>Montréal, QC H3B 2W4<br /><strong>time:</strong> 12:00 - 16:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.566657,45.506101,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Studio Tango Montreal</strong><p><strong>address:</strong> 7755 Boul St-Laurent #200-A<br /><strong>time:</strong> 21:30 - 03:00 <br /><strong>price:</strong> 12$<br /><strong>contact:</strong> phone: 514 844-2786 - email: info@studiotango.ca - website: http://www.studiotango.ca/ </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.626369,45.537618,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga chez Coco</strong><p><strong>address:</strong> 5723 Park Ave, Montreal, QC H2V 4G9<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.604817,45.523389,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>MonTango</strong><p><strong>address:</strong> 5588 Rue Sherbrooke Ouest, Montréal, QC H4A 1W3<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 10/12$<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.613352,45.471843,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tangueria</strong><p><strong>address:</strong> 6396 Rue St-Hubert,<br>Montréal, QC H2S 2M2<br /><strong>time:</strong> 13:30 - 16:30 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.602644,45.535762,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Las Piernas</strong><p><strong>address:</strong> 1252 Rue de Bleury<br>Montréal, QC H3B 2W4<br /><strong>time:</strong> 15:00 - 18:00 <br /><strong>price:</strong> 7$<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.5666865,45.5060333,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Las Piernas</strong><p><strong>address:</strong> 1252 Rue de Bleury<br>Montréal, QC H3B 2W4<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.5666865,45.5060333,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Studio Tango Montreal</strong><p><strong>address:</strong> 7755 Boul St-Laurent #200-A<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> 8$<br /><strong>contact:</strong> phone: 514 844-2786 - email: info@studiotango.ca - website: http://www.studiotango.ca/ </p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.6261913,45.5376989,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tango Club</strong><p><strong>address:</strong> 41 Rue Blainville O, Sainte-Thérèse, QC J7E 1X4<br /><strong>time:</strong> 18:00 - 00:00 <br /><strong>price:</strong> 9$<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.8445333,45.6393736,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Studio Tango Montreal</strong><p><strong>address:</strong> 7755 Boul St-Laurent #200-A<br /><strong>time:</strong> 14:30 - 17:45<br /><strong>price:</strong> 8$<br /><strong>contact:</strong> phone: 514 844-2786 - email: info@studiotango.ca - website: http://www.studiotango.ca/ </p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.6262141,45.5376998,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>MonTango Café Domingo</strong><p><strong>address:</strong> 5588 Rue Sherbrooke Ouest, Montréal, QC H4A 1W3<br /><strong>time:</strong> 13:00 - 17:00 <br /><strong>price:</strong> 8/10$<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.6133189,45.4718679,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Elefin</strong><p><strong>address:</strong> 11/26 Sukhumvit Soi One | Klong Toey Nua Wattana, Bangkok 10110<br /><strong>time:</strong> 20:00 - 22:30 <br /><strong>price:</strong> 200 THB<br /><strong>contact:</strong> phone: 0818473822 </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [100.550948,13.74501,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Dream</strong><p><strong>address:</strong> 10 Sukhumvit Soi 15, Sukhumvit 15 Alley, Klongtoey Nua, Wattana, Bangkok 10110<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> 250 THB<br /><strong>contact:</strong> phone: 0818473822 </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [100.5586429,13.7407451,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tangotuesdays</strong><p><strong>address:</strong> 5565 Hauiki Rd, Kapaa, HI 96746<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: maurizia@tangokauai.com </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-159.3504143,22.0849148,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Kalaheo NCenter Practica</strong><p><strong>address:</strong> 4480 Papalina Rd, Kalaheo, HI 96741<br /><strong>time:</strong> 20:30 - 21:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: +1 808-332-9770 </p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-159.5266594,21.924988,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Friday Milonga</strong><p><strong>address:</strong> 1445 Baldwin Ave, Makawao, HI 96768<br /><strong>time:</strong> 20:30 - 23:15 <br /><strong>price:</strong> $6<br /><strong>contact:</strong> email: tangoandhula@gmail.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-156.3507539,20.8924224,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Big Island Milonga</strong><p><strong>address:</strong> 65-1108 Mamalahoa Hwy, Waimea, HI 96743<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-155.6633948,20.0255822,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>IslaTango Milonga</strong><p><strong>address:</strong> 419 South St, Honolulu, HI 96813<br /><strong>time:</strong> \t19:30 - 04:00 <br /><strong>price:</strong> \t$10<br /><strong>contact:</strong> phone: (808) 721-2123 email: info@IslaTango.com </p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-157.861412,21.30011,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tango Cafe<br></strong><p><strong>address:</strong> Vogts gate 64, 0477 Oslo, Norway<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 22 11 33 08 email: cosmopolite@cosmopolite.no </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [10.7657017,59.9361434,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>n/a</strong><p><strong>address:</strong> Normannsgata 55, 0655 Oslo<br /><strong>time:</strong> 20:00 - 02:00 <br /><strong>price:</strong> 100 NOK<br /><strong>contact:</strong> email: sivymariano@gmail.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [10.7832607,59.9119967,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Jordan Milonga</strong><p><strong>address:</strong> Abdul Hamid Shouman Street, Amman<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +962799955550 email: mohammad.sabha@tangojordan.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [35.897215,31.968478,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Club Reykjavík Café Kaffitár</strong><p><strong>address:</strong> Bankastræti 8, 101 Reykjavík, Iceland<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: http://tango.is </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-21.9343481,64.1466643,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>n/a</strong><p><strong>address:</strong> Skólavörðustígur 12, 101 Reykjavík<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> 1000 ISK<br /><strong>contact:</strong> website; http://tango.is </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-21.9308426,64.1448294,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Cramo</strong><p><strong>address:</strong> Bergstaðastræti 7, 101 Reykjavík<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 1000 ISK<br /><strong>contact:</strong> website: http://tango.is/el-cramo-milonga/ </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-21.9323261,64.1453444,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>n/a</strong><p><strong>address:</strong> Laugavegur 105, Austurbær Reykjavík<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-21.9147836,64.1434775,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Mánudags Milonga á Sólon</strong><p><strong>address:</strong> Bankastræti 7a, 101 Reykjavík<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> website: http://www.tangoadventure.com/calendar.htm </p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-21.9338339,64.146844,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga Patamango</strong><p><strong>address:</strong> Eldama Ravine Rd, Westlands, Nairobi, Kenya<br /><strong>time:</strong> 19:30 - 23:30 <br /><strong>price:</strong> 500 KES<br /><strong>contact:</strong> website: http://www.patamango.com/milonga-patamango email: nairobi.tango@patamango.com </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [36.805413,-1.253476,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Informal</strong><p><strong>address:</strong> Calle de Eugenia Viñes 223, Valencia, Valencia/València 46011, Spain (Pub Monocle)<br /><strong>time:</strong> 19:000 - 22:00<br /><strong>price:</strong> n/a&euro;<br /><strong>contact:</strong> phone: +34 620 824 636</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-0.325659,
39.471966],
"type": "Point"
},
"id": "address.10113079004349460"
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Cultural</strong><p><strong>address:</strong> Calle del Ingeniero Rafael Janini 5, Valencia, Valencia/València 46022, Spain<br /><strong>time:</strong> 21:00 - 00:30<br /><strong>price:</strong> n/a&euro;<br /><strong>contact:</strong> phone: +34 963816658 or +34 687616163</p>",
"business":"milonga",
"day": "friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.340887,
39.468722],
"type": "Point"
},
"id": "address.10570296085326580"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Club de Tango</strong><p><strong>address:</strong> Carrer Mestre Serrano 5, Alboraya, Valencia/Valencia 46120, Spain<br /><strong>time:</strong> 22:00 - 1:30<br /><strong>price:</strong> n/a&euro;<br /><strong>contact:</strong> phone: +34 620 824 636  or +34 695 125 371</p>",
"business":"milonga",
"day": "saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.353379,
39.495552],
"type": "Point"
},
"id": "address.12581598114813550"
},
{
"type": "Feature",
"properties": {
"description": "<strong> Amigos del Baile</strong><p><strong>address:</strong> Calle del Pintor Maella 35, Valencia, Valencia/València 46023, Spain (Hotel Tryp Oceanic)<br /><strong>time:</strong> 19:30 - 22:30<br /><strong>price:</strong> n/a&euro;<br /><strong>contact:</strong> phone: +34 963338005</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.348172,
39.458492],
"type": "Point"
},
"id": "address.14823707548573810"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga El Beso</strong><p><strong>address:</strong> Camino Viejo del Saler 7, Valencia, Valencia/València 46012, Spain (Sala Canal)<br /><strong>time:</strong> 18:30 - 22:30<br /><strong>price:</strong> n/a&euro;<br /><strong>contact:</strong> phone: +34 963248653</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.337528,
39.412619],
"type": "Point"
},
"id": "address.15112237542575470"
},			
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Experimental</strong><p><strong>address:</strong> Paseo de la Pechina 67, Valencia, Valencia/València 46018, Spain<br /><strong>time:</strong> practice: 18:15-19:00 / milonga: 19:00-22:30<br /><strong>price:</strong> n/a&euro;<br /><strong>contact:</strong> phone: +34 620675146</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-0.40137,
39.473756],
"type": "Point"
},
"id": "address.3700131273031210"
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Lebanon</strong><p><strong>address:</strong> Yammout Building, Spears St, Bayrut, Lebanon<br /><strong>time:</strong> n/a <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +961.70120315 email: msamuel.2007@audencia.net </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [35.4919016,33.8944018,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practilonga ¡Ahora!</strong><p><strong>address:</strong> 6, Lorong Raja Chulan, Kuala Lumpur, 50250 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> RM10<br /><strong>contact:</strong> email: ulpiavictrix@hotmail.com </p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [101.7044473,3.1505818,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango with a twist</strong><p><strong>address:</strong> 165, Jalan Ampang, Kuala Lumpur, 50450 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia<br /><strong>time:</strong> 15:00 - 19:00 <br /><strong>price:</strong> RM10<br /><strong>contact:</strong> email: marguerite.brodie@gmail.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [101.717952,3.159669,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Del Sabor</strong><p><strong>address:</strong> No 1 Jalan Pantai Jaya Tower 3, Taman Bukit Pantai, 59200 Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> RM10<br /><strong>contact:</strong> email: sara.supernova@gmail.com </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [101.6634996,3.1159732,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica @ FAME Studio</strong><p><strong>address:</strong> 43B, Jalan Desa Bakti, Off Old Klang Road, Taman Desa, 58100 Kuala Lumpur, KL, Malaysia<br /><strong>time:</strong> 21:30 - 23:00 <br /><strong>price:</strong> RM10<br /><strong>contact:</strong> email: christopher_gan@yahoo.com / mclow0@yahoo.com </p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [101.6839897,3.1029818,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Pipiolo</strong><p><strong>address:</strong> 43B, Jalan Desa Bakti, Off Old Klang Road, Taman Desa, 58100 Kuala Lumpur, KL, Malaysia<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> RM15<br /><strong>contact:</strong> email: christopher_gan@yahoo.com / mclow0@yahoo.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [101.6839897,3.1029818,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Passion Milonga</strong><p><strong>address:</strong> Jalan Hutton & Lebuh Clarke, George Town, 10050 George Town, Pulau Pinang, Malaysia<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> RM10<br /><strong>contact:</strong> email: david.yeonglt@gmail.com </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [100.3306246,5.4197396,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de la Glorieta</strong><p><strong>address:</strong> Calle Arabial, 45, 18004 Granada. Spain<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 634 15 12 74 email: milongadelaglorieta@gmail.com </p>",
"business":"milonga",
"day": "saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.6074423,37.1681953,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga del Che Buenos Aires</strong><p><strong>address:</strong> Calle Alvarez Quintero & Calle Cervantes, 18213 Jun, Granada, Spain<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> 4€<br /><strong>contact:</strong> phone: 629981383 // 665120524 website: http://www.deboraycarlos.com/ </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.5948735,37.2171975,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Zenete</strong><p><strong>address:</strong> Calle Zenete, 36, 18010 Granada, Spain<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 699 84 88 75 website: https://www.facebook.com/Los-Lunes-al-Tango-628537217254938/ </p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.5981667,37.1801843,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Tertulia</strong><p><strong>address:</strong> Calle Pintor López Mezquita, 3, 18002 Granada, Spain<br /><strong>time:</strong> 22:00 - 01:00 <br /><strong>price:</strong> 3€<br /><strong>contact:</strong> phone: 686 85 46 69 </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.60722,37.17745,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Boom Boom Room</strong><p><strong>address:</strong> Calle Cárcel Baja, 10, 18010 Granada, Spain<br /><strong>time:</strong> 22:00 - 23:15 <br /><strong>price:</strong> 3€<br /><strong>contact:</strong> phone: 626 59 67 19 email: manuelrosalestango@gmail.com </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.5977624,37.1774326,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Café Continental</strong><p><strong>address:</strong> Calle Seminario, 11, 18002 Granada, Spain<br /><strong>time:</strong> 22:00 - 01:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 622035853 // 647181246 email: juanayjulio@gmail.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.6053,37.17439,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga del Centro</strong><p><strong>address:</strong> 5, Calle del Dr Cortezo, 17, 28012 Madrid, Spain<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> 8€<br /><strong>contact:</strong> phone: 600 681 882 website: https://www.facebook.com/milongadelcentro?pnref=story.unseen-section </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7038296,40.412743,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica guiada Tango Abierto</strong><p><strong>address:</strong> Plaza de Legazpi, 7, 28045 Madrid, Spain<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 645 548 513 email: tangoabierto.es@gmail.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.6949216,40.390857,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Barrio de Tango</strong><p><strong>address:</strong> Calle Relatores, 17, 28012 Madrid, Spain<br /><strong>time:</strong> 23:59 - 04:00 <br /><strong>price:</strong> 6€<br /><strong>contact:</strong> phone: 651 04 98 71 email: andreavent@gmail.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.703304,40.412654,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La milonga de Cha3</strong><p><strong>address:</strong> Calle de San Pol de Mar, 1, 28008 Madrid, Spain<br /><strong>time:</strong> 12:00 - 15:00 <br /><strong>price:</strong> 9€<br /><strong>contact:</strong> phone: 629 90 30 63 or 609 77 86 32 - email: neltango@hotmail.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7271199,40.4240211,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica El Laboratorio</strong><p><strong>address:</strong> 5, Calle del Dr Cortezo, 17, 28012 Madrid, Spain<br /><strong>time:</strong> 16:00 - 19:00 <br /><strong>price:</strong> 4€<br /><strong>contact:</strong> phone: 637 803 777 / 667 874 309 email: info@proyecciontango.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7038296,40.412743,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Exploratango – en el Retiro</strong><p><strong>address:</strong> Plaza Nicaragua, 12, 28009 Madrid, Spain<br /><strong>time:</strong> 18:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: mercedesstg@icloud.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.6850112,40.4194896,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica N’Clave de Tango</strong><p><strong>address:</strong> Calle de Meléndez Valdés, 28, 28015 Madrid, Spain<br /><strong>time:</strong> 18:00 - 20:00 <br /><strong>price:</strong> 4€<br /><strong>contact:</strong> phone: 635514576 email: tango@clave53.org </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7111728,40.4330358,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Tropezón</strong><p><strong>address:</strong> Calle de Carretas, 14, 28012 Madrid, Spain<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> 7€<br /><strong>contact:</strong> phone: 68906328 email: milongaeltropezon@gmail.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7034487,40.4149117,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica asistida PractiTango</strong><p><strong>address:</strong> Calle de Ercilla, 48, 28005 Madrid, Spain<br /><strong>time:</strong> 18:00 - 20:00 <br /><strong>price:</strong> 4€<br /><strong>contact:</strong> phone: 635 51 45 76 // 699 61 84 21 email: tango@carmendelarosa.com </p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7013208,40.4006873,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Bulín</strong><p><strong>address:</strong> Calle Jacometrezo, 8, 28013 Madrid, Spain<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> 8€<br /><strong>contact:</strong> phone: 617745032 email: mauricegambra@hotmail.com </p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.707929,40.420426,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica La Frontera</strong><p><strong>address:</strong> Paseo de los Melancólicos, 11, 28005 Madrid, Spain<br /><strong>time:</strong> 22:30 - 00:00 <br /><strong>price:</strong> 3€<br /><strong>contact:</strong> phone: 653 109 081 email: fernandonahmijastango@gmail.com </p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.71866,40.4091421,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Bien Milonga</strong><p><strong>address:</strong> Pl. de la República Argentina, 6, 28002 Madrid, Spain<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 7€<br /><strong>contact: phone: +34 677400424 - email: j.maria.otero@movistar.es</strong></p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.6851553,40.4457209,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tankas Café</strong><p><strong>address:</strong> Calle de Silva, 4, 28013 Madrid, Spain<br /><strong>time:</strong> 21:30 - 00:30 <br /><strong>price:</strong> 7€<br /><strong>contact:</strong> phone: 617745032 </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.707659,40.4205578,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Romántica</strong><p><strong>address:</strong> Calle de Meléndez Valdés, 28, 28015 Madrid, Spain<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> phone: 635 514 576 email: romantica@carmendelarosa.com </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7111728,40.4330358,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Hondo Bajo Fondo en la Plaza</strong><p><strong>address:</strong> Plaza Dos de Mayo, 28004 Madrid, Spain<br /><strong>time:</strong> 22:15 - 00:15 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 651049871 email: andreavent@gmail.com </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7040548,40.4272533,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Chantecler</strong><p><strong>address:</strong> Calle de San Pol de Mar, 1, 28008 Madrid, Spain<br /><strong>time:</strong> 21:30 - 01:30 <br /><strong>price:</strong> 9€<br /><strong>contact:</strong> phone: 630 84 45 72 email: nestordante@yahoo.es </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7271199,40.4240211,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Agarrate Catalina</strong><p><strong>address:</strong> Calle Jacometrezo, 8, 28013 Madrid, Spain<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 8€<br /><strong>contact:</strong> phone: 629 90 30 63 or 609 77 86 32 email: neltango@hotmail.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.707862,40.4203327,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Conventiyo</strong><p><strong>address:</strong> calle Roble, 22, 28020 Madrid, Spain<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> phone: 916318922 email: pabloybeatriz@gmail.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.6969147,40.4617643,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Recuerdo</strong><p><strong>address:</strong> Av Marqués de los Vélez, 12, 30008 Murcia, Spain<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 968 23 84 00 email: info@murciatango.com </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-1.1290328,37.9934018,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Recuerdo</strong><p><strong>address:</strong> Av Marqués de los Vélez, 12, 30008 Murcia, Spain<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 968 23 84 00 email: info@murciatango.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-1.1291349,37.9934402,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de Alcorcon</strong><p><strong>address:</strong> Av. las Retamas, 80, 28922 Alcorcón, Madrid, Spain<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 629 90 30 63 - 609 77 86 32 email: neltango@gmail.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-3.8261751,40.338149,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga en rojo</strong><p><strong>address:</strong> Calle Nogal, 4, 47004 Valladolid, Spain<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: informacion@lamilonga.org </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.7218554,41.6453191,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Centro</strong><p><strong>address:</strong> Av. del Cid Campeador, 2, 09005 Burgos, Spain<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +34 696 40 08 90 email: burgoslamilongadel27@yahoo.es </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-3.6987945,42.3428484,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica en Burgos</strong><p><strong>address:</strong> Plaza Vega, 14, 09002 Burgos, Spain<br /><strong>time:</strong> 19:30 - 21:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +34 696 40 08 90 email: burgoslamilongadel27@yahoo.es </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7022295,42.3389266,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Encina</strong><p><strong>address:</strong> Calle Honduras, 9, 39005 Santander, Cantabria, Spain<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +34 657 60 14 85 email: tango@cyte.net </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7968624,43.4706368,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga en Iruña / Pamplona</strong><p><strong>address:</strong> Calle Ermitagaña, 1, 31008 Pamplona, Navarra, Spain<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 676 33 27 59 </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-1.6659791,42.8107201,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La milonga del Miércoles</strong><p><strong>address:</strong> 1 Rue du Bois Belin, 64600 Anglet, France<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-1.5268769,43.4933926,0],
"type": "Point"
}
},				
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Cachivache</strong><p><strong>address:</strong> Muelle de la Merced - Bilbao (Mesedeetako Kaia, Bilbo, Bizkaia), Spain<br /><strong>time:</strong> 18:00 - 22:00 <br /><strong>price:</strong> 6€<br /><strong>contact:</strong> phone: +34658740885 </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-2.9267144,43.2568372,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Sótano Tango - Práctica</strong><p><strong>address:</strong> Gipuzkoa Hiribidea, 10, 20302 Irun, Gipuzkoa, Spain<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 662 177 317 email: elsotanotango@yahoo.es </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-1.795009,43.3393912,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Chacabuco 852</strong><p><strong>address:</strong> Ama Kandida Hiribidea, 21, 20140 Andoain, Gipuzkoa, Spain<br /><strong>time:</strong> 22:00 - 03:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 658 758 452 - 650 487 971 email: tango@joseba-bakartxo.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-2.0194636,43.222626,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Untoquede Tango Milonga</strong><p><strong>address:</strong> Iruña Kalea, 1 Bis, 48014 Bilbo, Bizkaia, Spain<br /><strong>time:</strong> 23:30 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 622 177 738 - 667 548 749 email: ainarah.larrea@hotmail.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-2.9497836,43.2683457,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Sótano Tango - Milonga</strong><p><strong>address:</strong> Gipuzkoa Hiribidea, 10, 20302 Irun, Gipuzkoa, Spain<br /><strong>time:</strong> 18:00 - 22:00 <br /><strong>price:</strong> 10€<br /><strong>contact:</strong> phone: 662 177 317 email: elsotanotango@yahoo.es </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-1.795009,43.3393912,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pamplona Tango Club</strong><p><strong>address:</strong> Calle Real, s/n, 31110 Noáin, Navarra, Spain<br /><strong>time:</strong> 22:30 - 03:30 <br /><strong>price:</strong> 10€<br /><strong>contact:</strong> phone: 669 18 36 87 / 620 45 75 24 email: roqueygiselletango@yahoo.com.ar </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-1.6336584,42.7648394,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Gomina Tango Club</strong><p><strong>address:</strong> Calle de la Princesa, 27, 28008 Madrid, Spain<br /><strong>time:</strong> 22:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: julioluquetango@gmail.com </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.71414,40.427173,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Discoteca Verdi</strong><p><strong>address:</strong> Calle de Orense, 68, 28020 Madrid, Spain<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +34 616815813 email: gominatangoclub@gmail.com </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.6937849,40.4617152,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Larios Café</strong><p><strong>address:</strong> Calle de Silva, 4, 28013 Madrid, Spain<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 662037986 email: daniel.nuevotango@gmail.com </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.707659,40.4205578,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Baldosa Floja</strong><p><strong>address:</strong> Calle Farmacia, 2, 28004 Madrid, Spain<br /><strong>time:</strong> 22:30 - 02:00 <br /><strong>price:</strong> 7€<br /><strong>contact:</strong> phone: 662037986 email: daniel.nuevotango@gmail.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7004198,40.4238688,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga del Centro</strong><p><strong>address:</strong> Calle del Dr Cortezo, 17, 28012 Madrid, Spain<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> 8€<br /><strong>contact:</strong> phone: 600 681 882 </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.7038586,40.4126792,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Casino</strong><p><strong>address:</strong> Carrer de la Marina, 19-21, 08005 Barcelona, Spain<br /><strong>time:</strong> 19:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 932.257.878 - 605.706.502 </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.1970822,41.386669,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Patio de la Morocha</strong><p><strong>address:</strong> Calle Juan Manuel Durán González, 28, 35007 Las Palmas de Gran Canaria, Las Palmas, Spain<br /><strong>time:</strong> 19:30 - 22:30 <br /><strong>price:</strong> 2€<br /><strong>contact:</strong> website: https://www.facebook.com/milongaelpatiodelamorocha/?fref=ts </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-15.4337804,28.1354994,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Itinerante</strong><p><strong>address:</strong> Calle Simón Bolívar, 3, 35007 Las Palmas de Gran Canaria, Las Palmas, Spain<br /><strong>time:</strong> 19:30 - 22:30 <br /><strong>price:</strong> 1€<br /><strong>contact:</strong> website: http://www2.ulpgc.es/?pagina=clubscultura&ver=tango </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-15.4299621,28.1388916,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Los Mareados</strong><p><strong>address:</strong> Calle Juan Manuel Durán González, 28, 35007 Las Palmas de Gran Canaria, Las Palmas, Spain<br /><strong>time:</strong> 20:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-15.4337804,28.1354994,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>donde Todos Bailan</strong><p><strong>address:</strong> Calle Nosquera, 11, 29008 Málaga, Spain<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.4223365,36.7231065,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga El Jardin</strong><p><strong>address:</strong> Calle Cañón, 1, 29015 Málaga, Spain<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.4186162,36.7204799,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga del Atrio</strong><p><strong>address:</strong> Av. Príes, 12, 29016 Málaga, Spain<br /><strong>time:</strong> 22:30 - 02:30 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-4.4066684,36.721004,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Llave del Tango</strong><p><strong>address:</strong> Calle Malpica, 7, 29002 Málaga, Spain<br /><strong>time:</strong> 20:30 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 655764775 email: lallavedeltango@hotmail.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.429158,36.7146543,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Movediza Milonga</strong><p><strong>address:</strong> 24, Calle Carlos Mackintosh, 29602 Marbella, Málaga, Spain<br /><strong>time:</strong> n/a <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (+34) 673 44 23 24 </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.8858171,36.5080451,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Nuevo Chiqué</strong><p><strong>address:</strong> San José 224, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-4428-0100 - email: nuevochique@gmail.com - facebook: www.facebook.com/marcela.pazos.7?fref=ts</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3861195,-34.6113859,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Calsesita Milonga</strong><p><strong>address:</strong> Avenida Entre Ríos 1056, Buenos Aires, Argentina<br /><strong>time:</strong> 19:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3205-0055 - email: carlosgallegoproducciones@hotmail.com - facebook: www.facebook.com/Milongadebuenosaires</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.391522,-34.6210809,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Berretin</strong><p><strong>address:</strong> Nogoyá 5228, Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: n/a - email: haqueirolo@hotmail.com - facebook: www.facebook.com/gustavoleila.tango?hc_ref=ARTPyJld0QemPbuotkt3R_Ev6CGWtpWKTW3UsO5tVWaVKXaEBO_ZOPrcP</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.5168509,-34.6188553,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de las Barrancas</strong><p><strong>address:</strong> Echeverría 1800, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +54-911-3567 6819 - email: melbacarrion@gmail.com - facebook: www.facebook.com/lamilongadelasbarrancas</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4501612,-34.5596132,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tangotica</strong><p><strong>address:</strong> Avenida Belgrano 3655, Buenos Aires, Argentina<br /><strong>time:</strong> 20:30 - 24:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5571-0349 or +54-911-5746-0217 - email: valeriabuyatti4@hotmail.com - facebook: www.facebook.com/bybproduccionesar</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.417859,-34.6154568,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cachirulo en el Beso</strong><p><strong>address:</strong> Riobamba 416, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4932-8594 or +54-911-4577-0434 - email: cachirulotango@hotmail.com - facebook: www.facebook.com/profile.php?id=100011442587498</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3939845,-34.6041776,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La María</strong><p><strong>address:</strong> Sarmiento 4006, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6202-9812 - email: majomarini@hotmail.com - facebook: www.facebook.com/lamariatango</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.421544,-34.604915,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club Crisol</strong><p><strong>address:</strong> Saraza 953, Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-3616-5287 - email: sofia.menno4@gmail.com - facebook: www.facebook.com/Club-Crisol-1873368656234025</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4313362,-34.6379281,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Viva la Pepa</strong><p><strong>address:</strong> Avenida Córdoba 5064, Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6761-1899 - email: pepapalazon@gmail.com - facebook: www.facebook.com/100000952555433</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.434912,-34.5911819,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Queer</strong><p><strong>address:</strong> Perú 571, Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3252-6894 - email: mariandoc73@hotmail.com - facebook: www.facebook.com/Tango-Queer-Buenos-Aires-46122212389</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3746671,-34.6146097,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Catedral</strong><p><strong>address:</strong> Sarmiento 4006, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-5925-2150 - email: lacatedraldealmagro@yahoo.com.ar - facebook: www.facebook.com/pages/LA-CATEDRAL-CLUB/118477378190528 - Twitter: @lacatedralclub</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.421544,-34.604915,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Ventanita de Arrabal</strong><p><strong>address:</strong> Avenida Rivadavia 3832, Ciudad autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-4958-3633 - email: sextetofantasma@gmail.com - facebook: www.facebook.com/VentanitaDeArrabal</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4200889,-34.6114501,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Caricias</strong><p><strong>address:</strong> Avenida Doctor Ricardo Balbín 4699, Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4794-7519 or +54-911-2637-3862 - email: martinviqueirarc@gmail.com - facebook: www.facebook.com/100001461032462</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4974244,-34.5533958,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Petit</strong><p><strong>address:</strong> Honduras 5028, Buenos Aires, Argentina<br /><strong>time:</strong> 22:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6021-9256 - email: belenortizdanza@gmail.com - facebook: www.facebook.com/tangoenelclub</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4306716,-34.5885226,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Parakultural</strong><p><strong>address:</strong> Avenida Raúl Scalabrini Ortiz 1331, Buenos Aires, Argentina<br /><strong>time:</strong> 22:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4833-3224 or +54-911-5738-3850 - email: omarviola@hotmail.com - web: www.parakultural.com.ar</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4276045,-34.5927559,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de Juan</strong><p><strong>address:</strong> Adolfo Alsina 1465, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 15:00 - 21:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4802-9708 or +54-911-4023-8515 - email: juanangeltango@gmail.com - facebook: www.facebook.com/juanangeltango</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3868995,-34.6111223,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Canal Rojo Tango</strong><p><strong>address:</strong> Avenida Raúl Scalabrini Ortiz 1331, Buenos Aires, Argentina<br /><strong>time:</strong> 15:30 - 20:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4832-6753 or +54-911-3604-0714 - email: saloncanning@gmail.com - facebook: www.facebook.com/apurotangopagina</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4276045,-34.5927559,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica La Proteña</strong><p><strong>address:</strong> Bernardo de Irigoyen 172, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 17:00 - 20:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-5042-6636 - email: nsp916@yahoo.com.ar - facebook: www.facebook.com/noemi.spinelli.399</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.380355,-34.610332,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga del Pueblo</strong><p><strong>address:</strong> Avenida Sáenz 459, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 17:00 - 20:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5098-3753 - email: oscarhectortango@gmail.com - facebook: www.facebook.com/100003025754372</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4166284,-34.6447875,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Maipu</strong><p><strong>address:</strong> Avenida Entre Ríos 1056, Buenos Aires, Argentina<br /><strong>time:</strong> 18:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-6519-9573 or +54-4300-8007 - email: elmaipu@hotmail.com - facebook: www.facebook.com/100000140870086</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.391522,-34.6210809,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Ciclo de Tango en el Verdi</strong><p><strong>address:</strong> Avenida Almirante Brown 736, Buenos Aires, Argentina<br /><strong>time:</strong> 18:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +54_911-4448-5691 - email: otesquinasur@gmail.com - facebook: www.facebook.com/ciclodetangoenelverdi</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3616354,-34.6311976,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica en la escuela DNI</strong><p><strong>address:</strong> Bulnes 1011, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4864-4575 or +54-11-4866-6553 - email: celina@dni-tango.com - web: www.dni-tango.com</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.41772,-34.5997256,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Bailongo de la Glorieta</strong><p><strong>address:</strong> Echeverría 1800, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-4938-5645 - email: petcheverry@hotmail.com - facebook: www.facebook.com/pablo.etcheverry.77</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4502858,-34.5595385,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Sueño Porteño</strong><p><strong>address:</strong> La Rioja 1180, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5021-4996 - email: abailarquelavidaseva@yahoo.com.ar - facebook: www.facebook.com/groups/333851066707170</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4070432,-34.623978,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonguita</strong><p><strong>address:</strong> Avenida Raúl Scalabrini Ortiz 1331, Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4771-8827 or +54-911-3068-1271 - email: milonguitabaile@gmail.com - web: www.milonguitabaile.com.ar</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4276533,-34.5927316,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica en Sadem Art</strong><p><strong>address:</strong> Avenida Belgrano 3655, Buenos Aires, Argentina<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-6194-6146 - email: marcohernandez.ar@gmail.com - facebook: www.facebook.com/auditoriosademart</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4178418,-34.6154887,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Rodriguez</strong><p><strong>address:</strong> Gral. Manuel A. Rodríguez 1191, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-2308-2819 or +54-911-5645-8027 - email: famatango@yahoo.com.ar - facebook: www.facebook.com/marta.fama.3</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4482344,-34.6076936,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Mata Galan</strong><p><strong>address:</strong> Sánchez de Bustamante 551, Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6360-5641 - email: milongamatagalan@gmail.com - facebook: www.facebook.com/LaMataGalan</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4143402,-34.6033827,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cheek to Cheek</strong><p><strong>address:</strong> Avenida Córdoba 5064, Buenos Aires, Argentina<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-5885-7432 or +54-911-4027-8879 - email: cheektocheektango@gmail.com - facebook: www.facebook.com/cheekto.cheek.73?fref=ts</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4349918,-34.5911285,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Oliverio</strong><p><strong>address:</strong> Vera 574, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6361-9486 or +54-911-5924-8818 - email: coloscar@gmail.com - facebook: www.facebook.com/100007673742241</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4384475,-34.5985401,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Porteño y Bailarin</strong><p><strong>address:</strong> Riobamba 416, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5153-8626 - email: juancarlosstasi@gmail.com - facebook: www.facebook.com/carlos.stasi</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3939845,-34.6041776,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Catedral</strong><p><strong>address:</strong> Sarmiento 4006, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-5925-2150- email: lacatedraldealmagro@yahoo.com.ar - facebook: www.facebook.com/pages/LA-CATEDRAL-CLUB/118477378190528</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4214842,-34.6049057,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Seguime...si podes</strong><p><strong>address:</strong> Loyola 828, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:30 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3393-9823 - email: maritelujan@gmail.com - facebook: www.facebook.com/seguimesipodesmilonga</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4381567,-34.5932713,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Maldita Milonga</strong><p><strong>address:</strong> Perú 571, Buenos Aires, Argentina<br /><strong>time:</strong> 22:30 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-2189-7747 or +54-911-6567-3334 - email: benditaymalditamilonga@gmail.com - facebook: www.facebook.com/malditamilonga1</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3746207,-34.6146278,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Mandrilera</strong><p><strong>address:</strong> Humberto Primo 2758, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:30 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5061-9502 - email: lamandrilera@gmail.com - facebook: www.facebook.com/220424538118944</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.403361,-34.622706,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Viruta Tango Club</strong><p><strong>address:</strong> Armenia 1366, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:45 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4775-0160 or +54-911-2616-1122 - email: info@lavirutatangoclub.com - facebook: www.facebook.com/LaVirutaTangoClub</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4308458,-34.591574,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Nuevo Chique</strong><p><strong>address:</strong> San José 224, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-4428-0100 - email: nuevochique@gmail.com - facebook: www.facebook.com/marcela.pazos.7?fref=ts</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3861195,-34.6113859,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Flores del Pial</strong><p><strong>address:</strong> Cnel. Ramón L. Falcon 2750, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 18:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4612-4257 - facebook: www.facebook.com/milongadeflores.delpial?fref=ts</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4671107,-34.6316515,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Lujos</strong><p><strong>address:</strong> Riobamba 416, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 18:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6111-1138 or +54-911-4199-5902 - email: oscarkotik@live.com.ar - facebook: www.facebook.com/100006230370407</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3939845,-34.6041776,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Berretin</strong><p><strong>address:</strong> Nogoyá 5228, Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 22:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: haqueirolo@hotmail.com - facebook: www.facebook.com/gustavoleila.tango?hc_ref=ARTPyJld0QemPbuotkt3R_Ev6CGWtpWKTW3UsO5tVWaVKXaEBO_ZOPrcP<br></p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.5168509,-34.6188553,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de las Barrancas</strong><p><strong>address:</strong> Echeverría 1800, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3567 6819 - email: melbacarrion@gmail.com - facebook: www.facebook.com/lamilongadelasbarrancas</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4502107,-34.5595959,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>De Querusa</strong><p><strong>address:</strong> Carlos Calvo 3745Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 00:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6677-3342 or +51-911-5509-8728 - email: pabloynoelia@gmail.com - facebook: www.facebook.com/dequerusapractica</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4185437,-34.6244583,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Cachila</strong><p><strong>address:</strong> La Rioja 1180, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4903-3551 or +54-911-6724-7359 - email: lacachilatango@gmail.com - facebook: www.facebook.com/daniel.rezk.3</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4070027,-34.6239362,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Abrazarte - Practitango al Funyi</strong><p><strong>address:</strong> Loyola 828, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4903-3551 or +54-11-6414-2273 - email: estango86@yahoo.com.ar - facebook: www.facebook.com/estela.bernal3?fref=ts</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4381567,-34.5932713,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>HyCTango</strong><p><strong>address:</strong> Avenida Eva Perón 1400, Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +54-911-4097-9759 - email: hyctango@yahoo.com.ar - facebook: www.facebook.com/hyctango.tango</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4444124,-34.6327784,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Abrazando Tangos</strong><p><strong>address:</strong> Avenida Álvarez Thomas 1121, 3A, Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5423-6280 or +54-911-4430-7329 - email: abrazandotangos@hotmail.com - facebook: www.facebook.com/abrazandotangos/?fref=ts</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4576265,-34.5783358,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Marabu</strong><p><strong>address:</strong> Maipú 365, Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3379-5979 - facebook: www.facebook.com/salonmarabu</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3765831,-34.6039059,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga El Pachu</strong><p><strong>address:</strong> Avenida Entre Ríos 1056, Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 01:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5737-2686 or +54 9 11 5015-4342 - email: elpuchutango@gmail.com - facebook: www.facebook.com/1605721378</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.391522,-34.6210809,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Abrazo Tango Club</strong><p><strong>address:</strong> Riobamba 416, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 14:30 - 20:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5515-1427 - email: elabrazo@elabrazotango.com - facebook: www.facebook.com/1149727057</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3939845,-34.6041776,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cheek to Cheek</strong><p><strong>address:</strong> Avenida Córdoba 5064, Buenos Aires, Argentina<br /><strong>time:</strong> 15:00 - 19:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-5885-7432 or +54-911-4027-8879 - email: cheektocheektango@gmail.com - facebook: www.facebook.com/cheekto.cheek.73?fref=ts</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4349918,-34.5911285,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Bs As</strong><p><strong>address:</strong> Avenida Entre Ríos 1056, Buenos Aires, Argentina<br /><strong>time:</strong> 18:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3205-0055 or +54-911-3876-6957 - email: carlosgallegoproducciones@hotmail.com - facebook: www.facebook.com/milongadeBsas</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3914542,-34.6210335,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Plaza Pappo</strong><p><strong>address:</strong> Avenida Juan B. Justo 4800, Buenos Aires, Argentina<br /><strong>time:</strong> 19:00 - 22:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3119-1449 - email: fenixdan21@gmail.com - facebook: www.facebook.com/profile.php?id=100013208946393</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.466613,-34.6108569,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Centro Anchorena</strong><p><strong>address:</strong> Doctor Tomás Manuel de Anchorena 441, Buenos Aires, Argentina<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: Carmendefazio_tango@hotmail.com - facebook: www.facebook.com/carmenjesustango</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4105616,-34.6047957,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Bailongo de la Glorieta</strong><p><strong>address:</strong> Echeverría 1800, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-4938-5645 - email: petcheverry@hotmail.com - facebook: www.facebook.com/pablo.etcheverry.77</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4501612,-34.5596132,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Milonguera</strong><p><strong>address:</strong> Teniente General Juan Domingo Perón 1878, Buenos Aires, Argentina<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-2320-5691 or +54-911-6044-0400 - email: mansillapaul95@gmail.com - facebook: www.facebook.com/etbabook</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3931909,-34.6069601,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de Norma</strong><p><strong>address:</strong> Av Juan Bautista Alberdi 436, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3924-1303 or +54-911-4661-8040 - email: lamilongadenorma@gmail.com - facebook: www.facebook.com/norma.fonseca.509?fref=ts</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4337434,-34.6231552,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Patio de Tango</strong><p><strong>address:</strong> Perú 272, Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +54-911-4079-0683 or 1+54-911-5568-0943 - email: iamatriain@yahoo.com - facebook: www.facebook.com/Patio-de-Tango-Manzana-de-las-Luces-450757248455996</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3747506,-34.6110975,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La milonga de la Uni en el CETBA</strong><p><strong>address:</strong> Agrelo 3231, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 30$AR<br /><strong>contact:</strong> phone: +54-11-4957-1382 - email: cnf.cetba@bue.edu.ar - facebook: www.facebook.com/LaMilongaDeLaUni</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4115225,-34.6178271,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Yumba de Dorita</strong><p><strong>address:</strong> Av Rivadavia 8619, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:30 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6117-1645 - email: lolatango1@gmail.com - facebook: www.facebook.com/layumbadedorita</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4852362,-34.6342271,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Abracatango Milonga</strong><p><strong>address:</strong> Deán Funes 2045, Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-5903-4035 - email: J_p_apro@hotmail.com - facebook: www.facebook.com/abracatangomilonga</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.404295,-34.6349303,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Vuelta</strong><p><strong>address:</strong> La Rioja 1180, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-4403-6999 - email: clelyrugnone@yahoo.com.ar - facebook: www.facebook.com/CLELYRUGNONE5gmail</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4070432,-34.623978,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Las Malevas</strong><p><strong>address:</strong> Tucumán 3428, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-3023-4038 - email: marianadragone@gmail.com - facebook: www.facebook.com/groups/314106785266569</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4140887,-34.5999791,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Catedral</strong><p><strong>address:</strong> Sarmiento 4006, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-5925-2150 - email: lacatedraldealmagro@yahoo.com.ar - facebook: www.facebook.com/pages/LA-CATEDRAL-CLUB/118477378190528 - Twitter: @lacatedralclub</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.421544,-34.604915,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Dos Orillas</strong><p><strong>address:</strong> Avenida Álvarez Thomas 1121, 3A, Buenos Aires, Argentina<br /><strong>time:</strong> 23:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6178-5111 or +54-911-6723-3960 - email: dosorillas.tango@gmail.com - facebook: www.facebook.com/Dos-Orillas-Practica-de-Tango-932027866856108</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4577048,-34.5783252,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Parakultural</strong><p><strong>address:</strong> Avenida Raúl Scalabrini Ortiz 1331, Buenos Aires, Argentina<br /><strong>time:</strong> 23:30 - 05:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4833-3224 or +54-911-5738-3850 - email: omarviola@hotmail.com - web: www.parakultural.com.ar</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4275353,-34.5927669,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Baldosa</strong><p><strong>address:</strong> Cnel. Ramón L. Falcon 2750, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4601-7988 or +54-11-4574-1593 - email: horaciotango@hotmail.com - facebook: www.facebook.com/100000243441659</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4671107,-34.6316515,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Marshall (Gay-friendly)</strong><p><strong>address:</strong> Cnel. Ramón L. Falcon 2750, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:00 - 03:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-5458-3423 or +54-5368-9219 - email: augustotango@hotmail.com - facebook: www.facebook.com/la.marshall.milonga</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3939845,-34.6041776,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Yira Yira</strong><p><strong>address:</strong> Humberto Primo 1462, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5021-4685 or +54-911-3359-67109219 - email: yirayiramilonga@gmail.com - facebook: www.facebook.com/ana.bocutti.7</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.386537,-34.62124,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>ZUM</strong><p><strong>address:</strong> Humberto Primo 1462, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:30 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6284-4568 - email: zumtango@gmail.com - web: www.zumtango.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4349918,-34.5911285,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Viruta Tango Club</strong><p><strong>address:</strong> Armenia 1366, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:30 - 06:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4775-0160 or +54-911-2616-1122 - email: info@lavirutatangoclub.com - facebook: www.facebook.com/LaVirutaTangoClub</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4307754,-34.5915657,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Elisa Danza</strong><p><strong>address:</strong> Calle del Postigo Bajo, 26, 33009 Oviedo, Asturias, Spain<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 985086220 email: elisadanza@telefonica.net </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-5.8404317,43.3614401,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Tortazo</strong><p><strong>address:</strong> Av. Conde Rudi, 29600 Marbella, Málaga, Spain<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-4.9081515,36.5108585,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga Que Faltaba</strong><p><strong>address:</strong> Calle Buitrago, 2, 29601 Marbella, Málaga, Spain<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.8857774,36.5105402,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica milonguera</strong><p><strong>address:</strong> Paseo Antonio Pérez Goyena, 31008 Pamplona, Navarra, Spain<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 608 25 25 41 </p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-1.6750087,42.8092074,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Mis Amores</strong><p><strong>address:</strong> Av. Onze de Setembre, 38, 08208 Sabadell, Barcelona, Spain<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> 6€<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.1057166,41.5527586,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Viejo Barrio</strong><p><strong>address:</strong> Calle de los Sueños, 27, 38006 Santa Cruz de Tenerife, Spain<br /><strong>time:</strong> 19:00 - 22:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 922279932 / 617213105 email: centrogalegotenerife@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-16.2614612,28.469237,0],
"type": "Point"
}
},					
{
"type": "Feature",
"properties": {
"description": "<strong>La Posada Del Comendador</strong><p><strong>address:</strong> Calle de los Predicadores, 70, 50003 Zaragoza, Spain<br /><strong>time:</strong> 23:00 - 2:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 636542867</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-0.8859076,41.6576429,0],
"type": "Point"
}
},					
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonguita de Terrassa</strong><p><strong>address:</strong> Carrer de Sant Francesc, 50, 08221 Terrassa, Barcelona, Spain<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 937808217</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.0129607,41.5584212,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La milonga de la Lola</strong><p><strong>address:</strong> Calle Jiménez Aranda, 5, 41018 Sevilla, Spain<br /><strong>time:</strong> 21:30 - 00:30 <br /><strong>price:</strong> 3 €<br /><strong>contact:</strong> phone: 649364991- email: casadeltango.sevilla@gmail.com - facebook: www.facebook.com/casadeltangosevilla</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9811818,37.3872786,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La milonga de la placita</strong><p><strong>address:</strong> Pl. del Museo, 41001 Sevilla, Spain<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 649364991 - email: casadeltango.sevilla@gmail.com - facebook: www.facebook.com/casadeltangosevilla</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9992989,37.3926659,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El corazón de la milonga</strong><p><strong>address:</strong> Calle Jiménez Aranda, 5, 41018 Sevilla, Spain<br /><strong>time:</strong> 22:00 - 01:00 <br /><strong>price:</strong> 4 €<br /><strong>contact:</strong> phone: 617727443 - email: elcorazondelamilongasevilla@gneraeventos.com - facebook: www.facebook.com/elcorazondelamilongasevilla</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9811818,37.3872786,0],
"type": "Point"
}
},
{
"type": "Features",
"properties": {
"description": "<strong>Milonga estación transitango</strong><p><strong>address:</strong> Plaza la Legión, s/n, 41001 Sevilla, Spain<br /><strong>time:</strong> 22:00 - 01:30 <br /><strong>price:</strong> 5 €<br /><strong>contact:</strong> phone: 630330023 - email: info@alejandrasabena.com - facebook: www.facebook.com/milongatransitango</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-6.0026898,37.3916741,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La milonga del sabado</strong><p><strong>address:</strong> Calle Alessandro Volta, 1, 41300 San José de la Rinconada, Sevilla, Spain<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 5 €<br /><strong>contact:</strong> phone: 646426235 - email: milongadelsabado@gmail.com - facebook: www.facebook.com/LaMilongadelSabado</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9333291,37.4883432,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga la baldosita</strong><p><strong>address:</strong> Plaza Pelícano, 2, 41003 Sevilla, Spain<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> 6 €<br /><strong>contact:</strong> phone: 618463605 - email: info@tangokills.com - facebook: www.facebook.com/tangosevilla</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-5.9841689,37.3975477,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga el enrosque</strong><p><strong>address:</strong> Av. la Cruz Roja, 62, 41008 Sevilla, Spain<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 5 €<br /><strong>contact:</strong> phone: 678946683 - facebook: www.facebook.com/elenrosque.sevilla</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-5.9811335,37.402485,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Práctica sur</strong><p><strong>address:</strong> Av. Blas Infante, 2, 41011 Sevilla, Spain<br /><strong>time:</strong> 19:00 - 21:15 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 655090744 - website: www.practicasursevilla.es</p>",
"business":"practica",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-6.0055381,37.3736386,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practilonga la tardecita</strong><p><strong>address:</strong> Calle Sánchez Perrier, 3, 41009 Sevilla, Spain<br /><strong>time:</strong> n/a <br /><strong>price:</strong> 4 €<br /><strong>contact:</strong> facebook: www.facebook.com/PractilongaLaTardecita/timeline</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-5.9860033,37.4025948,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga en La Bóveda</strong><p><strong>address:</strong> Calle de los Predicadores, 70, 50003 Zaragoza, Spain<br /><strong>time:</strong> 23:00 - 2:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 636542867 - email: nacemi@hotmail.es</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-0.8859076,41.6576429,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Para Mover los Pies</strong><p><strong>address:</strong> Av Córdoba 5942, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: paramoverlospies@yahoo.com - facebook: www.facebook.com/Para-mover-los-Pies-1935326270055281</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4440503,-34.5851934,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Rumbo al Sur</strong><p><strong>address:</strong> Av Gral. Iriarte 2300, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +54-1194303-3393 - email: dorisbennan@gmail.com - facebook: www.facebook.com/BarLosLaureles</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3796041,-34.6494435,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga del Chau Che Clu</strong><p><strong>address:</strong> Avenida Velez Sarsfield 222, Buenos Aires, Argentina<br /><strong>time:</strong> 22:30 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-5383-2080 - email: chauchebuenosaires@gmail.com - facebook: www.facebook.com/chaucheclu</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.390014,-34.6367852,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga el Batacazo</strong><p><strong>address:</strong> Avenida Medrano 627, Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: suyaiserpa@hotmail.com - facebook: www.facebook.com/Milonga-EL-Batacazo--412286072484176</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.420746,-34.6027429,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Catedral</strong><p><strong>address:</strong> Sarmiento 4006, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-5925-2150 - email: lacatedraldealmagro@yahoo.com.ar - facebook: www.facebook.com/pages/LA-CATEDRAL-CLUB/118477378190528 - twitter: @lacatedralclub</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4216183,-34.6049278,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>After Office Tango en Amapola</strong><p><strong>address:</strong> Montevideo 421, Buenos Aires, Argentina<br /><strong>time:</strong> 22:15 - 03:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +54-911-4165-3180 - email: afrossasco@yahoo.com.ar - facebook: www.facebook.com/MilongaLaAmapola</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.3893132,-34.6039503,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Federal</strong><p><strong>address:</strong> Hipólito Yrigoyen 1440, Buenos Aires, Argentina<br /><strong>time:</strong> 22:30 - 01:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +54-911-4381-5269 or +54-911-3251-0714 - email: cpc.elhormiguero@gmail.com - facebook: www.facebook.com/CPCElHormiguero</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.38679,-34.610022,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de los Zucca</strong><p><strong>address:</strong> Humberto Primo 1462, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 22:30 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-911-6973-9027 - email: roberto_zuccarino@hotmail.com - facebook: www.facebook.com/La-Milonga-de-Los-Zucca-128305531096560</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.386537,-34.62124,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Archibrazo</strong><p><strong>address:</strong> Mario Bravo 441, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:00 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4864-6412 - email: rodrigotigalo@hotmail.com - facebook: www.facebook.com/archibrazotango</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4168706,-34.6048681,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Garufa</strong><p><strong>address:</strong> Campichuelo 472, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 23:00 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4951-0599 - email: zozedurden@yahoo.com.ar - facebook: www.facebook.com/636058229821046</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4352304,-34.6118905,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Viruta Tango Club</strong><p><strong>address:</strong> Armenia 1366, Ciudad Autonoma de Buenos Aires, Argentina<br /><strong>time:</strong> 00:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +54-11-4775-0160 or +54-911-2616-1122 - email: info@lavirutatangoclub.com - facebook: www.facebook.com/LaVirutaTangoClub</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.4307593,-34.5916276,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonguera</strong><p><strong>address:</strong>Via Giove 11, 91100 Trapani Trapani, Italy<br /><strong>time:</strong>22:00 - 03:00<br /><strong>price:</strong> n/a;<br /><strong>contact:</strong> facebook: www.facebook.com/raulagatatango</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},					 
"geometry": {
"coordinates": [12.560891,38.006652],
"type": "Feature"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica La Milonguera</strong><p><strong>address:</strong>Via Giove 11, 91100 Trapani Trapani, Italy<br /><strong>time:</strong>21:00 - 22:00<br /><strong>price:</strong> n/a;<br /><strong>contact:</strong> facebook: www.facebook.com/raulagatatango</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},					 
"geometry": {
"coordinates": [12.560891,38.006652],
"type": "Feature"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Teatro</strong><p><strong>address:</strong> Av. de San Sebastián, 152, 38005 Santa Cruz de Tenerife, Spain<br /><strong>time:</strong> 19:30 - 23:00 <br /><strong>price:</strong> 8€<br /><strong>contact:</strong> email: sylvain.mastrogiovanni@gmail.com - website: http://la-milonga-del-teatro.webnode.fr</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-16.2598304,28.4638373,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Teatro</strong><p><strong>address:</strong> Calle Dr. Jose Naveiras, 24A, 38001 Santa Cruz de Tenerife, Spain<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> email: sylvain.mastrogiovanni@gmail.com - website: http://la-milonga-del-teatro.webnode.fr</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-16.2530438,28.4733667,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga <br>Tanteas Tango Segovia</strong><p><strong>address:</strong> 601 KM La Granja de San Ildefonso, Ctra. Segovia, 3, 40193 Palazuelos de Eresma, Segovia, Spain<br /><strong>time:</strong> 22:15 - 00:45 <br /><strong>price:</strong> 8 €<br /><strong>contact:</strong> phone: 626 633 210 - email: info@tanteas.es - website: http://tanteas.es</p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-4.0593268,40.9102063,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Entretango Salamanca</strong><p><strong>address:</strong> Calle San Justo, 26, 37001 Salamanca, Spain<br /><strong>time:</strong> 22:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 616724784 - website: http://entretangosalamanca.blogspot.com.es/ - facebook: https://www.facebook.com/entretango.salamanca</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-5.6620462,40.9631312,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de mis Amores</strong><p><strong>address:</strong> Carrer d'Antoni Marquès, 23, 07003 Palma, Illes Balears, Spain<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 8 €<br /><strong>contact:</strong> phone: +34 676252389 - email: milongapalmaplay@gmail.com - facebook: https://www.facebook.com/pages/La-milonga-de-mis-amores/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [2.6500858,39.5789313,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Noche de la Morocha</strong><p><strong>address:</strong> Donau-City-Straße 2, 1220 Wien, Austria<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +43 (0) 699 1 944 20 62 - email: ale_rogel@yahoo.com - website: http://www.alejandrarogelalberdi.at/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.4152553,48.2330189,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga El Firulete</strong><p><strong>address:</strong> Bäckerstraße 16, 1010 Wien, Austria<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> 8 €<br /><strong>contact:</strong> phone: 06641736976  - email: nicolastango@hotmail.com - website: http://www.nicolastango.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.3767176,48.208773,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Crossover Milonga<br></strong><p><strong>address:</strong> Gonzagagasse 14, 1010 Wien, Austria<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: http://www.crossovermilonga.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.3704372,48.2154742,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Galeria Ideal</strong><p><strong>address:</strong> Geibelgasse 14-16, 1150 Wien, Austria<br /><strong>time:</strong> 21:00- 00:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: matjatt@galeria-ideal.at - website: http://www.galeria-ideal.at/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.3324345,48.1892188,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tangobar</strong><p><strong>address:</strong> Schwarzspanierstraße 13, 1090 Wien, Austria<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> 8 €<br /><strong>contact:</strong> phone: 069918146624 - email: tango@tangobar.at- website: http://www.tangobar.at/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.3568738,48.2165479,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Neolonga</strong><p><strong>address:</strong> Rahlgasse 5, 1060 Wien, Austria<br /><strong>time:</strong> 21:00 to 00:30 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> phone: 004367683054648 </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.3609455,48.2016571,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Gitana</strong><p><strong>address:</strong> Ungargasse 8, 1030 Wien, Austria<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> t5€<br /><strong>contact:</strong> phone: +43(0)67683054648</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.385765,48.2035915,0],
"type": "Point"
}
},

{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Istanbul</strong><p><strong>address:</strong> Rahlgasse 5, 1060 Wien, Austria<br /><strong>time:</strong> t21:00 - 00:30 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> phone: 004367683054648</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.3609455,48.2016571,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Milonguero</strong><p><strong>address:</strong> Kaiser-Josef-Platz 4, 9500 Villach, Austria<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 0676 5627755 - email: milongueroinfo@aon.at - website: http://www.milonguero.info/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [13.8450504,46.6151997,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Tangueria</strong><p><strong>address:</strong> Jette-Jetse steenweg 239, 1080 Sint-Jans-Molenbeek, Belgium<br /><strong>time:</strong> 16:00 - 21:00 <br /><strong>price:</strong> 6,5 €<br /><strong>contact:</strong> phone: 32/(0)2/345.68.91 - email: info@marisayoliver.com - website: http://www.marisayoliver.com/en.workshopsandballs.html</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [4.333553,50.8633044,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cafe Dansant</strong><p><strong>address:</strong> Fortstraat, 1060 Sint-Gillis, Belgium<br /><strong>time:</strong> 16:00 - 21:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 32/(0)2/541.01.70 - email: DePianofabriek@vgc.be - website: http://www.giselaysergio.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [4.3422132,50.8281982,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Ball @ Jardins du Dieweg</strong><p><strong>address:</strong> Dieweg 69, 1180 Ukkel, Belgium<br /><strong>time:</strong> 16:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +32/(0)486 464 222 - email: tangodieweg@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [4.3426797,50.7939917,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Bar</strong><p><strong>address:</strong> Rue de Dublin 13, 1050 Ixelles, Belgium<br /><strong>time:</strong> 21:30 - 03:00 <br /><strong>price:</strong> 6,5 €<br /><strong>contact:</strong> phone: 0497028543 - email: almadeltango@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [4.3669488,50.8370265,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cité Culture</strong><p><strong>address:</strong> Robijnstraat, 1020 Brussel, Belgium<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: eugeniaramirezmiori@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [4.3198066,50.8940748,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Centre Culturel de Schaerbeek</strong><p><strong>address:</strong> Rue de Locht 91, 1030 Schaerbeek, Belgium<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: eugeniaramirezmiori@gmail.com - website: http://www.tangoargentino.be/fr/agenda.htm</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [4.3718638,50.8637411,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Intercult77</strong><p><strong>address:</strong> Avenue Ducpétiaux 133A, 1060 Saint-Gilles, Belgium<br /><strong>time:</strong> 21:30 - 04:00 <br /><strong>price:</strong> t6,5 €<br /><strong>contact:</strong> phone: 0497599163 - email: cellule133a@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [4.3453863,50.8223619,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Bar</strong><p><strong>address:</strong> Rue de Dublin 13, 1050 Ixelles, Belgium<br /><strong>time:</strong> 22:00 - 01:00 <br /><strong>price:</strong> 2,5 €<br /><strong>contact:</strong> phone: 0497028543 - email: almadeltango@hotmail.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [4.3669488,50.8370265,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Dansant</strong><p><strong>address:</strong> Kerkstraat 9, 1851 Grimbergen, Belgium<br /><strong>time:</strong> 16:00 - 21:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 32/(0)2/270.37.0 - email: uniontango@yucom.be - website: http://www.giselaysergio.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [4.3811014,50.9683091,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga+Matine La Galeria</strong><p><strong>address:</strong> ul. 'Ivan Denkoglu' 30, 1000 Sofia Center, Sofia, Bulgaria<br /><strong>time:</strong> 16:00 - 20:00 <br /><strong>price:</strong> 8 lv<br /><strong>contact:</strong> email: info@tango.bg - website: http://www.tango.bg/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [23.3200961,42.6938341,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Before & After</strong><p><strong>address:</strong> ul. 'Hristo Belchev' 12, 1000 Sofia, Bulgaria<br /><strong>time:</strong> 19:30 - 22:30 <br /><strong>price:</strong> 3 lv<br /><strong>contact:</strong> phone: +359 88 7383574 - email: sofiatango@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.3212647,42.6932775,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Saloon</strong><p><strong>address:</strong> ul. 'George Washington' 8, 1000 Sofia Center, Sofia, Bulgaria<br /><strong>time:</strong> t22:00 - 03:00 <br /><strong>price:</strong> 2.5 €<br /><strong>contact:</strong> phone: +359 888 232 995 - email: info@laokoontango.com - website: http://www.laokoontango.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.321486,42.7006033,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Salsa Plus Club</strong><p><strong>address:</strong> ul. Aksakov 31, 1000 Sofia Center, Sofia, Bulgaria<br /><strong>time:</strong> 19:30 - 22:30 <br /><strong>price:</strong> t3 lv<br /><strong>contact:</strong> phone: +359 2 9804080 - email: sofiatango@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.3336354,42.6925782,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga DaDa Cultural</strong><p><strong>address:</strong> ul. 'Georgi Benkovski' 10, 1000 Sofia Center, Sofia, Bulgaria<br /><strong>time:</strong> 20:30 - 02:00 <br /><strong>price:</strong> 2.5 EUR<br /><strong>contact:</strong> phone: +359895932177 - email: estefyadrian@gmail.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.329258,42.6969883,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Social</strong><p><strong>address:</strong> pl. 'Petko R. Slaveykov' 4, 1000 Sofia Center, Sofia, Bulgaria<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> 5 lv<br /><strong>contact:</strong> phone: +359 882 533282 - email: info@tanguerin.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [23.3244215,42.6915473,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonguita</strong><p><strong>address:</strong> Ul. Ivana Mažuranica 32, 23000, Zadar, Croatia<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 385 98 650 873 - email: info@tangozadar.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [15.2333587,44.12032,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Sentimental</strong><p><strong>address:</strong> Zrinjevac 15, 10000, Zagreb, Croatia<br /><strong>time:</strong> 21:30 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 385915347386 - email: info@tangoargentino.hr</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [15.9776129,45.8099602,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Compadre</strong><p><strong>address:</strong> Gajeva ul. 25, 10000, Zagreb, Croatia<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: tangocroatia@yahoo.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [15.9766813,45.809714,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Old Pharmacy Pub</strong><p><strong>address:</strong> Ul. Andrije Hebranga 11a, 10000, Zagreb, Croatia<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 098/9502738 - email: argentina.tango@yahoo.com - facebook: https://www.facebook.com/libertango.croatia.1</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [15.9749572,45.808991,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Almacen</strong><p><strong>address:</strong> Urheilukatu 24, 00250 Helsinki, Finland<br /><strong>time:</strong> 20:00 - 00:30 <br /><strong>price:</strong> 5 €<br /><strong>contact:</strong> phone: +358400512051 - email: info@tango.fi- website: http://www.tango.fi/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [24.9211296,60.1872141,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>TABA</strong><p><strong>address:</strong> Kastanievej 2, 1876 Frederiksberg C, Denmark<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong>  email: info@tabatango.dk - website: http://www.tabatango.dk/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [12.5430133,55.6781512,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga i Tingluti</strong><p><strong>address:</strong> Kapelvej 46C, 2200 København N, Denmark<br /><strong>time:</strong> 21:00 - 01:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 28 22 29 55</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [12.550795,55.686468,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Boca Tangosalon</strong><p><strong>address:</strong> Dronningensgade 34, 1420 København K, Denmark<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> 50 kr<br /><strong>contact:</strong> phone: 2361 1333 - email: gunner@tangodelnorte.dk</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [12.590926,55.671254,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Grobovka</strong><p><strong>address:</strong> Havlíckovy sady 58/2, 120 00 Praha 2-Vinohrady, Czechia<br /><strong>time:</strong> 19:30 - 23:15 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +420-602 232 650 - email: milong@email.cz</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [14.4449612,50.0692597,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Jam Milonga</strong><p><strong>address:</strong> Národní 25, 110 00 Praha 1-Staré Mesto, Czechia<br /><strong>time:</strong> 21:15 - 00:00 <br /><strong>price:</strong> CZK 50<br /><strong>contact:</strong> email: info@tangoimperial.cz</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [14.418509,50.08258,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Zlatá Lyra</strong><p><strong>address:</strong> Michalská 440/11, Staré Mesto, 110 00 Praha-Praha 1, Czechia<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: marek@caminito.cz</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [14.4197147,50.0850627,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Habanita club (on hold - Not verified)</strong><p><strong>address:</strong> Peristeriou 6, Thessaloniki 546 25, Greece<br /><strong>time:</strong> 23:00 - 01:00 <br /><strong>price:</strong> 6 €<br /><strong>contact:</strong> phone: +30 6974 102502 - email: info@pablitoyanastacia.gr - website: http://www.pablitoyanastacia.gr/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [22.9370033,40.6356989,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Universidad (on hold - Not verified)<br></strong><p><strong>address:</strong> 713 00, Athina Palace, Iraklio 00, Greece<br /><strong>time:</strong> 22:30 - 02:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +306937064444 - email: nikdal@gmail.com- website: http://dance.culture.uoc.gr/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [25.0827584,35.3073431,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Sur (on hold - Not verified)</strong><p><strong>address:</strong> 713 05, Monis Kalivianis 8, Iraklio 713 05, Greece<br /><strong>time:</strong> 22:30 - 02:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +306937064444 - email: tango@tangoneon.gr- website: http://www.tangoneon.gr/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [25.1327279,35.320076,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga sto Limani (on hold - Not verified)</strong><p><strong>address:</strong> 25is Avgoustou 25, Iraklio 712 02, Greece<br /><strong>time:</strong> t22:00 - 01:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 0030 6949 215 320 - email: tango_sto_limani@yahoo.com- website: http://www.cretetango.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [25.1340859,35.3411814,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga in Retro Nuevo bar (on hold - Not verified)</strong><p><strong>address:</strong> El. Venizelou 13, Kerkira 491 00, Greece<br /><strong>time:</strong> 21:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +30 6944 66 75 26 - email: fuegodeltango@yahoo.gr</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.91599,39.6256099,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Aires (on hold - Not verified)</strong><p><strong>address:</strong> Agiou Dimitriou 13, Athina 105 54, Greece<br /><strong>time:</strong> 22:00 - 00:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +30 699 842 0566 - email: stathis_tsi@yahoo.gr</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.7255136,37.9791876,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Pasional (on hold - Not verified)</strong><p><strong>address:</strong> Pesmazoglou 5, Athina 105 64, Greece<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> t10 €<br /><strong>contact:</strong> phone: 0302108831280 - email: milongapasional2000@yahoo.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.7311583,37.9811385,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Mi Tango Querido (on hold - Not verified)</strong><p><strong>address:</strong> Iakchou 22, Athina 118 54, Greece<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> t10 €<br /><strong>contact:</strong> phone: +30 694 257 8858</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.7121378,37.9777456,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Libertango (on hold - Not verified)</strong><p><strong>address:</strong> Dimitrakopoulou 106, Athina 117 41, Greece<br /><strong>time:</strong> 22:30 - 00:00 <br /><strong>price:</strong> 10 €<br /><strong>contact:</strong> phone: +30 210 921 4404 - email: info@libertango.gr</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.7213855,37.9628389,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Vanguardia (on hold - Not verified)</strong><p><strong>address:</strong> Kolokotroni 6, Athina 105 61, Greece<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +30 693 710 6972 - email: carlosmariatango@yahoo.com- website: http://milonga-la-vanguardia.blogspot.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.731664,37.9775768,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Che Buenos Aires (on hold - Not verified)</strong><p><strong>address:</strong> Agiou Dimitriou 13, Athina 105 54, Greece<br /><strong>time:</strong> 22:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +30 687 177 4653 - email: arielfuhr@yahoo.com.ar</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.7255136,37.9791876,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Portena (on hold - Not verified)</strong><p><strong>address:</strong> Dekeleon 46, Athina 118 54, Greece<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> t10 €<br /><strong>contact:</strong> phone: +30 697 324 309 - website: http://www.feliotango.gr/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.7097022,37.9763092,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Uránia</strong><p><strong>address:</strong> Budapest, Rákóczi út 21, 1088 Hungary<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> 800 Ft<br /><strong>contact:</strong> phone: 06706012823 - email: pstefani@hotmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.065078,47.495274,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Fészek</strong><p><strong>address:</strong> Budapest, Kertész u. 36, 1073 Hungary<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> t800 Ft<br /><strong>contact:</strong> phone: t06706012823 - email: pstefani@hotmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.0660314,47.5011615,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Transit Milonga</strong><p><strong>address:</strong> Budapest, Paulay Ede u. 41, 1061 Hungary<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 800 Ft<br /><strong>contact:</strong> phone: 06203370055 - email: transittango@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.0600525,47.5019834,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Villa Milonga</strong><p><strong>address:</strong> Budapest, Múzeum u. 7, 1088 Hungary<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> 800 Ft<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.0632662,47.490213,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Open Air - nyíltszíni milonga a Várban</strong><p><strong>address:</strong> Várpalota, Budai Nagy Antal u., 8100 Hungary<br /><strong>time:</strong> 21:00 - 23:30 <br /><strong>price:</strong> 400 Ft<br /><strong>contact:</strong> phone: 06203370055 - email: transittango@gmail.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.03361,47.50161,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tango y Alma</strong><p><strong>address:</strong> Budapest, Bem rkp. 6, 1011 Hungary<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> 1000HUF <br /><strong>contact:</strong> phone: 06203771214 - email: koppergotz@yahoo.co.uk - website: http://www.tangoyalma.hu/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.040041,47.500565,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Mucsarnok Milonga</strong><p><strong>address:</strong> Budapest, Dózsa György út 37, 1146 Hungary<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> 1200 Ft<br /><strong>contact:</strong> phone: 06209431614 - email: mucsarnokmilonga@gmail.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [19.078653,47.5140496,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Wynn's Hotel</strong><p><strong>address:</strong> 35-39 Abbey Street Lower, North City, Dublin 1, D01 C9F8, Ireland<br /><strong>time:</strong> 21:45 - 00:00 <br /><strong>price:</strong> 5 €<br /><strong>contact:</strong> phone: 0872292463 - email: monitango@yahoo.com.ar</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-6.258688,53.348342,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga in Kanepes Centre</strong><p><strong>address:</strong> Skolas iela 15, Centra rajons, Riga, LV-1010, Latvia<br /><strong>time:</strong> 17:00 - 21:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> phone: +371-26344457 - email: aigars@tangostudio.lv- website: http://www.tangostudio.lv/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [24.1189816,56.9581079,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Restaurant Sarkans</strong><p><strong>address:</strong> Stabu iela 10, Centra rajons, Riga, LV-1010, Latvia<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +371 26344457 - email: aigars@tangostudio.lv</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [24.1233268,56.9585835,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Ideal</strong><p><strong>address:</strong> Palac Schaffgotschów, Kosciuszki 34, 50-012 Wroclaw, Poland<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> 10 PLN<br /><strong>contact:</strong> phone: 691 876 301- email: irek.michalewicz@gmail.com- website: http://www.tangoki.eu/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [17.0338814,51.102733,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Opera Nova</strong><p><strong>address:</strong> Marszalka Focha 5, Bydgoszcz, Poland<br /><strong>time:</strong> t18:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 0048 666838198 - email: tomelki@op.pl - website: http://www.restauracjamaestra.pl/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [17.9977812,53.1244017,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Elegante</strong><p><strong>address:</strong> Dluga 44/50, 00-241 Warszawa, Poland<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> 15 PLN<br /><strong>contact:</strong> phone: +48 501 372 170 - email: milonga@milonga.pl - website: http://www.milonga.pl/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [21.0039365,52.2464855,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga: Chlodna St</strong><p><strong>address:</strong> Chlodna 3, Warszawa, Poland<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> 15 PLN<br /><strong>contact:</strong> phone: +48 502 701 920 - email: akademiatanga@wp.pl</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.993722,52.2384415,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Alternativa</strong><p><strong>address:</strong> Dluga 44/50, 00-241 Warszawa, Poland<br /><strong>time:</strong> 22:30 - 01:30 <br /><strong>price:</strong> 15 PLN<br /><strong>contact:</strong> phone: +48 501 372 170 - email: milonga@milonga.pl</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [21.0039365,52.2464855,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>'U Artystow' club</strong><p><strong>address:</strong> Mazowiecka 11A, Warszawa, Poland<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 15 PLN<br /><strong>contact:</strong> email: luiza@tango-arte.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [21.0126819,52.237742,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Salon de los Viernes</strong><p><strong>address:</strong> Korte Leidsedwarsstraat 12, 1017 RC Amsterdam, Netherlands<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> 5 €<br /><strong>contact:</strong> phone: 0614.529.434 - website: http://www.academiadetango.nl/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [4.8821159,52.3649382,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Salon Tangoland</strong><p><strong>address:</strong> De Wittenstraat 100, 1052 Amsterdam, Netherlands<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> 2 €<br /><strong>contact:</strong> phone: 020-6261863 - website: http://www.tangoland.nl/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [4.8781372,52.3830616,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Salon TangoDiep</strong><p><strong>address:</strong> Oostelijke Handelskade 44, 1019 BN Amsterdam, Netherlands<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 6 €<br /><strong>contact:</strong> phone: 020-616.30.60 - facebook: http://www.tangodiep.nl/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [4.9362512,52.3740464,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Migdolas</strong><p><strong>address:</strong> Savanoriu pr. 170, Kaunas 44149, Lithuania<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +370 698 28115 / +370 686 52593 - email: cesonis@hotmail.com - website: http://www.partneris.lt/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.927546,54.9080423,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Prospekto pub</strong><p><strong>address:</strong> Gedimino pr. 2, Vilnius 01103, Lithuania<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> t10 Lt<br /><strong>contact:</strong> phone: +370 685 30319 - email: info@tangoargentino.lt - website: http://www.tangoargentino.lt/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [25.2849832,54.6859824,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tangovivo - Zazhigalka</strong><p><strong>address:</strong> Nevsky avenue, 74-76, Sankt-Peterburg, Russia, 191025<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> 150 rub.<br /><strong>contact:</strong> phone: +73180244 - email: info@tangovivo.ru - website: http://tangovivo.ru/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [30.3464867,59.9334821,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Internazionaljnaya Milonga</strong><p><strong>address:</strong> embankment river Moyka, 40, Sankt-Peterburg, Russia, 191186<br /><strong>time:</strong> 19:30 - 01:00 <br /><strong>price:</strong> 300 rub<br /><strong>contact:</strong> phone: 89052541563 - email: edissa@mail.ru - website: http://www.edissa.spb.ru/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [30.320331,59.9372687,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Caminito</strong><p><strong>address:</strong> prospekt Engelsa, 111?1, Sankt-Peterburg, Russia, 194354<br /><strong>time:</strong> 20:00 - 00:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> website: http://www.caminito.spb.ru/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [30.324023,60.036388,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Friday Practica</strong><p><strong>address:</strong> Calea Calara?i 55, Bucure?ti 030167, Romania<br /><strong>time:</strong> 21:00 - 23:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: contact@tangobrujo.ro - website: http://www.tangobrujo.ro/</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [26.112125,44.432773,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Latino Coelho</strong><p><strong>address:</strong> R. Machado dos Santos 827, 4400-209 Vila Nova de Gaia, Portugal<br /><strong>time:</strong> 23:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 936007883 - email: fernando_jorge_as@yahoo.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-8.6319135,41.1257087,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Das Antas</strong><p><strong>address:</strong> R. Prof. Bento de Jesus Caraça 315, 4200-132 Porto, Portugal<br /><strong>time:</strong> t17:00 - 20:00 <br /><strong>price:</strong> 2,50 €<br /><strong>contact:</strong> phone: 914013206 - email: portotango@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-8.5978298,41.163933,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Corazon de Tango</strong><p><strong>address:</strong> Praceta da Aldeia Nova 191, 4460-282 Sra. da Hora, Portugal<br /><strong>time:</strong> t23:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 918670295</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-8.6575023,41.1780656,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Viernes<br></strong><p><strong>address:</strong> Tv. da Figueirôa 24, 4050-107 Porto, Portugal<br /><strong>time:</strong> 23:00 - 02:30 <br /><strong>price:</strong> 4 €<br /><strong>contact:</strong> phone: 936007883 / 919612025 - email: info@licaodetango.com- website: http://www.licaodetango.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-8.618685,41.1554811,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Maus Hábitos de Salão</strong><p><strong>address:</strong> R. de Passos Manuel 178, 4000-382 Porto, Portugal<br /><strong>time:</strong> 22:00 - 01:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +351936201893 / +351963085224 - email: liberaisdotango@gmail.com - website: http://liberaisdotango.atspace.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-8.6057331,41.1467156,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga d'A Barraca</strong><p><strong>address:</strong> Largo Santos 2, 1200-109 Lisboa, Portugal<br /><strong>time:</strong> 21:30 - 00:30 <br /><strong>price:</strong> 6 €<br /><strong>contact:</strong> phone: +351917262897 - email: tango@netcabo.pt- website: http://www.tangoportugal.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-9.1553322,38.7070697,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Lx</strong><p><strong>address:</strong> R. Maria 73, 1170-210 Lisboa, Portugal<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> 10€<br /><strong>contact:</strong> phone: +351960058710 - email: pasiontango@gmail.com - website: http://www.pasiontango.net/</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-9.1332162,38.7248194,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango E.Motion</strong><p><strong>address:</strong> R. Flores de Lima 8, 1700-196 Lisboa, Portugal<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +351 919 634 909 - email: tango2dance@hotmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-9.1453643,38.7481718,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Brava</strong><p><strong>address:</strong> R. Fábrica de Material de Guerra 1, 1950-128 Lisboa, Portugal<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 964859049 - email: lamorochatanguera@gmail.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-9.1012422,38.743791,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga da Bica</strong><p><strong>address:</strong> R. da Bica de Duarte Belo 42, 1200-288 Lisboa, Portugal<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +351960058710 - email: pasiontango@gmail.com - website: http://www.pasiontango.net/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-9.1459962,38.7102054,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga STEPS</strong><p><strong>address:</strong> R. Caminhos de Ferro 90B, 1100-108 Lisboa, Portugal<br /><strong>time:</strong> 23:00 - 01:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: +351960058710 - email: pasiontango@gmail.com - website: http://www.pasiontango.net/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-9.1233403,38.7140278,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Havana Club</strong><p><strong>address:</strong> Nikole Spasica 1, Beograd, Serbia<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.4544052,44.8182626,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Poseydon Club</strong><p><strong>address:</strong> Sajmište 20, Beograd, Serbia<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.4423895,44.8138242,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Forever Milonga</strong><p><strong>address:</strong> Kralja Petra 71, Beograd, Serbia<br /><strong>time:</strong> 22:30 - 03:00 <br /><strong>price:</strong> 1€<br /><strong>contact:</strong> phone: +381631185838 - email: dusanplavsic@gmail.com - website: http://tangosavez.webs.com/milonge.htm</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.456948,44.8213713,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga para todos</strong><p><strong>address:</strong> Bircaninova 22, Beograd, Serbia<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 063355350 - email: tangosanic@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.4610432,44.803457,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>ComTrade Group</strong><p><strong>address:</strong> XOXOXO<br /><strong>time:</strong> 22:00 -02:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 063/210-375 - website: http://www.tangosavez.org.yu/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.4183312,44.8009187,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Yachting Club Belgrade</strong><p><strong>address:</strong> Nušic´eva 6, Beograd, Serbia<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.4641132,44.8150751,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>TANGODROM</strong><p><strong>address:</strong> Balkanska 13, Beograd, Serbia<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> 1 €<br /><strong>contact:</strong> phone: +381 64 11 768 11 - email: stoputaboje@yahoo.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.4605313,44.8118148,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>KPGT</strong><p><strong>address:</strong> Radnicka 3, Beograd, Serbia<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [20.4127922,44.8379792,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Bar</strong><p><strong>address:</strong> Šmartinska cesta 106, 1000 Ljubljana, Slovenia<br /><strong>time:</strong> t21:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +386 41 324 828 - website: http://www.tangoslovenia.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [14.537508,46.0677903,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milagro</strong><p><strong>address:</strong> Pushkins'ka St, 5, Kharkiv, Kharkiv Oblast, Ukraine, 61000<br /><strong>time:</strong> 19:00 - 23:30 <br /><strong>price:</strong> 4 €<br /><strong>contact:</strong> phone: +38-050-98-08-267 - email: denischevchenco@ya.ru - website: http://tango.kh.ua/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [36.2334129,49.9924885,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Kontrabas</strong><p><strong>address:</strong> Pushkins'ka St, 56, Kharkiv, Kharkiv Oblast, Ukraine, 61000<br /><strong>time:</strong> 19:00 - 22:45 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 066-200-07-33 - website: http://tango.kh.ua</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [36.2438618,50.0008102,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Don Milonga</strong><p><strong>address:</strong> Pfingstweidstrasse 10, 8005 Zürich, Switzerland<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> Fr. 10<br /><strong>contact:</strong> phone: +41 (0) 79 641 26 68 / +41 (0) 76 580 51 19 - email: oscar@tangoharmonia.ch - website: http://www.tangoharmonia.ch/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [8.5181962,47.3880538,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>A bailar</strong><p><strong>address:</strong> Steinenring 60, 4051 Basel, Switzerland<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> Fr. 8<br /><strong>contact:</strong> phone: 061-281 14 03 - email: leknesch@datacomm.ch</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [7.580752,47.5504826,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga am Freitag</strong><p><strong>address:</strong> Rohrerstrasse 80, 5000 Aarau, Switzerland<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> Fr. 10<br /><strong>contact:</strong> email: kontakt@tangoaarau.ch</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [8.0607944,47.3940482,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tivoli Tango</strong><p><strong>address:</strong> Lilla Allmänna Gränd 9, 115 21 Stockholm, Sweden<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> 80 sek<br /><strong>contact:</strong> phone: 46 8 318382 / 0733397339 - email: daniel.ponce@spray.se</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [18.0973929,59.3234873,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Torsdagsmilonga</strong><p><strong>address:</strong> Tjärhovsgatan 19, 116 28 Stockholm, Sweden<br /><strong>time:</strong> 18:00 - 22:00 <br /><strong>price:</strong> 50 sek<br /><strong>contact:</strong> phone: 076 255 60 70 - email: peder@kerrik.se - website: http://kerrik.se/tjarlek/tangodelsur.html</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [18.0817354,59.3159307,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonguear</strong><p><strong>address:</strong> Drottninggatan 91, Stockholm, Sweden<br /><strong>time:</strong> t19:00 - 23:00 <br /><strong>price:</strong> 50 sek<br /><strong>contact:</strong> phone: 0762290338 / 0709171564 - email: tango.stockholm@gmail.com - website: http://www.tango-stockholm.se/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [18.061766,59.3335173,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Luna y Molino La Milonga</strong><p><strong>address:</strong> Wollaton Hall Dr, Nottingham NG8 1AF, UK<br /><strong>time:</strong> 20:00 - 00:30 <br /><strong>price:</strong> £7<br /><strong>contact:</strong> phone: 01636 646825 - email: info@lunaymolino.co.uk - website: http://www.lunaymolino.co.uk/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-1.1884833,52.9471429,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>L Tango Milonga</strong><p><strong>address:</strong> Village Road, Clifton Village, Clifton, Nottingham NG11 8NE, UK<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> £ 4<br /><strong>contact:</strong> phone: 00 447530549570 - email: lisa.cherry-downes@ntlworld.com - website: http://www.tangoinnottingham.moonfruit.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-1.187042,52.908282,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga @ the CCA</strong><p><strong>address:</strong> 350 Sauchiehall St, Glasgow G2 3JD, UK<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> £5<br /><strong>contact:</strong> phone: 07887928673 - email: sari_lievonen@hotmail.com - website: http://tangointhecity.wordpress.com/milongas/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.2651325,55.8657696,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Bordona at Sloans' Ballroom</strong><p><strong>address:</strong> 62 Argyle St, Glasgow G2 8BJ, UK<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: stuiji@hotmail.com - website: http://www.tangoglasgow.org.uk/sloanestango.html</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-4.25196,55.8578469,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Eton Milonga</strong><p><strong>address:</strong> Eton Ct, Eton, Windsor SL4, UK<br /><strong>time:</strong> 19:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 07793 142743 - email: dance@thamesvalleytango.co.uk - website: http://www.thamesvalleytango.co.uk/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-0.6099128,51.4878774,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de los Domingos</strong><p><strong>address:</strong> 38 W Nicolson St, Edinburgh EH8 9DD, UK<br /><strong>time:</strong> 17:30 - 23:00 <br /><strong>price:</strong> £3<br /><strong>contact:</strong> email: info@edinburghtango.org.uk</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-3.1852948,55.94442,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>University Social Club</strong><p><strong>address:</strong> Mill Ln, Cambridge CB2 1RX, UK<br /><strong>time:</strong> 20:00 - 22:30 <br /><strong>price:</strong> £4<br /><strong>contact:</strong> email: info@tangoevent.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [0.1165186,52.2014658,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Tango Bar</strong><p><strong>address:</strong> Cambridge CB2 1NL, UK<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> £4<br /><strong>contact:</strong> email: info@cambridgetango.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [0.128602999999998,52.198051,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Wimbledon Village Club</strong><p><strong>address:</strong> 26 Lingfield Rd, Wimbledon, London SW19 4QD, UK<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> £7<br /><strong>contact:</strong> phone: 02085408963 - email: leofandango@hotmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.216609,51.4233176,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Latvian House</strong><p><strong>address:</strong> The Latvian Welfare Fund, 72 Queensborough Terrace, London W2 3SH, UK<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> £5<br /><strong>contact:</strong> phone: 07748648322 - email: info@rojoynegroclub.com - website: http://www.rojoynegroclub.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.1848332,51.5110772,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Cobden Club</strong><p><strong>address:</strong> 170-172 Kensal Road, Ladbroke Grove, London W10 5BN, UK<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> £8<br /><strong>contact:</strong> phone: 07908845162 - email: eleonora@tangology.org - website: http://www.tangology.org//</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.206298800000013,51.5255459,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonguita Porteña</strong><p><strong>address:</strong> Währinger Str. 6-8, 1090 Wien, Austria<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> email: info@2x4tango.at - website: http://www.2x4tango.at/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [16.3612732,48.2156889,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Chamuyo</strong><p><strong>address:</strong> 2474 Prince Edward St, Vancouver, BC V5T, Canada<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 604 338 4069 - email: tango@santiagodeborah.com - website: http://www.santiagodeborah.com/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-123.0959251,49.2627697,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Cachila</strong><p><strong>address:</strong> 805 Dovercourt Rd, Toronto, ON M6H 2X4, Canada<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: lacachila@hotmail.ca - website: http://lacachila.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-79.4295093,43.6627784,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Barrios del Sur</strong><p><strong>address:</strong> 2643 128th St, Surrey, BC V4A 3W6, Canada<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 778.773.9904 - email: info@portalatango.com - website: http://www.portalatango.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.8678838,49.0504378,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Sunalta Community Center</strong><p><strong>address:</strong> 1627 10 Ave SW, Calgary, AB T3C 0J7, Canada<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 001-403-28803679 - email: tango_ar@telus.net - website: http://www.clubtangoargentino.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-114.098346,51.0442084,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Epicurean Tango Society</strong><p><strong>address:</strong> 1727 14 Ave SW, Calgary, AB T3C 0W8, Canada<br /><strong>time:</strong> 19:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 403 540 2525 - email: epicureantangosociety@gmail.com - website: http://epicureantangosociety.blogspot.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-114.1011928,51.040408,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango y Nada Mas</strong><p><strong>address:</strong> 505 78 Ave SW, Calgary, AB T2V 0T3, Canada<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 403 9227903 - email: info@tangoynadamas.net - website: http://www.tangoynadamas.net/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-114.0740212,50.9829694,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Noche de Tango</strong><p><strong>address:</strong> 606 1 St SW, Calgary, AB T2P 3B1, Canada<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 403-613-4865 - email: pwidling@shaw.ca - website: http://teatango.wordpress.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-114.0653563,51.047312,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Red Chair Cafe</strong><p><strong>address:</strong> 337 E 4th Ave, Anchorage, AK 99501, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: http://tangotrance.blogspot.com.es/ - facebook: https://www.facebook.com/groups/169107137562/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-149.877516,61.2188964,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Anchorage Wednesday Milonga</strong><p><strong>address:</strong> W 4th and D Street , Anchorage, AK 99501, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> t$5<br /><strong>contact:</strong> website: http://tangotrance.blogspot.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-149.8894143,61.2185565,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Layalena</strong><p><strong>address:</strong> 1290 N Scottsdale Rd, Tempe, AZ 85281, USA<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 480.966.9116 - email: info@layalenarestaraunt.com - website: http://tangoarizona.com/Phoenix/layalena.htm</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-111.9277361,33.4428589,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Belrose Matinee Milonga</strong><p><strong>address:</strong> 1415 Fifth Ave, San Rafael, CA 94901, USA<br /><strong>time:</strong> 16:00 - 19:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: 925.899.4611 / 650.834.5658 - email: livtango@gmail.com - website: http://www.espiritulibretango.blogspot.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.5325616,37.9743448,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Shall We Dance Milonga</strong><p><strong>address:</strong> 2209 S El Camino Real, San Mateo, CA 94403, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (650) 888-3996 - email: melvamg@aol.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.307192,37.5461861,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Arrabal Milonga</strong><p><strong>address:</strong> 2929 19th St, San Francisco, CA 94110, USA<br /><strong>time:</strong> 21:00 - 01:30 <br /><strong>price:</strong> $17<br /><strong>contact:</strong> phone: 415-810-6040 - email: adolfo@tangozarry.com - website: http://www.tangozarry.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.411364,37.760124,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Verdi Club Milonga</strong><p><strong>address:</strong> 2424 Mariposa St, San Francisco, CA 94110, USA<br /><strong>time:</strong> 20:45 - 00:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 415-810-6040 - email: adolfo@tangozarry.com - website: http://www.tangozarry.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4079386,37.7634058,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Soma Milonga</strong><p><strong>address:</strong> 19 Heron St, San Francisco, CA 94103, USA<br /><strong>time:</strong> 20:30 - 01:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: http://sftango.com/tango_soma.html</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4084535,37.7744811,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Revolution w/Live Music</strong><p><strong>address:</strong> 1667 Market St, San Francisco, CA 94103, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 415.661.1852 - website: http://www.intimateembracetango.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4216113,37.7730143,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Los Amigos in Pasadena</strong><p><strong>address:</strong> 1368 N Lake Ave, Pasadena, CA 91104, USA<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: http://www.apurotango.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-118.1318088,34.1694837,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Monte Cristo Club</strong><p><strong>address:</strong> 136 Missouri St, San Francisco, CA 94107, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> email: gntango@mindspring.com </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.3970541,37.7646348,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Cellspace Alternative Milonga</strong><p><strong>address:</strong> 2050 Bryant St, San Francisco, CA 94110, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: dolfdude@sbcglobal.net - website: http://project-tango.org/CELLspace/CELLspace.html</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.410331,37.7611692,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Cumparsita</strong><p><strong>address:</strong> 2101 Mariposa St, San Francisco, CA 94107, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: info@tangowithcarolina.com - website: http://www.tangowithcarolina.com/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.404668,37.7632318,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Palo Alto Milonga w/ Eric & Rebecca</strong><p><strong>address:</strong> 555 Waverley St, Palo Alto, CA 94301, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: elainekho@mac.com - website: http://www.ericlindgrentango.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.1593046,37.4468532,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club Danzarin</strong><p><strong>address:</strong> 17961 Sky Park Cir, Irvine, CA 92614, USA<br /><strong>time:</strong> 20:45 - 00:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> email: clubdanzarin@aol.com - website: http://www.clubdanzarin.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.8629109,33.6900318,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Magdalena</strong><p><strong>address:</strong> 301, 580 Grand Ave, Oakland, CA 94610, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (510) 836 0812 - website: http://www.tangomagdalena.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.249963,37.8093494,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Alberto's Night Club</strong><p><strong>address:</strong> 736 W Dana St, Mountain View, CA 94041, USA<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: espiritulibretango@yahoo.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.0789214,37.3923683,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Saturday Westwood Milonga</strong><p><strong>address:</strong> 1945 Westwood Blvd, Los Angeles, CA 90025, USA<br /><strong>time:</strong> 21:30 - 01:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 818-244-2136 - email: tangosplash@earthlink.net - website: http://www.tangosplash.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-118.4351313,34.0473716,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de Los Altos</strong><p><strong>address:</strong> 347 1st St, Los Altos, CA 94022, USA<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 408.460.0508 - email: AcademiaDeTangoArgentino@yahoo.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.1163769,37.3766535,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Danceasy Milonga<br></strong><p><strong>address:</strong> 9943-9951 San Pablo Ave, El Cerrito, CA 94530, USA<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: 510/524-9100 - email: danceasy@comcast.net - website: http://www.danceasy.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.302515,37.899494,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Glorieta</strong><p><strong>address:</strong> 600 E Kennedy Blvd, Tampa, FL 33602, USA<br /><strong>time:</strong> 15:30 - 20:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (813) 920-3330 - email: victoria_bsas@yahoo.com - website: http://www.tampatangoargentino.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-82.4551351,27.9492068,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Continental Milonga</strong><p><strong>address:</strong> 6531 Park Blvd N, Pinellas Park, FL 33781, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 727-612-6883 / 727-586-4665</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.726921,27.839542,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tampa Bay Tango Club Milonga</strong><p><strong>address:</strong> 150 E Davis Blvd, Tampa, FL 33606, USA<br /><strong>time:</strong> t21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 813-222-5040 / 813-960-8871 - website: http://www.tampabaytango.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.4546567,27.9281013,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Simone's Milonga</strong><p><strong>address:</strong> 8336 W Hillsborough Ave, Tampa, USA<br /><strong>time:</strong> 20:00 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (813) 391-1056 - email: simone@simonesalsa.com - website: http://www.simonesalsa.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.5770317,27.9962021,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Blue Moon Milonga</strong><p><strong>address:</strong> 5098 66th St N, St. Petersburg, FL 33709, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (813) 380-8432</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.7290472,27.8179796,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Savoy South Ballroom Milonga</strong><p><strong>address:</strong> 4000 Park Blvd N, Pinellas Park, FL 33781, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (813) 205-8801</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.6880872,27.83878,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Eddy & Veronica's Alternative Milonga</strong><p><strong>address:</strong> 5098 66th St N, St. Petersburg, FL 33709, USA<br /><strong>time:</strong> 18:00 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (813)380-8432</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.7290472,27.8179796,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>t1st Dance Studio</strong><p><strong>address:</strong> 5098 66th St N, St. Petersburg, FL 33709, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: (813) 380-8432</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.7290472,27.8179796,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Twilight Tango Milonga</strong><p><strong>address:</strong> 1000 Circus Blvd, Sarasota, FL 34232, USA<br /><strong>time:</strong> 17:00 - 21:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: 941-365-0888 - email: mckramer@verizon.net</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.4884904,27.3454382,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Sara Dance Center</strong><p><strong>address:</strong> 5000 Fruitville Rd, Sarasota, FL 34232, USA<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (813) 380-8432</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-82.4691,27.3373089,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Patio de la Morocha<br></strong><p><strong>address:</strong> 11601 S Orange Blossom Trail, Orlando, FL 32837, USA<br /><strong>time:</strong> 19:30 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 407 908 2449 / 407 963 7710 - email: patiodelamorocha@yahoo.com - website: http://www.tangoenorlando.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-81.4040369,28.3982143,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga of Orlando</strong><p><strong>address:</strong> 2609 Gowen St, Orlando, FL 32806, USA<br /><strong>time:</strong> 19:30 - 23:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (321) 229-0973 </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-81.3531058,28.513924,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Roberto Maiolo’s Milonga</strong><p><strong>address:</strong> 12390 W Dixie Hwy, North Miami, FL 33161, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (954) 986-0261</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1889187,25.8890847,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Roberto Maiolo’s Milonga</strong><p><strong>address:</strong> 12390 W Dixie Hwy, North Miami, FL 33161, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (954) 986-0261</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1889187,25.8890847,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Waterfrontoo</strong><p><strong>address:</strong> 2205 N Tamiami Trail, Nokomis, FL 34275, USA<br /><strong>time:</strong> 17:00 - 20:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: (941) 429-4438</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.4689665,27.1546623,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Fourth Sunday Milonga</strong><p><strong>address:</strong> 303 S Tamiami Trail, Nokomis, FL 34275, USA<br /><strong>time:</strong> 17:00 - 20:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (941) 907-3923</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.4527895,27.1224565,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga del Pueblo</strong><p><strong>address:</strong> 6255 SW 8th St, West Miami, FL 33144, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (305) 266-1897</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.2972136,25.7634863,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Baldoza</strong><p><strong>address:</strong> 18401 NE 19th Ave, North Miami Beach, FL 33179, USA<br /><strong>time:</strong> 20:00 to 23:00 <br /><strong>price:</strong> $ 15<br /><strong>contact:</strong> phone: 407-437-5406 - email: labaldoza@gmail.com - website: http://milongamiami.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.162234,25.9450053,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de Gardel</strong><p><strong>address:</strong> 10827 SW 40th St, Miami, FL 33165, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (305) 632-2567</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.3703686,25.7323612,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Decotango Milonga</strong><p><strong>address:</strong> 1766 Bay Rd, Miami Beach, FL 33139, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (786) 554-2770</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.143872,25.7933343,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Nights at Novecento</strong><p><strong>address:</strong> 1080 Alton Rd, Miami Beach, FL 33139, USA<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (305) 531-0900</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1412356,25.7815765,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga in Miami Beach</strong><p><strong>address:</strong> 448 Española Way, Miami Beach, FL 33139, USA<br /><strong>time:</strong> 22:00 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: (786) 287-7260</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1327508,25.786804,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Liber & Rosa's Milonga</strong><p><strong>address:</strong> 908 Lehto Ln, Lake Worth, FL 33461, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: 561-351-4573 - email: decisiontango@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1082442,26.6084082,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>West Palm Beach Tango Milonga</strong><p><strong>address:</strong> 6295 Lake Worth Rd, Lake Worth, FL 33463, USA<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (954) 986-0261</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1435339,26.6193511,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>nternational Club of Argentine Tango Milonga</strong><p><strong>address:</strong> 410 SE 3rd St, Hallandale Beach, FL 33009, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (305) 864-5785</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1437345,25.9813659,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Media Luz</strong><p><strong>address:</strong> 5842 14th St W, Bradenton, FL 34207, USA<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (914)882-9096 - email: hharvito@hotmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.5758867,27.4371507,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>C.A.T.S. Milonga</strong><p><strong>address:</strong> 362 Whitney Ave, New Haven, CT 06511, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (203) 874-2102 - email: phelpstango@optonline.net</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.9188214,41.3207297,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cafe Rue Restaurant Milonga</strong><p><strong>address:</strong> 95 Railroad Ave, Greenwich, CT 06830, USA<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (203) 629-1056</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.6250299,41.0218478,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Circle of Life Milonga</strong><p><strong>address:</strong> 50 Chapman Pl, East Hartford, CT 06108, USA<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: (860) 291-8747</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.6446954,41.7730843,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Bailecito del Viernes</strong><p><strong>address:</strong> 209 Kalamath St, Denver, CO 80223, USA<br /><strong>time:</strong> 22:15 - 01:00 <br /><strong>price:</strong> t$5<br /><strong>contact:</strong> phone: 303-893-6783 / 303-546-6620 - email: gabrielacarone@gmail.com - website: http://www.gabrielatango.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-105.000529,39.71987,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga</strong><p><strong>address:</strong> 209 Kalamath St, Denver, CO 80223, USA<br /><strong>time:</strong> 21:15 - 00:00 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> phone: 303-893-6783 / 303-546-6620 - email: gabrielacarone@gmail.com - website: http://www.gabrielatango.com/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-105.000529,39.71987,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Red Martini</strong><p><strong>address:</strong> 25 N Tejon St, Colorado Springs, CO 80903, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 719-475-0625 - email: nancyrod4@juno.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-104.8229232,38.834927,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Corazon a Corazon</strong><p><strong>address:</strong> 325 N Hoyne Ave, Chicago, IL 60612, USA<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 312-771-1226 - email: alison@americantangoinstitute.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6789633,41.887493,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Dance Center Chicago Studio</strong><p><strong>address:</strong> 3868 N Lincoln Ave, Chicago, IL 60613, USA<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 773.936.1619 - email: agape@dancetangochicago.com - website: http://www.dancetangochicago.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-87.6777698,41.9520854,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Meet-up Milonga</strong><p><strong>address:</strong> 325 N Hoyne Ave, Chicago, IL 60612, USA<br /><strong>time:</strong> t21:00 - 01:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: 312-771-1226 - email: alison@americantangoinstitute.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6789633,41.887493,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Barba Yianni Restaurant</strong><p><strong>address:</strong> 4761 N Lincoln Ave, Chicago, IL 60625, USA<br /><strong>time:</strong> t21:00 - 00:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 773.936.1619 - email: agape@dancetangochicago.com - website: http://www.dancetangochicago.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6879615,41.9683383,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga a Media Luz</strong><p><strong>address:</strong> 470 Candler Park Dr NE, Atlanta, GA 30307, USA<br /><strong>time:</strong> 19:30 - 22:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (770) 454-1177</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.339798,33.767089,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tangueros Emory Milonga</strong><p><strong>address:</strong> Glenn Memorial Church School Building, 1660 N Decatur Rd, Atlanta, GA 30307, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> email: raidingpartygames@yahoo.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.3233896,33.78879,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Evolution</strong><p><strong>address:</strong> 800 Miami Cir NE, Atlanta, GA 30324, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: (404) 378-2937</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.3632484,33.8308317,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Barrio Norte Milonga<br></strong><p><strong>address:</strong> 6125 Roswell Rd NE, Atlanta, GA 30328, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (404) 325-1360</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.3780594,33.9221882,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango and Tapas at Pura Vida</strong><p><strong>address:</strong> 656 North Highland Avenue Northeast, Atlanta, GA 30306, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (404) 870-9797</p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-84.3527352,33.7723025,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Bohemia</strong><p><strong>address:</strong> 721 Miami Cir NE, Atlanta, GA 30324, USA<br /><strong>time:</strong> 21:00 - 00:30<br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (404) 754-8272</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.363082,33.82967,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Dinner and Tango at Carpe Diem</strong><p><strong>address:</strong> 105 Sycamore Pl, Decatur, GA 30030, USA<br /><strong>time:</strong> t21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (404) 687-9696</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.2890351,33.7732895,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>CocoCabana Grill</strong><p><strong>address:</strong> 2031A University Blvd E, Hyattsville, MD 20783, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: caritango@yahoo.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.9781721,38.9818006,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Portenia</strong><p><strong>address:</strong> 7800 Wisconsin Ave, Bethesda, MD 20814, USA<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> t$15<br /><strong>contact:</strong> phone: (301) 775-6014 - email: vivitango@verizon.net - website: http://www.vivianatango.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-77.0955655,38.9884168,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Divino Lounge Milonga</strong><p><strong>address:</strong> 7345 Wisconsin Ave, Bethesda, MD 20814, USA<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (240) 497-0300</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0932588,38.9831997,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>tWomen's Club of Bethesda</strong><p><strong>address:</strong> 5500 Sonoma Rd, Bethesda, MD 20817, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: (301) 346-5079 - email: eugeniap@mris.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-77.1102652,39.0014073,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Nuevos Aires Milonga</strong><p><strong>address:</strong> 509 S Broadway, Baltimore, MD 21231, USA<br /><strong>time:</strong> t18:30 - 21:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 301-257-6818 - email: pablo@fontanatango.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.5930268,39.2852723,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Kiss Me I'm a Tango Dancer Milonga</strong><p><strong>address:</strong> 1301 Baylis St, Baltimore, MD 21224, USA<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> $13<br /><strong>contact:</strong> phone: (443) 794-1139</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.5673986,39.2784217,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Pasional</strong><p><strong>address:</strong> 18 Roth St, Alexandria, VA 22314, USA<br /><strong>time:</strong> t21:00 - 00:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 240 715 4233 - email: info@tangoduos.com - website: http://www.tangoduos.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-77.0810565,38.8069112,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Porteña</strong><p><strong>address:</strong> 20 Crowley St, Burlington, VT 05401, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: (802)734-3135 - email: champlaintango@hotmail.com - website: http://www.champlaintango.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-73.2237044,44.4873289,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Salsalina Dance Studio</strong><p><strong>address:</strong> 266 Pine St, Burlington, VT 05401, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> t$5<br /><strong>contact:</strong> phone: 802-877-6648 / 802-863-3440</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.2146846,44.472061,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Aparicion de Tango Milonga</strong><p><strong>address:</strong> XOXOXO<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 603 835 6790</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.553741,42.83682,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Tango Incident</strong><p><strong>address:</strong> 99 S Bend St, Pawtucket, RI 02860, USA<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: margolect@yahoo.com - website: http://www.providencetango.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.3724932,41.8751972,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Pa' la Gente</strong><p><strong>address:</strong> 421 Grand Ave, Pawtucket, RI 02861, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: info@hardroadtango.com - website: http://hardroadtango.com/Sundaymilonga/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.3519933,41.891159,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Last Tango in Portland</strong><p><strong>address:</strong> Morrison St, Providence, RI 02906, USA<br /><strong>time:</strong> 20:00 - 02:00 <br /><strong>price:</strong> t$5<br /><strong>contact:</strong> email: JayRabePDX@gmail.com - website: http://tangomoments.com/pages/Calendar.htm</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-71.3897667,41.8422692,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Aime Comme Moi</strong><p><strong>address:</strong> 6305 SE Foster Rd, Portland, OR 97206, USA<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> email: info@hardroadtango.com - website: http://hardroadtango.com/fridaymilonga/index.html</p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.5982231,45.4904848,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Northstar Ballroom</strong><p><strong>address:</strong> 635 NE Killingsworth Ct, Portland, OR 97211, USA<br /><strong>time:</strong> 20:00 - 00:30 <br /><strong>price:</strong> t$7<br /><strong>contact:</strong> phone: 5037843786 - email: j_wallach@yahoo.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.6593123,45.5623006,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango at Norse Hall</strong><p><strong>address:</strong> Dance with Susan Molitor in Portland, OR, USA, 111 NE 11th Ave, Portland, OR 97232, USA<br /><strong>time:</strong> 19:30 - 00:00 <br /><strong>price:</strong> $6<br /><strong>contact:</strong> email: ajpk007@yahoo.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.6549747,45.5238613,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Noches de Buenos Aires at the PPAA<br></strong><p><strong>address:</strong> 618 SE Alder St, Portland, OR 97214, USA<br /><strong>time:</strong> t19:00 - 00:00 <br /><strong>price:</strong> $6<br /><strong>contact:</strong> email: robhauk@teleport.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.6597252,45.5179421,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Saratoga SAVOY Milonga</strong><p><strong>address:</strong> 7 Wells St, Saratoga Springs, NY 12866, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: 1-518-587-5132 - email: dance@saratogasavoy.com - website: http://www.saratogasavoy.com/files/milonga.html</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-73.7922581,43.086001,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>All Night Milonga</strong><p><strong>address:</strong> 37 W 26th St, New York, NY 10010, USA<br /><strong>time:</strong> t22:00 - 05:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 646-742-9400 - email: info@steppingoutstudios.com - website: http://steppingoutstudios.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-73.9904318,40.7445741,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Conspiracy</strong><p><strong>address:</strong> 85 W Broadway, New York, NY 10007, USA<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> website: http://www.tangoconspiracy.net/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-74.0091396,40.715201,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Nacional</strong><p><strong>address:</strong> 239 14th St, Brooklyn, NY 11215, USA<br /><strong>time:</strong> 21:30 - 01:30 <br /><strong>price:</strong> t$14<br /><strong>contact:</strong> phone: 917-385-9698, 917-691-6399 - email: coco_arregui@yahoo.com - website: http://www.tangolanacional.com/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.989091,40.66657,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Meat Market Milonga</strong><p><strong>address:</strong> 135 W 20th St, New York, NY 10011, USA<br /><strong>time:</strong> t21:30 - 00:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 212-633-6445 - email: carina@tangonyc.com - website: http://www.tangonyc.com/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.9955772,40.7418524,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Casa de Tango</strong><p><strong>address:</strong> 109 E 9th St, New York, NY 10003, USA<br /><strong>time:</strong> 17:00 - 01:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: 6464199278 - email: info@blackwellassociates.com - website: http://www.blackwellassociates.com/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.989906,40.730521,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Nuestra Milonga</strong><p><strong>address:</strong> 100 Carondelet Plaza, Clayton, MO 63105, USA<br /><strong>time:</strong> 18:00 - 22:00 <br /><strong>price:</strong> t$5<br /><strong>contact:</strong> website: http://www.tangostlouis.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-90.3309628,38.6483731,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Lounge</strong><p><strong>address:</strong> 37 S Maple Ave, Webster Groves, MO 63119, USA<br /><strong>time:</strong> t19:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 314-324-0887 - website: http://www.tangoteacher.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-90.355103,38.591047,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Casera</strong><p><strong>address:</strong> 9817 Countryshire Pl, Creve Coeur, MO 63141, USA<br /><strong>time:</strong> t21:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: andreakallaus@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-90.396579,38.662132,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga des Artes</strong><p><strong>address:</strong> 2 Oak Knoll Park, Clayton, MO 63105, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: http://www.tangostlouis.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-90.3195495,38.6376776,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Argentine Tango Detroit</strong><p><strong>address:</strong> 7758 Auburn Rd, Utica, MI 48317, USA<br /><strong>time:</strong> 21:00 - 23:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 586-254-0560 - email: lori@argentinetangodetroit.com - website: http://www.argentinetangodetroit.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-83.033784,42.6258543,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango at the Factory</strong><p><strong>address:</strong> 38 Harlow St, Worcester, MA 01605, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: (774) 364-1099</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-71.7955718,42.2838684,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Argentine Tango Break</strong><p><strong>address:</strong> 16 Bow St, Somerville, MA 02143, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 617-413-2981 / 781-395-1513</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.0972088,42.3807113,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>NeoTangoZone Milonga</strong><p><strong>address:</strong> Park St, Northampton, MA 01062, USA<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: 413 281 9722</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.6741607,42.3338084,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Pituca Milonga</strong><p><strong>address:</strong> 105 Rumford Ave, Auburndale, MA 02466, USA<br /><strong>time:</strong> t21:00 - 00:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: 617.969.2280 - email: info@baleraballroom.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.2424761,42.3582155,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>More Tuesday Milonga</strong><p><strong>address:</strong> 1353 Cambridge St, Cambridge, MA 02139, USA<br /><strong>time:</strong> t21:00 - 00:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: (617) 388-1168</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-71.100088,42.3738079,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Blue Milonga</strong><p><strong>address:</strong> 1615 Beacon St, Brookline, MA 02446, USA<br /><strong>time:</strong> t21:00 - 01:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (617) 393-3772 - email: mailto:info@bluetango.org - website: http://www.bluetango.org/bluemilonga.html</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-71.1345446,42.3390855,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Despues</strong><p><strong>address:</strong> 201 N 85th St, Seattle, WA 98103, USA<br /><strong>time:</strong> 21:00 - 01:30 <br /><strong>price:</strong> t$8<br /><strong>contact:</strong> phone: 206 579 2431 - email: mourad_zaoui@msn.com - website: http://www.tangoseattle.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.3565185,47.6903927,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Comme il Faut</strong><p><strong>address:</strong> 16th St NW, Washington, DC, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: (703) 801-3624</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0363661,38.9463095,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Chevy Chase Ballroom Milonga</strong><p><strong>address:</strong> 5205-5207 Wisconsin Ave NW, Washington, DC 20015, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: (202) 462-0870</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-77.083587,38.957533,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Portenia</strong><p><strong>address:</strong> 5205-5207 Wisconsin Ave NW, Washington, DC 20015, USA<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> t$15<br /><strong>contact:</strong> phone: (301) 775-6014 - email: vivitango@verizon.net - website: http://www.vivianatango.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-77.083587,38.957533,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del ARTe</strong><p><strong>address:</strong> 406-410 7th St NW, Washington, DC 20004, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (202) 290-1910 - website: http://www.tango-red.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0223138,38.895133,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>18th Street Lounge</strong><p><strong>address:</strong> 1212 18 St NW, Washington, DC 20036, USA<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: 720-252-6701 - email: sharnafabiano@gmail.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0419278,38.9062036,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Weekly Monday Milonga</strong><p><strong>address:</strong> 4629 41st St NW, Washington, DC 20016, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (301) 664-9690</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0800045,38.9504961,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga De Los Artistas</strong><p><strong>address:</strong> 2437 18 St NW, Washington, DC 20009, USA<br /><strong>time:</strong> t22:00 - 02:00 <br /><strong>price:</strong> t$10<br /><strong>contact:</strong> phone: (703) 626-6786 - email: arnaud@sietetango.com - website: http://lounge.sietetango.com/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0419546,38.921516,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cambalache (Casino of Cartagena)</strong><p><strong>address:</strong> Calle Mayor, 15, 30201 Cartagena, Murcia, Spain<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +34 639613502 - email: cambalachetangocartagena@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-0.98643,37.60007],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Historica</strong><p><strong>address:</strong> calle Palos de la Frontera s/n. 41004 Sevilla, Spain (Facultad de Filología)<br /><strong>time:</strong> 12:00 - 15:00<br /><strong>price:</strong> 3&euro;<br /><strong>contact:</strong> phoen: +34 630 33 00 23 - email: info@alejandrasabena.com - facebook: /AlejandraSabenaTango - web:http://sevilla.espurotango.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9918028,37.3802818,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Gala</strong><p><strong>address:</strong> Avda. Diego Martínez Barrio, 8. 41013 Sevilla, Spain<br /><strong>time:</strong> 22:30 - 04:00<br /><strong>price:</strong> 25&euro;<br /><strong>contact:</strong> phoen: +34 630 33 00 23 - email: info@alejandrasabena.com - facebook: /AlejandraSabenaTango - web:http://sevilla.espurotango.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9762915,37.3751605,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Milonguera</strong><p><strong>address:</strong> Avda. Diego Martínez Barrio, 8. 41013 Sevilla, Spain<br /><strong>time:</strong> 22:30 - 04:00<br /><strong>price:</strong> 25&euro;<br /><strong>contact:</strong> phoen: +34 630 33 00 23 - email: info@alejandrasabena.com - facebook: /AlejandraSabenaTango - web:http://sevilla.espurotango.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9762031,37.3752133,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Plaza España</strong><p><strong>address:</strong> Avda. de Isabel la Católica. 41004 Sevilla, Spain<br /><strong>time:</strong> 12:00 - 15:00<br /><strong>price:</strong> free;<br /><strong>contact:</strong> phoen: +34 630 33 00 23 - email: info@alejandrasabena.com - facebook: /AlejandraSabenaTango - web:http://sevilla.espurotango.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9875985,37.3763414,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Luna Llena</strong><p><strong>address:</strong> Avda. de Isabel la Católica. 41004 Sevilla, Spain<br /><strong>time:</strong> 22:30 - 04:00<br /><strong>price:</strong> 29&euro;<br /><strong>contact:</strong> phoen: +34 630 33 00 23 - email: info@alejandrasabena.com - facebook: /AlejandraSabenaTango - web:http://sevilla.espurotango.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-5.9762245,37.3751834,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The TANGO EXPERIENCE</strong><p><strong>address:</strong> 541 Standard St, El Segundo, CA 90245, USA<br /><strong>time:</strong> 20:30 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 310-621-0622 - email: livingtango@live.com - website: http://LivingTango.com/tango-experience </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-118.414978,33.923331,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga LAX</strong><p><strong>address:</strong> 8025 W Manchester Ave, Playa Del Rey, CA 90293, USA<br /><strong>time:</strong> 19:00 - 00:30 <br /><strong>price:</strong> $18<br /><strong>contact:</strong> phone: 310-621-0622 - email: livingtango@live.com - website: http://LivingTango.com/milonga-lax</p>",
"business":"milonga",
"day": "Sunday ",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-118.436411,33.9594028,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cambalache</strong><p><strong>address:</strong> Calle Mayor, 15, 30201 Cartagena, Murcia<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 639613502 - email: cambalachetangocartagena@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-0.986436,37.6000779,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>M8tango-Milonga</strong><p><strong>address:</strong> Mitternachtsgasse 8, 55116 Mainz, Germany<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 01776107150 - email: mark@freetango.de - website: www.m8tango.de</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [8.2717,50.00369,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Judy's Practica - Brisbane</strong><p><strong>address:</strong> 164 Sexton St, Tarragindi QLD 4121, Australia<br /><strong>time:</strong> 22:30 - 00:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: http://www.paralosninos.net/events</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [153.0479971,-27.5180163,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pure Milonguero Milonga - Gold coast</strong><p><strong>address:</strong> 18 Fairway Dr, Clear Island Waters QLD 4226, Australia<br /><strong>time:</strong> 19:30 - 21:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: http://www.paralosninos.net/events</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [153.4069458,-28.0363578,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pure Milonguero Milonga - Brisbane</strong><p><strong>address:</strong> 47 Abbotsleigh St, Holland Park QLD 4121, Australia<br /><strong>time:</strong> 19:30 - 23:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: http://www.paralosninos.net/events</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [153.05982,-27.514793,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica 'LA MARÍA'</strong><p><strong>address:</strong> Sarmiento 4006, C1197AAH CABA, Argentina<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $60<br /><strong>contact:</strong> email: agustinarozados@hotmail.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.421544,-34.604915,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica: LA MARÍA ROLERA</strong><p><strong>address:</strong> Sarmiento 4006, C1197AAH CABA, Argentina<br /><strong>time:</strong> 18:00 - 21:30 <br /><strong>price:</strong> $80<br /><strong>contact:</strong> email: agustinarozados@hotmail.com</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-58.421544,-34.604915,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Queer</strong><p><strong>address:</strong> Carrer de la Paloma, 5, 08001 Barcelona<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong>  email: tangoqueerbcn@gmail.com</p>",
"business":"milonga",
"day": "Monday ",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.165397,41.3825565,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Peña Del Tango Alicante</strong><p><strong>address:</strong> Calle Bernardo López García, 25, 03013 Alacant, Alicante, Spain<br /><strong>time:</strong> 23:00 - 00:30 <br /><strong>price:</strong> 8€<br /><strong>contact:</strong> phone: 676315557 - facebook: https://www.facebook.com/pena.deltangoalicante/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.4798535,38.3536811,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Evora</strong><p><strong>address:</strong> MacCurtain Street, Montenotte, Cork, Ireland<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> €5<br /><strong>contact:</strong> facebook: https://www.facebook.com/Cork-Tango-404746006352962/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-8.467591,51.9013174,0],
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Cooperativa</strong><p><strong>address:</strong> 4300 N 82nd St, Scottsdale, AZ 85251, UAS<br /><strong>time:</strong> 15:00 - 18:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: milongacooperativa@yahoo.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-111.904843,33.4991295,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong><br>El Abrazo</strong><p><strong>address:</strong> 4425 N Granite Reef Rd, Scottsdale, AZ 85251, USA<br /><strong>time:</strong> 20:30 - 22:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 505-977-8381 - email: mlicon@asu.edu</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-111.8997566,33.501037,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Sol</strong><p><strong>address:</strong> 2848 S Carriage Ln, Mesa, AZ 85202, USA<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> email: Judah.page@gmail.com - website: www.tangowisdom.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-111.8924905,33.3627623,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong><br>Lake Merritt Cheers Milonga</strong><p><strong>address:</strong> 200 Grand Ave, Oakland, CA 94610<br /><strong>time:</strong> 14:40 - 17:15 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 510-847-8596 - email: tangoleson@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2612893,37.8114446,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Afternoon Milonga - Nadir (Beloved) Tango</strong><p><strong>address:</strong> 2837 Claremont Blvd, Berkeley, CA 94705, USA<br /><strong>time:</strong> 15:30 - 18:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.2449029,37.8592824,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Rubia</strong><p><strong>address:</strong> 5855 Christie Ave, Emeryville, CA 94608, USA<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: (510) 282-8118</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.2957805,37.839745,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong><br>Two Left Feet Danville</strong><p><strong>address:</strong> 194 Diablo Rd, Danville, CA 94526, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: www.tangonation.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.9995818,37.8231735,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Allegro Milonga</strong><p><strong>address:</strong> 5855 Christie Ave, Emeryville, CA 94608, USA<br /><strong>time:</strong> 21:10 - 00:10 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 510-847-8596 - email: tangolesson@gmail.com - website: www.tangonation.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2957805,37.839745,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Free Emeryville Tango</strong><p><strong>address:</strong> 4321 Salem St, Emeryville, CA 94608, USA<br /><strong>time:</strong> 14:00 - 15:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: goldenagetangoacademy@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2797389,37.8339836,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong><br>Blackhawk Tango Outdoor </strong><p><strong>address:</strong> 398 Hartz Ave, Danville, CA 94526, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: (510) 406-4583 - email: carlinsf@gmail.com - facebook: www.facebook.com/pages/Blackhawk-Tango/417083931695445?ref=stream</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.9998678,37.821981,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Época in Oakland</strong><p><strong>address:</strong> 200 Grand Ave, Oakland, CA 94610, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $13<br /><strong>contact:</strong> phone: 415 370 6045 - email: ashvin@tangouniverse.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2612679,37.811469,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Sycamore Club Holidays Champagne Masquerade</strong><p><strong>address:</strong> 635 Old Orchard Dr, Danville, CA 94526, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (510)406-4583 - facebook: www.facebook.com/pages/Blackhawk-Tango/417083931695445?ref=stream</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.977859,37.8139245,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Abrazo, the queer milonga</strong><p><strong>address:</strong> 1970 Chestnut St, Berkeley, CA 94702, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong>n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.2883827,37.8701085,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Two Left Feet Danville Milonga </strong><p><strong>address:</strong> 194 Diablo Rd, Danville, CA 94526, USA<br /><strong>time:</strong> 21:10 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 925-831-8111 - website: www.tangonation.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.9995818,37.8231735,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>All-Nighter</strong><p><strong>address:</strong> 2560 Ninth Street, Berkeley, CA 94710, USA<br /><strong>time:</strong> 22:30 - 03:00 <br /><strong>price:</strong> $13<br /><strong>contact:</strong> facebook: https://www.facebook.com/groups/587339038104024/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.2917051,37.8591556,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Queer Tango Practica</strong><p><strong>address:</strong> 1970 Chestnut St, Berkeley, CA 94702, USA<br /><strong>time:</strong> 16:30 - 17:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: Abrazoqueertango@gmail.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2883827,37.8701085,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Lisette’s Practica</strong><p><strong>address:</strong> 5855 Christie Ave, Emeryville, CA 94608, USA<br /><strong>time:</strong> 16:00 - 18:00 <br /><strong>price:</strong> $6<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2957805,37.839745,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Lisette's Practica</strong><p><strong>address:</strong> 2500 Embarcadero, Oakland, CA 94606, USA<br /><strong>time:</strong> 21:00 - 22:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2395977,37.7757272,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Argentine Tango Practica</strong><p><strong>address:</strong> 5855 Christie Ave, Emeryville, CA 94608, USA<br /><strong>time:</strong> 21:30 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2957805,37.839745,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>BlackhawDanville</strong><p><strong>address:</strong> 635 Old Orchard Dr, Danville, CA 94526, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong>  facebook: www.facebook.com/pages/Blackhawk-Tango/417083931695445?ref=stream</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.977859,37.8139245,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Next Practica</strong><p><strong>address:</strong> 2560 Ninth Street, Berkeley, CA 94710, USA<br /><strong>time:</strong> 21:15 - 22:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: TheOrganicTangoSchool.org</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2917051,37.8591556,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Mandinga</strong><p><strong>address:</strong> 3369 Mt Diablo Blvd, Lafayette, CA 94549, USA<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 925-209-4124 - email: marcelo@escuelatangoba.com - website: escuelatangoba.com/marcelosolis/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.1071305,37.8943143,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Estrella Tango</strong><p><strong>address:</strong> 3702 Lone Tree Way, Antioch, CA 94509, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.806724,37.9844857,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica in Trinidad</strong><p><strong>address:</strong> 409 Trinity St, Trinidad, CA 95570, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-124.1427047,41.0602091,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica in Trinidad</strong><p><strong>address:</strong> 409 Trinity St, Trinidad, CA 95570, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-124.1427803,41.0602056,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Mendocino Elk Practica</strong><p><strong>address:</strong> Elk, CA 95432, USA<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: (707) 937-0078 - email: bysawyer@mcn.org - website: http://www.tangomendocino.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-123.715147,39.1291999,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Mendocino Milonga</strong><p><strong>address:</strong> 15051 Caspar Rd, Caspar, CA 95420, USA<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: tangomendocino.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-123.813517,39.3656099,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango at 215 Main, Point Arena</strong><p><strong>address:</strong> 215 Main St, Point Arena, CA 95468, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-123.6921464,38.9093692,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga w/ Live Music M.Puig & S.Asarnow</strong><p><strong>address:</strong> 305 Harbor Dr, Sausalito, CA 94965, USA<br /><strong>time:</strong> 19:30 - 22:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: (415) 331-2899 - website: www.tengotango.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4983951,37.8688499,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de San Anselmo</strong><p><strong>address:</strong> 167 Tunstead Ave, San Anselmo, CA 94960, USA<br /><strong>time:</strong> 20:15 - 23:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> website: www.AlmaDelTango.org</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.564119,37.9736851,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cachirulo Milonga @ The Warehouse</strong><p><strong>address:</strong> 64 Digital Dr, Novato, CA 94949, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $15 <br /><strong>contact:</strong> phone: 650–834–5658 - website: www.tangoapilado.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.5307639,38.0708611,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Valentina- Alternative Music @ ADT</strong><p><strong>address:</strong> 167 Tunstead Ave, San Anselmo, CA 94960, USA<br /><strong>time:</strong> 18:30 - 23:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.564119,37.9736851,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Beso Practica</strong><p><strong>address:</strong> 64 Digital Dr, Novato, CA 94949, USA<br /><strong>time:</strong> 17:00 - 20:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 650-834-5658 - website: tangoapilado.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.5307639,38.0708611,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango 2/3 & La Práctica</strong><p><strong>address:</strong> 167 Tunstead Ave, San Anselmo, CA 94960, USA<br /><strong>time:</strong> 20:15 - 22:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: www.AlmaDelTango.org</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.564119,37.9736851,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Monterey Bay Practica</strong><p><strong>address:</strong> 540 Calle Principal, Monterey, CA 93940, USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.8963492,36.5975962,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Monterey Bay's 4th Sun milonga</strong><p><strong>address:</strong> 540 Calle Principal, Monterey, CA 93940, USA<br /><strong>time:</strong> 18:00 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.8963492,36.5975962,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Monterey Bay 2d Sat milonga</strong><p><strong>address:</strong> 540 Calle Principal, Monterey, CA 93940, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.8963492,36.5975962,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Atomica</strong><p><strong>address:</strong> 17961 Sky Park Cir, Irvine, CA 92614, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.8629195,33.68999,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Practica</strong><p><strong>address:</strong> 17961 Sky Park Cir, Irvine, CA 92614, USA<br /><strong>time:</strong> 21:15 - 23:00 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.8629195,33.68999,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga (r)Evolución</strong><p><strong>address:</strong> 1502 E Warner Ave, Santa Ana, CA 92705, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-117.8493212,33.715333,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango master Gato Valdez </strong><p><strong>address:</strong> 1502 E Warner Ave, Santa Ana, CA 92705, USA<br /><strong>time:</strong> 20:30 - 21:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> website: www.gatotango.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.8493212,33.715333,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>3rd Sunday Milonga w. Andrea Monti y Adrian Durso</strong><p><strong>address:</strong> 6009 Folsom Blvd, Sacramento, CA 95819, USA<br /><strong>time:</strong> 18:30 - 22:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.4314873,38.5563453,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango On Broadway</strong><p><strong>address:</strong> 2416 18th St, Sacramento, CA 95818, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $3<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.4898492,38.5613556,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango @ TBTR</strong><p><strong>address:</strong> 128 J St, Sacramento, CA 95814, USA<br /><strong>time:</strong> 20:30 - 21:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: rivertango.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.50455,38.583034,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Connection w/Mike & Alisa</strong><p><strong>address:</strong> 1920 T St, Sacramento, CA 95811, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $3<br /><strong>contact:</strong> email: Alisaandmike@yahoo.com</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.4858188,38.5656276,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Friday Nite Milonga, Tango On Broadway</strong><p><strong>address:</strong> 2416 18th St, Sacramento, CA 95818, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.4898492,38.5613556,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango by the River Milonga</strong><p><strong>address:</strong> 128 J St, Sacramento, CA 95814, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: rivertango.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-121.50455,38.583034,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Toca Tango</strong><p><strong>address:</strong> 2170, 12750 Carmel Country Rd #103, San Diego, CA 92130, USA<br /><strong>time:</strong> 20:00 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: theartoftango.net/tocatango.html</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-117.2299734,32.9536633,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Madonna Practica</strong><p><strong>address:</strong> 100 Madonna Rd, San Luis Obispo, CA 93405, USA<br /><strong>time:</strong> 18:00 - 19:45 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: ken@artmusic.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-120.6755535,35.2669227,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Rich's Practica @ Veterans, SLO</strong><p><strong>address:</strong> 801 Grand Ave, San Luis Obispo, CA 93401, USA<br /><strong>time:</strong> 20:00 - 21:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: rich@slotango.org</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-120.6526848,35.2887009,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Abrazo</strong><p><strong>address:</strong> 2880 Broad St, San Luis Obispo, CA 93401, USA<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> phone: 805-762-4688 - email: marylwhite.mail@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-120.6510495,35.2638411,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>First Friday Milonga</strong><p><strong>address:</strong> 365 Quintana Rd, Morro Bay, CA 93442, USA<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> phone: 8053053327 - email: hypsizigus@yahoo.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-120.8488075,35.3714544,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonguita</strong><p><strong>address:</strong> 2880 Broad St, San Luis Obispo, CA 93401, USA<br /><strong>time:</strong> 20:15 - 22:30 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> phone: (805)762-4688 - email: marylwhite.mail@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-120.6510495,35.2638411,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Sentimental San Mateo</strong><p><strong>address:</strong> 217 S Claremont St, San Mateo, CA 94401, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: (415) 573-5735 - facebook: https://www.facebook.com/milongasentimental</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.3208715,37.5671414,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga de los Mendoza</strong><p><strong>address:</strong> 85 43rd Ave, San Mateo, CA 94403, USA<br /><strong>time:</strong> 21:30 - 00:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 415-468-9226 - website: www.dndance.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.2900661,37.5284761,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Sentimental San Mateo </strong><p><strong>address:</strong> 217 S Claremont St, San Mateo, CA 94401, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: (415) 573-5735 - facebook: https://www.facebook.com/milongasentimental</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.3208715,37.5671414,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Practica in San Mateo</strong><p><strong>address:</strong> 217 S Claremont St, San Mateo, CA 94401, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (415) 573-5735 - email: info@motionartscenter.com - website: www.TangoClasses.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.3208715,37.5671414,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Sunday practica in San Mateo</strong><p><strong>address:</strong> 2209 S El Camino Real, San Mateo, CA 94403, USA<br /><strong>time:</strong> 14:00 - 16:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-122.307192,37.5461861,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Argentine tango Practica. San Bruno</strong><p><strong>address:</strong> 757 San Mateo Ave, San Bruno, CA 94066, USA<br /><strong>time:</strong> 12:00 - 14:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: goldenagetangoacademy@gmail.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4116632,37.6294735,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica in San Mateo</strong><p><strong>address:</strong> 2209 S El Camino Real, San Mateo, CA 94403, USA<br /><strong>time:</strong> 19:30 - 21:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.307192,37.5461861,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Time</strong><p><strong>address:</strong> 217 S Claremont St, San Mateo, CA 94401, USA<br /><strong>time:</strong> 22:00 - 23:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (415) 573-5735 - email: info@motionartscenter.com - website: www.MotionArtsCenter.com</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.3208715,37.5671414,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Free Practica On Saturday Afternoon</strong><p><strong>address:</strong> 711 Nevada St, Redwood City, CA 94061, USA<br /><strong>time:</strong> 14:00 - 16:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.2388664,37.4723625,0],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Takuu</strong><p><strong>address:</strong> Seminario, 360, Santiago, Chile<br /><strong>time:</strong>19:30 - 00:00<br /><strong>price:</strong> $3.500 CLP<br /><strong>contact:</strong>Facebook: https://www.facebook.com/Takuu.Tango</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-70.6288563,-33.4454573],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Bienmilonga</strong><p><strong>address:</strong> Seminario, 360, Santiago, Chile<br /><strong>time:</strong>21:30 - 00:30<br /><strong>price:</strong> $1.500 CLP<br /><strong>contact:</strong>Facebook: https://www.facebook.com/Takuu.Tango</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-70.6288563,-33.4454573],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Gotango</strong><p><strong>address:</strong> Seminario, 360, Santiago, Chile<br /><strong>time:</strong>22:00 - 00:30<br /><strong>price:</strong> n/a<br /><strong>contact:</strong>Facebook: https://www.facebook.com/Takuu.Tango</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-70.6288826,-33.4453811],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Anochi</strong><p><strong>address:</strong> Seminario, 360, Santiago, Chile<br /><strong>time:</strong>22:00 - 00:30<br /><strong>price:</strong> n/a<br /><strong>contact:</strong>Facebook: https://www.facebook.com/Takuu.Tango</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-70.6288826,-33.4453811],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Magaldi Tango Club</strong><p><strong>address:</strong> Santiago 1349, Santiago, Región Metropolitana, Chile<br /><strong>time:</strong>22:00 - 00:30<br /><strong>price:</strong> $1.500 CLP<br /><strong>contact:</strong> n/a<p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-70.5900885,-33.6280291],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Studio Tango Montreal</strong><p><strong>address:</strong> 7755 Boul St-Laurent #200-A<br /><strong>time:</strong> 20:30 - 01:30 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong>contact: phone: 514 844-2786 - email: info@studiotango.ca - website: http://www.studiotango.ca/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.6262532,45.5377314],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Puntazo (Peña Chung King)</strong><p><strong>address:</strong> Gral. Alvear 627, Y4600AJE San Salvador de Jujuy, Jujuy, Argentina<br /><strong>time:</strong> 23:00 - 04:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong>contact: phone: 38851831 - Facebook: /Puntazo Tango  and /Fernando Moreno</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-65.3020102,-24.1839911],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pecado Nuevo (in Arde Roma)</strong><p><strong>address:</strong> Av. Gdor. José María Fascio 1076, San Salvador de Jujuy, Jujuy, Argentina<br /><strong>time:</strong> 21:30 - 04:00 <br /><strong>price:</strong> 60 $AR<br /><strong>contact:</strong>contact: phone: +54 3886869008 - email: sf_pablo@hotmail.com - Facebook: /PecadoNuevoMilonga </p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-65.3081793,-24.1808437],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cozy Zone</strong><p><strong>address:</strong> 2164 Boul de Maisonneuve E, Montréal, QC H2K 2E1, Canada<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a </p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.5507561,45.5261826],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Diversión</strong><p><strong>address:</strong> calle San Martín 580, San Carlos de Bariloche, Río Negro, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> 50 $AR<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.3185022,-41.1330139],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Diversión</strong><p><strong>address:</strong> calle San Martín 580, San Carlos de Bariloche, Río Negro, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> 50 $AR<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.318441,-41.1330053],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong> Arrabalera (in FASTA school)</strong><p><strong>address:</strong> 800-898 DE LOS, Av. de los Pioneros, San Carlos de Bariloche, Río Negro, Argentina<br /><strong>time:</strong> 22:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.3165305,-41.1399009],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Mariposita en Andanzas</strong><p><strong>address:</strong> calle Quaglia 566, San Carlos de Bariloche, Río Negro, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> 150$AR<br /><strong>contact:</strong> Phone: 154250300</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.3090231,-41.13757],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Mariposita en Andanzas</strong><p><strong>address:</strong> calle Quaglia 566, San Carlos de Bariloche, Río Negro, Argentina<br /><strong>time:</strong> 22:00 - 03:00 <br /><strong>price:</strong> 150$AR<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.3088655,-41.1374657],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Lunes de Milonga</strong><p><strong>address:</strong> Carrer de Feliu, 18, Bajos, 08041 Barcelona, España<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> Phone: +34 637240437</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.1749394,41.4177564],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica en la pista de la biblioteca</strong><p><strong>address:</strong> Av. San Martín & Gral. Roca, El Bolsón, Río Negro, Argentina<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> Phone: 11 62999149 or 2044 157990</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.535295,-41.9638919],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El tango sear popular o no sera nada</strong><p><strong>address:</strong> Av. San Martín & Gral. Roca, El Bolsón, Río Negro, Argentina<br /><strong>time:</strong> 19:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> Phone: 2944 913353</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.5352708,-41.9639079],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tinta roja</strong><p><strong>address:</strong> Padre Feliciano, El Bolsón, Río Negro, Argentina (at Terrazas del Piltri)<br /><strong>time:</strong> 22:30 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> Phone: 2944 629228</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.5365278,-41.9647238],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El tango sear popular o no sera nada</strong><p><strong>address:</strong> Av. San Martín & Gral. Roca, El Bolsón, Río Negro, Argentina<br /><strong>time:</strong> 19:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> Phone: 2944 913353</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.535295,-41.9638919],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tinta roja</strong><p><strong>address:</strong> Pastorino 1941, El Bolsón, Río Negro, Argentina<br /><strong>time:</strong> 22:30 - 03:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> Phone: 2944 629228</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.5304952,-41.9750196],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>CDA Tango</strong><p><strong>address:</strong> 411 N 15th St, Coeur d'Alene, ID 83814, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-116.7658686,47.6764466],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>3rd Friday Riolo Milonga</strong><p><strong>address:</strong> 502 N Capitol Ave, Indianapolis, IN 46204, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-86.1619023,39.7744656],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Dilkusha</strong><p><strong>address:</strong> 805 S Meridian St, Indianapolis, IN 46225, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-86.1590515,39.756353],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Traditional Milonga</strong><p><strong>address:</strong> 805 S Meridian St, Indianapolis, IN 46225, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-86.1590515,39.756353],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Lawrence Tango Practica</strong><p><strong>address:</strong> 722 Massachusetts St, Lawrence, KS 66044, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-95.2354357,38.9703952],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga NUEVA</strong><p><strong>address:</strong> 16 Bow St, Somerville, MA 02143, USA<br /><strong>time:</strong> 21:00 - 01:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> facebook: https://www.facebook.com/groups/milonganueva/ - website: www.milonganueva.net</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-71.0972088,42.3807113],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Worcester Tango Weekly Practica</strong><p><strong>address:</strong> 1 Salem St, Worcester, MA 01608, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: worcestertango.org</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.7988947,42.2600388],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Argentine Tango practica</strong><p><strong>address:</strong> 10660 E Carter Rd, Traverse City, MI 49684, USA<br /><strong>time:</strong> 19:30 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 231-883-1987</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-85.641568,44.78051],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga</strong><p><strong>address:</strong> 164 E Maple Rd, Troy, MI 48083, USA<br /><strong>time:</strong> 20:15 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 248-808-6099 - website: www.citystyletango.org</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-83.1437086,42.5478139],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Desperadoes Practica</strong><p><strong>address:</strong> 2507 East 25th Street, Minneapolis, MN 55406, USA<br /><strong>time:</strong> 19:30 - 21:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-93.2358223,44.9571308],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga RUSA</strong><p><strong>address:</strong> 307 E 5th St, Austin, TX 78701, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> email: tkamrath@gmail.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-97.7402698,30.2659539],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Esquina Passion Milonga</strong><p><strong>address:</strong> 209 Pedernales St, Austin, TX 78702, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: info@esquinatangoaustin.com - website: www.esquinatangoaustin.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-97.7157242,30.2559149],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Tazza Fresca Milonga</strong><p><strong>address:</strong> 519 W 37th St, Austin, TX 78705, USA<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-97.7385936,30.3016326],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga Cambiante</strong><p><strong>address:</strong> 190 Cedar Ln, Santa Barbara, CA 93108, USA<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-119.6739539,34.4329232],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga Principal</strong><p><strong>address:</strong> 20 W Calle Laureles, Santa Barbara, CA 93105, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: brunotheonly@yahoo.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-119.7283231,34.4396071],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Regular Monday Practica</strong><p><strong>address:</strong> 20 W Calle Laureles, Santa Barbara, CA 93105, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> $5<br /><strong>n/a</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-119.7283231,34.4396071],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tuesday Night Tango in Palo Alto</strong><p><strong>address:</strong> 4161 Alma St, Palo Alto, CA 94306, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong>website: tntango.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.1119247,37.4112553],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga</strong><p><strong>address:</strong> 1824 Hillsdale Ave, San Jose, CA 95124, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.9179649,37.2620436],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Igor Polk Practica</strong><p><strong>address:</strong> 3921 Fabian Way, Palo Alto, CA 94303, USA<br /><strong>time:</strong> 22:45 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (510)-582-8711 - email: ipolk@virtuar.com - website: www.virtuar.com/jcc/</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.1031421,37.423049],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Sunnyvale Guided Practica</strong><p><strong>address:</strong> 905 Kifer Rd, Sunnyvale, CA 94086, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $8<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.0099421,37.3748243],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Starlite Practica</strong><p><strong>address:</strong> 5178 Moorpark Ave, San Jose, CA 95129, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: (510) 394-7478</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-121.9950504,37.3088846],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Nora & Ed Practica</strong><p><strong>address:</strong> 4161 Alma St, Palo Alto, CA 94306, USA<br /><strong>time:</strong> 22:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: n/a - email: misstango@hotmail.com - facebook: www.facebook.com/norayed.tango</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.1119247,37.4112553],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Ecstasy</strong><p><strong>address:</strong> 924 Soquel Ave, Santa Cruz, CA 95062, USA<br /><strong>time:</strong> 19:00 - 21:30 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> phone: 831-227-0138</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.0131568,36.9783428],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>John & Nancy Lingemann Practica</strong><p><strong>address:</strong> 532 Center St, Santa Cruz, CA 95060, USA<br /><strong>time:</strong> 21:15 - 22:30 <br /><strong>price:</strong> $3<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.0271764,36.9722813],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Santa Cruz Milonga</strong><p><strong>address:</strong> 418 Front St, Santa Cruz, CA 95060, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 831-325-6760</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.0238538,36.9709867],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Desde El Alma</strong><p><strong>address:</strong> 2470 El Rancho Dr, Santa Cruz, CA 95060, USA<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.0233753,37.0276262],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 1016 H St, Modesto, CA 95354, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-120.9972542,37.6388004],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Equinox</strong><p><strong>address:</strong> 102 Besant Rd, Ojai, CA 93023 USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> website: www.QuantumTango.net</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-119.2780468,34.4429724],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Incident</strong><p><strong>address:</strong> 113 S Montgomery St, Ojai, CA 93023, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> website: quantumtango.net/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-119.2437843,34.446934],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>X Session - Tango Exchange</strong><p><strong>address:</strong> 454 E Main St, Ventura, CA 93001, USA<br /><strong>time:</strong> 18:30 - 19:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: quantumtango.net</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-119.2938342,34.2805661],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Modern Music Alternative Milonga</strong><p><strong>address:</strong> 12613 Washington Blvd, Los Angeles, CA 90066, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 310 880-3322 - email: mcc_la@yahoo.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-118.43277,33.9969398],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Cypher Spot Tango Club</strong><p><strong>address:</strong> 212 South La Brea Ave, Inglewood, CA 90301, USA<br /><strong>time:</strong> 16:00 - 19:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-118.3530951,33.9612701],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Encuentro</strong><p><strong>address:</strong> 4346 Woodman Ave, Sherman Oaks, CA 91423, USA<br /><strong>time:</strong> 21:30 - 03:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-118.4307774,34.1494076],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Open Practica</strong><p><strong>address:</strong> 12958 W Washington Blvd, Los Angeles, CA 90066, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> website: www.oxygentango.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-118.4392071,33.9945795],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>PBP Practica</strong><p><strong>address:</strong> 9431 Venice Blvd, Culver City, CA 90232, USA<br /><strong>time:</strong> 15:00 - 17:30 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> email: makelatango@gmail.com - website: www.makelatango.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-118.3959389,34.025374],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tu Practica</strong><p><strong>address:</strong> 4346 Woodman Ave, Sherman Oaks, CA 91423, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-118.4307774,34.1494076],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Sueño Practica</strong><p><strong>address:</strong> 40 Railroad Ave, Milford, CT 06460, USA<br /><strong>time:</strong> 21:00 - 22:30 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> email: info@tangosueno.com - website: www.tangosueno.com/milonga.html</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.0588133,41.2226098],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga TS</strong><p><strong>address:</strong> 40 Railroad Ave, Milford, CT 06460, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong>  email: info@tangosueno.com - website: www.tangosueno.com/milonga.html</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-73.0588133,41.2226098],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Hot Water Practica</strong><p><strong>address:</strong> 818 S Water St, Milwaukee, WI 53204, USA<br /><strong>time:</strong> 19:45 - 21:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 262 353 0922<br> - email: hal53024@sbcglobal.net</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.9064714,43.0226815],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Middlebury tango practice group</strong><p><strong>address:</strong> 68 S Pleasant St, Middlebury, VT 05753, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: swstudio2@gmail.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-73.1668049,44.0131522],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Brattleboro practica</strong><p><strong>address:</strong> 118 Elliot St, Brattleboro, VT 05301, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: sally.brat.tango@gmail.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-72.5606901,42.8513176],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Brattleboro Milonga</strong><p><strong>address:</strong> 118 Elliot St, Brattleboro, VT 05301, USA<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: sally.brat.tango@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.5606901,42.8513176],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 3419 Weeks Hill Rd, Stowe, VT 05672, USA<br /><strong>time:</strong> 15:30 - 18:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: vic.2004@hotmail.com - website: aliciatango.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.721695,44.511002],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango with TangoOSU</strong><p><strong>address:</strong> Women's Field House, 1790 Cannon Dr, Columbus, OH , USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> facebook: https://www.facebook.com/groups/tangoosu/ - website: https://u.osu.edu/tangoosu/</p>",
"business":" practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-83.0210756,39.9981049],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Missoula</strong><p><strong>address:</strong> 121 W Main St, Missoula, MT 59802, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong>$10<br /><strong>contact:</strong> phone: 406-443-3439 - website: www.ddcmontana.com></p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-113.9955045,46.8710662],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga TS New Dimensions in Norwalk</strong><p><strong>address:</strong> 15 Cross St, Norwalk, CT 06851, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> email: info@tangosueno.com - website: www.tangosueno.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-73.4140394,41.1202004],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga El Abrazo de Gainesville</strong><p><strong>address:</strong> 524 NW 1st St, Gainesville, FL 32601, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 352-226-1103 - website: https://milongaelabrazodegainesville.wordpress.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-82.3260231,29.6564297],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Practica Thursdays</strong><p><strong>address:</strong> 3932 W Hillsborough Ave, Tampa, FL 33614, USA<br /><strong>time:</strong> 20:30 - 22:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-82.5107989,27.9945212],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Lab Orlando</strong><p><strong>address:</strong> 1720 N Goldenrod Rd #7, Orlando, FL 32807, USA<br /><strong>time:</strong> 21:45 - 23:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: n/a - email: tangolaborlando@gmail.com - website: www.tangolaborlando.com</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-81.2873535,28.57076],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Hilo, Robin's Milonga</strong><p><strong>address:</strong> 603 W Lanikaula St, Hilo, HI 96720, USA<br /><strong>time:</strong> 19:00 - 22:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> facebook: https://www.facebook.com/Hale-Lanikaula-58155204131/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-155.083499,19.7029535],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 1428 W 9th Ave, Spokane, WA 99204, USA<br /><strong>time:</strong> 14:30 - 16:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong>  email: info4les2@yahoo.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.4338723,47.6477463],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango @ Affinity</strong><p><strong>address:</strong> 3304 E 44th Ave, Spokane, WA 99223, USA<br /><strong>time:</strong> 12:00 - 14:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.3611242,47.6129339],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango @ Satori</strong><p><strong>address:</strong> 122 S Monroe St, Spokane, WA 99201, USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.4267784,47.6557777],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango @ Satori</strong><p><strong>address:</strong> 122 S Monroe St, Spokane, WA 99201, USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.4267784,47.6557777],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango @ Satori</strong><p><strong>address:</strong> 122 S Monroe St, Spokane, WA 99201, USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.4267784,47.6557777],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga on the Move</strong><p><strong>address:</strong> 1400 N Meadowwood Ln, Liberty Lake, WA 99019, USA<br /><strong>time:</strong> 18:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-117.101827,47.6709992],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Gato y Perros</strong><p><strong>address:</strong> 408 E 7th Ave, Spokane, WA 99202, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 509-290-8138 - email: mattdoval@mac.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-117.4041654,47.6484271],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Queen City Milonga</strong><p><strong>address:</strong> 20 Crowley St, Burlington, VT 05401, USA<br /><strong>time:</strong> 19:45 - 22:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 802.999.1798</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-73.2236856,44.4873182],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Queen City Milonga</strong><p><strong>address:</strong> 20 Crowley St, Burlington, VT 05401, USA<br /><strong>time:</strong> 19:45 - 22:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 802.999.1798</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-73.2236856,44.4873182],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango del Barrio Weekly Practica</strong><p><strong>address:</strong> 6110 Hamilton Ave, Cincinnati, OH 45224 USA<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-84.5474595,39.2021957],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga del Barrio Norte</strong><p><strong>address:</strong> 6110 Hamilton Ave, Cincinnati, OH 45224 USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.5474595,39.2021957],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>New Dancer Milonga</strong><p><strong>address:</strong> 6110 Hamilton Ave, Cincinnati, OH 45224 USA<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.5474595,39.2021957],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Entre Tango y Tango Milonga</strong><p><strong>address:</strong> 6110 Hamilton Ave, Cincinnati, OH 45224 USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.5474595,39.2021957],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Craft Practica</strong><p><strong>address:</strong> 22 Martin St, Reno, NV 89509, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-119.8082539,39.5150046],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>VZT 3rd Saturday Milonga</strong><p><strong>address:</strong> 2540 Sutro St, Reno, NV 89512, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: vz@vztango.com - website: www.vztango.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-119.7978672,39.5526585],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga IDEAL</strong><p><strong>address:</strong> 5 High St, Medford, MA 02155, USA<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> website: www.ultimatetango.com/milonga-ideal/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.1097138,42.4184659],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Boston Sunday Practica</strong><p><strong>address:</strong> 26 New St, Cambridge, MA 02138, USA<br /><strong>time:</strong> 15:00 - 18:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> facebook: https://www.facebook.com/groups/169457099614/ - website: sundaypractica.com/</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.1404605,42.3878553],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica Chiquita</strong><p><strong>address:</strong> 5 High St, Medford, MA 02155, USA<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> website: www.ultimatetango.com/guided-practica/</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-71.1097138,42.4184659],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Brisa</strong><p><strong>address:</strong> 3801 Bee Ridge Rd, Sarasota, FL 34233, USA<br /><strong>time:</strong> 17:00 - 20:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-82.4928162,27.2992053],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Victor & Aksana Tango</strong><p><strong>address:</strong> 3801 Bee Ridge Rd, Sarasota, FL 34233, USA<br /><strong>time:</strong> 20:00 - 21:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-82.4928162,27.2992053],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practilonga</strong><p><strong>address:</strong> 3801 Bee Ridge Rd, Sarasota, FL 34233, USA<br /><strong>time:</strong> 16:00 - 18:00 <br /><strong>price:</strong> 10$<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-82.4928162,27.2992053],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga El Cabeceo</strong><p><strong>address:</strong> 3512 S Dixie Hwy, West Palm Beach, FL 33405, USA<br /><strong>time:</strong> 16:00 - 19:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.054352,26.680013],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Florida All Night Milonga</strong><p><strong>address:</strong> 1023 N Dixie Hwy, West Palm Beach, FL 33401, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 954- 6070910 - email: allnightmilonga@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-80.053528,26.722365],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Chiqui's Milonga at American Finnish Cub</strong><p><strong>address:</strong> 908 Lehto Ln, Lake Worth, FL 33461, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 561-602-7085 - email: lamilonguitadelakeworth@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1082662,26.6082769],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Lab - Stuart</strong><p><strong>address:</strong> 1712 NW Federal Hwy, Stuart, FL 34994, USA<br /><strong>time:</strong> 20:30 - 21:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.265552,27.225694],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga de Corazon</strong><p><strong>address:</strong> 1712 NW Federal Hwy, Stuart, FL 34994, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-80.265552,27.225694],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>HARTS Milongueta</strong><p><strong>address:</strong> 1385 East St, New Britain, CT 06053, USA<br /><strong>time:</strong> 14:00 - 17:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.7590571,41.690411],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 1385 East St, New Britain, CT 06053, USA<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: www.HARTSTANGO.net</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-72.7590571,41.690411],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Sueño Academy </strong><p><strong>address:</strong> 190 Court St, Middletown, CT 06457, USA<br /><strong>time:</strong> 21:30 - 22:30 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> email: info@tangosueno.com - website: www.tangosueno.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-72.6502192,41.5604303],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Harts Milonga</strong><p><strong>address:</strong> 1385 East St, New Britain, CT 06053, USA<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong>  website: www.HARTSTANGO.net</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.7590571,41.690411],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Ideal</strong><p><strong>address:</strong> 211 SE 1st Ave, Hallandale Beach, FL 33009, USA<br /><strong>time:</strong> 20:15 - 02:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 305-905-3935 - email: tangofantasy@aol.com - website: www.tangofantasy.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1475248,25.9826595],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club International Tango Argentino (CITA)</strong><p><strong>address:</strong> 700 S Dixie Hwy, Hollywood, FL 33020, USA<br /><strong>time:</strong> 20:30 - 01:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1498015,26.0058938],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Practica</strong><p><strong>address:</strong> 202 NE 1st Ave, Hallandale Beach, FL 33009, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 786-424-3752 - email: carolinabalmaseda@gmail.com - facebook: https://www.facebook.com/groups/525750840952407/</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1477736,25.9880728],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Pituca</strong><p><strong>address:</strong> 2305 E Atlantic Blvd, Pompano Beach, FL 33062, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 954-646-2480 - website: www.tangoentredos.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.100735,26.2319366],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Las Pebetas</strong><p><strong>address:</strong> 3129 W Hallandale Beach Blvd, Hallandale Beach, FL 33009, USA<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 3057760538</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-80.1685426,25.9865639],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>DDC Practica</strong><p><strong>address:</strong> 121 W Main St, Missoula, MT 59802, USA<br /><strong>time:</strong> 18:00 - 19:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 406-443-3439 - website: www.ddcmontana.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-113.9955045,46.8710662],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Shakespeare Practica</strong><p><strong>address:</strong> 103 S 3rd St W, Missoula, MT 59801, USA<br /><strong>time:</strong> 18:00 - 19:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> facebook: https://www.facebook.com/groups/317032738368807 - website: tangomissoula.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-113.9978564,46.8668103],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Elegante</strong><p><strong>address:</strong> 110 W Sahara Ave #130, Las Vegas, NV 89117 USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-115.2538247,36.1444156],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Friday Tango Nights</strong><p><strong>address:</strong> 6315 S Rainbow Blvd #103, Las Vegas, NV 89118, USA<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-115.2433832,36.073662],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Cabaceo Milonga</strong><p><strong>address:</strong> 6315 S Rainbow Blvd #103, Las Vegas, NV 89118, USA<br /><strong>time:</strong> 21:30 - 01:30 <br /><strong>price:</strong> $18<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-115.2433832,36.073662],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Chiche Bombon de Judy y Jon</strong><p><strong>address:</strong> 6315 S Rainbow Blvd #103, Las Vegas, NV 89118, USA<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> $18<br /><strong>contact:</strong>  phone: 520.907.2050  - email: tangowithjudy@yahoo.com </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-115.2433832,36.073662],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Bohemian Milonga</strong><p><strong>address:</strong> 2301 Renaissance Dr, Las Vegas, NV 89119, USA<br /><strong>time:</strong> 21:30 - 02:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-115.1217275,36.1035171],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>WMT Community Tango Lab Practica</strong><p><strong>address:</strong> 33 Hawley St, Northampton, MA 01060, USA<br /><strong>time:</strong> 19:00 - 21:30 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> email: westernMAtango@gmail.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-72.6265279,42.3197037],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>WMT practica</strong><p><strong>address:</strong> 33 Hawley St, Northampton, MA 01060, USA<br /><strong>time:</strong> 19:00 - 21:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: westernmatango@gmail.com</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-72.6265279,42.3197037],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Estudio Tango Milonga </strong><p><strong>address:</strong> 1046 S East St, Amherst, MA 01002, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: abbyconnolly@gmail.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.503409,42.340719],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>WMT milonga</strong><p><strong>address:</strong> 33 Hawley St, Northampton, MA 01060, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $30<br /><strong>contact:</strong> email: westernmatango@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-72.6265279,42.3197037],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>George's IslaTango Monday Milonga</strong><p><strong>address:</strong> 932 Ward Ave, Honolulu, HI 96814, USA<br /><strong>time:</strong> 19:30 - 22:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: islatango@hotmail.com - website: www.islatango.com</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-157.850924,21.3017653],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Medici's - Tango Milonga</strong><p><strong>address:</strong> 2754 Woodlawn Dr #7, Honolulu, HI 96822, USA<br /><strong>time:</strong> 20:00 - 22:30 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-157.8109651,21.3085264],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>George's Tango</strong><p><strong>address:</strong> 932 Ward Ave, Honolulu, HI 96814, USA<br /><strong>time:</strong> 20:30 - 22:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> email: islatango@hotmail.com - website: www.islatango.com</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-157.850924,21.3017653],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>2 Dollar Tango - Thurs Practica</strong><p><strong>address:</strong> 150 N King St #202, Honolulu, HI 96817, USA<br /><strong>time:</strong> 20:00 - 21:00 <br /><strong>price:</strong> $2<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-157.8639878,21.3128392],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica @ HonClub</strong><p><strong>address:</strong> 932 Ward Ave, Honolulu, HI 96814, USA<br /><strong>time:</strong> 20:30 - 22:30 <br /><strong>price:</strong> $8<br /><strong>contact:</strong> email: islatango@hotmail.com - website: www.islatango.com</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-157.850924,21.3017653],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Paradise Tango's Monthly Milonga</strong><p><strong>address:</strong> 150 N King St #202, Honolulu, HI 96817, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 831-239-6529 - email: jenny@paradisetango.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-157.8639878,21.3128392],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango 21: Milonga La Baldosita</strong><p><strong>address:</strong> 1240 W Randolph St, Chicago, IL 60607, USA<br /><strong>time:</strong> 19:00 - 22:30 <br /><strong>price:</strong> $16<br /><strong>contact:</strong> phone: 312-217-0304 - email: dinah@tango21.com - website: www.tango21.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6581901,41.88461],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Al's TangoChicago Practica Bridgeport</strong><p><strong>address:</strong> 735 W 35th St, Chicago, IL 60616, USA<br /><strong>time:</strong> 18:00 - 20:00 <br /><strong>price:</strong> $25<br /><strong>contact:</strong> email: al@tangochicago.com - website: www.tangochicago.com</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6452996,41.8306387],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Villa Park Practilonga</strong><p><strong>address:</strong> 207 S Villa Ave #250, Villa Park, IL 60181, USA<br /><strong>time:</strong> 19:00 - 21:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 312-404-5242</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.9687827,41.8863867],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Suburbana</strong><p><strong>address:</strong> 25 W Davis St, Arlington Heights, IL 60005, USA<br /><strong>time:</strong> 20:15 - 22:15 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.9838678,42.0835082],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practica w/Rod + Melit</strong><p><strong>address:</strong> XOXOXO<br /><strong>time:</strong> 21:45 - 00:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6878895,41.9683763],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Al's TangoChicago Practica Bridgeport</strong><p><strong>address:</strong> 735 W 35th St, Chicago, IL 60616, USA<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> $25<br /><strong>contact:</strong> phone: 312-788-3408 - email: al@tangochicago.com - website: www.tangochicago.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6452996,41.8306387],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Pizpireta</strong><p><strong>address:</strong> 4761 N Lincoln Ave, Chicago, IL 60625, USA<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6878895,41.9683763],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Anna & Ernest Practica</strong><p><strong>address:</strong> 2546 W Peterson Ave, Chicago, IL 60659, USA<br /><strong>time:</strong> 21:30 - 23:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 773-726-9492</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6940018,41.990728],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Al's TangoChicago Practica Bridgeport</strong><p><strong>address:</strong> 735 W 35th St, Chicago, IL 60616, USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> $25<br /><strong>contact:</strong> email: al@tangochicago.com - website: www.tangochicago.com</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6452996,41.8306387],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Dancing Fridays</strong><p><strong>address:</strong> 4809 N Ravenswood Ave #117, Chicago, IL 60640, USA<br /><strong>time:</strong> 22:00 - 01:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6737339,41.9693229],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Al's TangoChicago Practica Bridgeport</strong><p><strong>address:</strong> 735 W 35th St, Chicago, IL 60616, USA<br /><strong>time:</strong> 21:30 - 02:00 <br /><strong>price:</strong> $25<br /><strong>contact:</strong> email: al@tangochicago.com - website: www.tangochicago.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.6452996,41.8306387],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga at Wheeling Krug Community Circle</strong><p><strong>address:</strong> 1400 S Wolf Rd, Wheeling, IL 60090, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 224-423-5784</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.908758,42.108441],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Practice</strong><p><strong>address:</strong> 1506 E 53rd St, Chicago, IL 60615, USA<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 773-726-9492 - email: alohatango2002@yahoo.com - facebook: https://www.facebook.com/ErnestWilliamsTango/</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-87.5887855,41.7996603],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Porteña</strong><p><strong>address:</strong> 4146 N Elston Ave, Chicago, IL 60618, USA<br /><strong>time:</strong> 21:30 - 01:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 847-331-7172</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-87.7229413,41.956049],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Pavadita - Traditional Milonga</strong><p><strong>address:</strong> 3868 N Lincoln Ave, Chicago, IL 60613, USA<br /><strong>time:</strong> 21:30 - 02:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-87.6777992,41.9521465],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>312 Milonga</strong><p><strong>address:</strong> 3868 N Lincoln Ave, Chicago, IL 60613, USA<br /><strong>time:</strong> 21:30 - 02:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-87.6777992,41.9521465],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Los Besos Milonguita</strong><p><strong>address:</strong> 3868 N Lincoln Ave, Chicago, IL 60613, USA<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-87.6777992,41.9521465],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango OUT Milonga</strong><p><strong>address:</strong> 2701 Collins Ave, Miami Beach, FL 33140, USA<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 786-530-7876 - faceboook: www.facebook.com/tangowithus</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.125374,25.8038139],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong> Oscar Caballero & Roxana Garber Practica</strong><p><strong>address:</strong> 1613 NE 163rd St, North Miami Beach, FL 33162, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 786.221.5976 - email: info@tangotimes.us - website: www.tangotimes.us</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1676924,25.9261846],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Guacha</strong><p><strong>address:</strong> 1465 SW 8th St, Miami, FL 33135, USA<br /><strong>time:</strong> 21:30 - 00:15 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 7862623087</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.2190165,25.7659062],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong> Oscar Caballero & Roxana Garber Practica</strong><p><strong>address:</strong> 1613 NE 163rd St, North Miami Beach, FL 33162, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 786.221.5976 - email: info@tangotimes.us - website: www.tangotimes.us</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1676924,25.9261846],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango By The Bay</strong><p><strong>address:</strong> 7601 E Treasure Dr, North Bay Village, FL 33141, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: (786)355-0882 - facebook: https://www.facebook.com/diegosantanat</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1458676,25.8476718],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong> Oscar Caballero & Roxana Garber Practica</strong><p><strong>address:</strong> 1613 NE 163rd St, North Miami Beach, FL 33162, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 786.221.5976 - email: info@tangotimes.us - website: www.tangotimes.us</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1676924,25.9261846],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Open Air Tango on Lincoln Road</strong><p><strong>address:</strong> Jefferson Ave & Lincoln Rd, Miami Beach, FL 33139, USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-80.1376806,25.7904278],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Thursdays in Delray Beach</strong><p><strong>address:</strong> 802 NE 1st St, Delray Beach, FL 33483, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 786-262-3087</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.0645808,26.463177],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong> Oscar Caballero & Roxana Garber Practica</strong><p><strong>address:</strong> 1613 NE 163rd St, North Miami Beach, FL 33162, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 786.221.5976 - email: info@tangotimes.us - website: www.tangotimes.us</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1676924,25.9261846],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango OUT Class/Practice on Miami Beacht</strong><p><strong>address:</strong> 1130 Washington Ave, Miami Beach, FL 33139, USA<br /><strong>time:</strong> 20:00 - 21:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 786-530-7876 - facebook: www.facebook.com/tangowithus</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.1326305,25.7826246],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>TangOcho Milonga</strong><p><strong>address:</strong> 1501 SW 8th St, Miami, FL 33135, USA<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 786-991-5169</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-80.2195979,25.7658561],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Paz</strong><p><strong>address:</strong> 401 Van Ness Ave #212, San Francisco, CA 94102, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: goldenagetangoacademy@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.42101,37.7795732],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Fundamentals Practica</strong><p><strong>address:</strong> 3450 3rd St h, San Francisco, CA 94124, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $7<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.387507,37.745874],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Valenciano Milonga</strong><p><strong>address:</strong> 1153 Valencia St, San Francisco, CA 94110, USA<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> website: www.tangosabor.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4205566,37.7545322],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Switch Tango Series & Practica</strong><p><strong>address:</strong> 455A Valencia St, San Francisco, CA 94103, USA<br /><strong>time:</strong> 21:00 - 22:00 <br /><strong>price:</strong> $16<br /><strong>contact:</strong> phone: 415-335-6384 - facebook: https://www.facebook.com/events/203522696889057 - website: https://switchtangoseries.eventbrite.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4218321,37.7657122],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>CELLspace Alt. Milonga</strong><p><strong>address:</strong> 934 Brannan St, San Francisco, CA 94103, USA<br /><strong>time:</strong> 21:10 - 00:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> facebook: www.facebook.com/ProjectTango - email: ben@project-tango.info - website: project-tango.info/2018/03/2-progressive-series-314-425/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4066262,37.7711778],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Free Tango practica</strong><p><strong>address:</strong> 1900 Geary Blvd, San Francisco, CA 94115, USA<br /><strong>time:</strong> 15:00 - 16:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: goldenagetangoacademy@gmail.com</p>",
"business":"practica",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4350898,37.784588],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga La Paz</strong><p><strong>address:</strong> 401 S Van Ness Ave, San Francisco, CA 94103, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: goldenagetangoacademy@gmail.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.417319,37.766634],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Malevaje Milonga</strong><p><strong>address:</strong> 2424 Mariposa St, San Francisco, CA 94110, USA<br /><strong>time:</strong> 21:10 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: 415-810-6040 - email: caszarry@gmail.com - website: www.tangozarry.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.407929,37.763394],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>La Milonga Genesis</strong><p><strong>address:</strong> 404 Clement St, San Francisco, CA 94118, USA<br /><strong>time:</strong> 22:00 - 01:30 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.4636431,37.7832315],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Bissap Baobab Practica</strong><p><strong>address:</strong> 3372 19th St, San Francisco, CA 94110, USA<br /><strong>time:</strong> 13:00 - 16:00 <br /><strong>price:</strong> $8<br /><strong>contact:</strong>  website: www.sfotango.com</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-122.418809,37.7604125],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Practica</strong><p><strong>address:</strong> 1532 Taraval St, San Francisco, CA 94116, USA<br /><strong>time:</strong> 19:00 - 22:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-122.4827013,37.7430353],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tanguísimo Milonga</strong><p><strong>address:</strong>No. 4, Section 3, Zhongxiao East Road, Da’an District, Taipei City, Taiwan 106<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> NT200<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [121.533403,25.0419759],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Esencia</strong><p><strong>address:</strong> No. 2, Songjiang Road, Zhongshan District, Taipei City, Taiwan 10491<br /><strong>time:</strong> 21:00 - 23:30 <br /><strong>price:</strong> NT200<br /><strong>contact:</strong> phone: 0952963556</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [121.5327183,25.0462724],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Esencia</strong><p><strong>address:</strong> No. 2, Songjiang Road, Zhongshan District, Taipei City, Taiwan 10491<br /><strong>time:</strong> t21:00 - 01:00 <br /><strong>price:</strong> NT250<br /><strong>contact:</strong> phone: 0952963556</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [121.5327183,25.0462724],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tanguísimo Classic Milonga</strong><p><strong>address:</strong> No. 4, Section 3, Zhongxiao East Road, Da’an District, Taipei City, Taiwan 106<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> NT250<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [121.533403,25.0419759],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Little Havana</strong><p><strong>address:</strong>2, Lorong Sahabat, Bukit Bintang, 50200 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia<br /><strong>time:</strong> t21:30 - 23:00 <br /><strong>price:</strong> 10 rm<br /><strong>contact:</strong> phone: 0342510407 - email: info@tangomalaysia.com</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [101.7064422,3.1469688],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Frangipani</strong><p><strong>address:</strong>25, Changkat Bukit Bintang, Bukit Bintang, 50200 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia<br /><strong>time:</strong> 21:30 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 60321443001</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [101.7073716,3.1476089],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Night</strong><p><strong>address:</strong> 5 Chome-2-1 Tsukiji, Chuo-ku, Tokyo-to 104-0045, Japan<br /><strong>time:</strong> 18:30 - 23:00 <br /><strong>price:</strong> 1000 yen<br /><strong>contact:</strong> phone: 0351485001 - website: http://www.tangojapan.com/gyu/cubancafe_eg.htm</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [139.7692035,35.6626569],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Club Tanguisimo</strong><p><strong>address:</strong> 7 Chome Ginza, Chuo, Tokyo 104-0061, Japan<br /><strong>time:</strong> 20:00 - 22:30 <br /><strong>price:</strong> 2800 yen<br /><strong>contact:</strong> phone: 0335445600  - website: http://www.tangodance.co.jp/english/index.htm</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [139.7620704,35.6693143],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Luna de Tango</strong><p><strong>address:</strong> 2 Chome-12-12 Shibuya, Shinjuku-ku, Tōkyō-to 150-0002, Japan<br /><strong>time:</strong> 20:00 - 23:30 <br /><strong>price:</strong> t2500 yen<br /><strong>contact:</strong> phone: 0354660701 - email: lunadetango@lunadetango.com - website: http://homepage3.nifty.com/lunadetango</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [139.7083253,35.6586834],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Puro Tango</strong><p><strong>address:</strong>1 Chome-8-1 Nishishinjuku, Shinjuku-ku, Tōkyō-to 160-0023, Japan<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> t3000 yen<br /><strong>contact:</strong> phone: 0352873307 - website: http://homepage3.nifty.com/purotango</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [139.6967391,35.6909181],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Milonga Nuevo</strong><p><strong>address:</strong> Toi Shan Association Building, 167-169 Hennessy Rd, Wan Chai, Hong Kong<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> HK$60<br /><strong>contact:</strong> email: info@lestango.com.hk - website: http://www.letstango.com.hk/milonga.html</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [114.1738234,22.277909],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango at Jade Lounge</strong><p><strong>address:</strong> 39 Main Rd, Green Point, Cape Town, 8051, South Africa<br /><strong>time:</strong> t21:00 - 00:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 082 397 9868 - email: info@tangocapetown.co.za</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [18.4131484,-33.9094217],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango at 35 on Rose</strong><p><strong>address:</strong> 35 Rose St, Cape Town City Centre, Cape Town, 8000, South Africa<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> R30<br /><strong>contact:</strong> phone: 083 626 2484 - email: mwisisct@mweb.co.za</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [18.4169392,-33.9197665],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Albert Hall Tango</strong><p><strong>address:</strong> 208 Albert Rd, Woodstock, Cape Town, 7915, South Africa<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> R30<br /><strong>contact:</strong> phone: 083 626 2484 - email: mwisisct@mweb.co.za</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [18.4521,-33.9273],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>El Abrazo</strong><p><strong>address:</strong> Bill Peters Dr, Green Point, Cape Town, 8051, South Africa<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> R30<br /><strong>contact:</strong> phone: 711383001 - email: info@capetango.com - website: http://www.capetango.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [18.4005396,-33.905889],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Dance Junxion</strong><p><strong>address:</strong>54 on Bath Hotel, 54 Bath Ave, Rosebank, 2196, South Africa<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> R40<br /><strong>contact:</strong> phone: 011- 442-6462 - email: Jona@candidbaby.co.za</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [28.0407,-26.14555],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>Tango Tensity</strong><p><strong>address:</strong> 123 Long Rd, Northcliff, Randburg, 2115, South Africa<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> R40<br /><strong>contact:</strong> phone: 084 487 8445 - email: touch@tango-tensity.co.za - website: http://tango-tensity.co.za/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [27.9649283,-26.167711],
"type": "Point"
}
},
{
"type": "Feature",
"properties": {
"description": "<strong>The Bakery of Buenos Aires Breakfast Milonga</strong><p><strong>address:</strong>14th Ave & Bagley Terrace, Valeriedene, Randburg, 2195, South Africa<br /><strong>time:</strong> 10:00 - 12:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 011 782 2765</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [27.9534746,-26.1448052],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga en Lo de Brian</strong><p><strong>address:</strong> Av. San Martín 147, Caleta Olivia, Santa Cruz, Argentina<br /><strong>time:</strong> 21:00 to 00:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 2974605906 (Quiroga Luis) - facebook:/Luis Quiroga </p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-67.5178294,-46.4400806],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Glorieta Milonguera</strong><p><strong>address:</strong> Calle José Fuchs y Monseñor Fagnano, Caleta Olivia, Santa Cruz, Argentina<br /><strong>time:</strong> 16:00 to 20:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 01137689136 (kike) - facebook:/La Olivia Milonguera </p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-67.5255293,-46.4399973],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Matine Vida Mía</strong><p><strong>address:</strong>(in Draw Bar-pool) San Martín 372, 9000 Comodoro Rivadavia, Chubut, Argentina<br /><strong>time:</strong> 21:00 - 00:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> facebook: /matinee.vida.mia</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-67.4795254,-45.8610503],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Alma mía</strong><p><strong>address:</strong>Alfredo Palacios 519, Comodoro Rivadavia, Chubut, Argentina<br /><strong>time:</strong> 21:30 - 00:00<br /><strong>price:</strong> 50 $AR<br /><strong>contact:</strong> facebook: /matemilonguero.elrecodo.9</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-67.5239706,-45.8822047],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Confitería Honolulu</strong><p><strong>address:</strong>(bar Honolulu) calle San Martín 263, Comodoro Rivadavia, Chubut, Argentina<br /><strong>time:</strong> 22:30 - 00:00<br /><strong>price:</strong> free<br /><strong>contact:</strong> facebook: https://m.facebook.com/story.php?story_fbid=374645569670339&id=327852191016344</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-67.4777716,-45.8609107],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>El Matemilonguero, El Recodo</strong><p><strong>address:</strong>Alfredo Palacios 519, Comodoro Rivadavia, Chubut, Argentina<br /><strong>time:</strong> 21:00 - 00:00<br /><strong>price:</strong> 50 $AR<br /><strong>contact:</strong> facebook: /matemilonguero.elrecodo.9</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-67.5240594,-45.8821815],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Noche de Tango - Garabato</strong><p><strong>address:</strong>Hotel Seminole 2 1/2 cuadras al Sur, Managua, Nicaragua<br /><strong>time:</strong> 19:00 - 00:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong>  email: tangonicaragua1@gmail.com - facebook: https://www.facebook.com/pg/TangoNicaragua/events/?ref=page_internal</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-86.266526,12.12082],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>El Colectivo del Tango</strong><p><strong>address:</strong> Carrer de les Hortes, 10, 08004 Barcelona, Spain<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> voluntary contribution<br /><strong>contact:</strong> facebook: https://www.facebook.com/El-colectivo-del-tango-1245781802218823/</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [2.1672764,41.37319],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Milonguita de Fantasía de Tango</strong><p><strong>address:</strong> Calle 59, San José, San Pedro, Costa Rica<br /><strong>time:</strong> 19:30 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 2280-3173 / 8336-1616 - email: info@fantasiadetango.com - facebook: https://www.facebook.com/Fantasia-de-Tango-154932740924/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-84.0520265,9.9312088],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Milonguita de Costa Rica Tango</strong><p><strong>address:</strong> Av 15, San José, Costa Rica<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> ₡2000<br /><strong>contact:</strong> phone: 8869-6899 / 8349-1357 - email: infocostaricatango@gmail.com - facebook: https://www.facebook.com/crtango/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-84.0648937,9.938702],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Milonga de Promenade</strong><p><strong>address:</strong> PROMEDADE,<br>San José Province, San Pedro, Costa Rica<br /><strong>time:</strong> 19:30 - 23:00 <br /><strong>price:</strong> ₡3000<br /><strong>contact:</strong> phone: 2283-6660 / 2283-6421 / 2283-0300 - email: info@artespromenade.com - facebook: https://www.facebook.com/prometango</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-84.0554787,9.9257967],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Milonga de Heredia</strong><p><strong>address:</strong> Academia Boga Dance Studio, Heredia Province, San Pablo, Costa Rica<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> ₡3000<br /><strong>contact:</strong> phone: 8345-7060 - email: gigicosta123@yahoo.com - facebook: https://www.facebook.com/bogadancestudio/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-84.1021765,10.00075],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga de Silvia y John</strong><p><strong>address:</strong> Cra. 51 #45-80, Medellín, Antioquia, Colombia<br /><strong>time:</strong> 20:30 - 00:30 <br /><strong>price:</strong> $ 8.000<br /><strong>contact:</strong> email: silviar01@une.net.co</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-75.5697828,6.246492],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Café Proa</strong><p><strong>address:</strong> Avenida España 549, Asunción, Paraguay<br /><strong>time:</strong> t22:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 021 222 456 - email: proa@telesurf.com.py</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-57.6219603,-25.2850871],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Papusa... La Milonga</strong><p><strong>address:</strong> Belisario Salinas 380, La Paz, Bolivia<br /><strong>time:</strong> t19:00 - 01:30 <br /><strong>price:</strong> 25 Bs<br /><strong>contact:</strong> phone: 70122395 - 73070876 - email: papusalamilonga@hotmail.com - website: http://papusalamilonga.blogspot.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-68.1266568,-16.5097156],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga - La Academia de TangoTorino</strong><p><strong>address:</strong> Socabaya 457, La Paz Zona 2, Bolivia<br /><strong>time:</strong> t21:00 - 01:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 591 715 78000 - email: academiatorino@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-68.1343385,-16.4962295],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Baile en el Salón Zitarrosa</strong><p><strong>address:</strong> Avenida Los Castaños, Caracas 1071, Miranda, Venezuela<br /><strong>time:</strong> t20:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +58 212 239 56 82 - email: tangocaracas@tangocaracas.com - website: http://www.tangocaracas.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-66.8254982,10.5022444],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Milonga de Gardel</strong><p><strong>address:</strong> Calle Bolívar 164, Miraflores, Peru<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (00511) 467-1964 - email: citetango@yahoo.com.ar</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0285401,-12.1256858],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Continental</strong><p><strong>address:</strong> Av Javier Prado Oeste 1081, Cercado de Lima 15073, Peru<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 796-5914 / 990-664486 - email: perutango@yahoo.es - website: http://milongaperu.blogspot.com/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0417364,-12.0938713],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>CiTango</strong><p><strong>address:</strong> Sánchez Carrión #131, Lima 15063, Peru<br /><strong>time:</strong> t16:00 - 21:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (511) 9668 5675 - email: gardenia@ci-tango.com - website: http://www.ci-tango.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.020782,-12.148785],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Café Libro</strong><p><strong>address:</strong> Leonidas Plaza, Quito 170143, Ecuador<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 3$<br /><strong>contact:</strong> phone: 00 (593 2) 225 0321 - website: http://www.cafelibro.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-78.4874681,-0.2041304],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Café Libro</strong><p><strong>address:</strong> Leonidas Plaza, Quito 170143, Ecuador<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> 3$<br /><strong>contact:</strong> phone: 00 (593 2) 225 0321 - website: http://www.cafelibro.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-78.4874681,-0.2041304],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Tangueria</strong><p><strong>address:</strong> Av. Paul Rivet 222, Quito 170517, Ecuador<br /><strong>time:</strong> t17:00 - 00:00 <br /><strong>price:</strong> $3<br /><strong>contact:</strong> phone: 59322254316 - email: latangueria@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-78.4824582,-0.1964899],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga del Mansi<br></strong><p><strong>address:</strong> Malecón Simón Bolivar 1406 y, Aguirre, Guayaquil 090307, Ecuador<br /><strong>time:</strong> 19:30 - 23:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: duvaltangoshow@gmail.com- website: http://www.pasiontango.net/milongas.aspx?ev=461</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-79.8802148,-2.1940212],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Belos Ayres</strong><p><strong>address:</strong> R. Francisco Deslandes, 18 - Anchieta, Belo Horizonte - MG, 30310-530, Brazil<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (31) 3287-7111 - email: imprensa.elabrazo@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.9301201,-19.9443308],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga do Milo</strong><p><strong>address:</strong> R. Tomé de Souza, 935 - Funcionários, Belo Horizonte - MG, 30140-130, Brazil<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> R$12,00<br /><strong>contact:</strong> phone: (31) 3262-3224 - email: milotango@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.935432,-19.9369987],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Rubrotango</strong><p><strong>address:</strong> R. Paula Gomes, 306 - São Francisco, Curitiba - PR, Brazil<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (41) 3082-3358 / 9915-0312 - email: rubrotango@rubrotango.com</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-49.2726553,-25.4248976],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Cumparsita</strong><p><strong>address:</strong> R. Paula Gomes, 306 - São Francisco, Curitiba - PR, Brazil<br /><strong>time:</strong> t20:30 - 23:30 <br /><strong>price:</strong> R$ 10,00<br /><strong>contact:</strong> phone: 3224-89-39 - email: izabellavf@yahoo.com.br</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-49.2726553,-25.4248976],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Biertango</strong><p><strong>address:</strong> R. Paula Gomes, 306 - São Francisco, Curitiba - PR, Brazil<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> R$ 10,00<br /><strong>contact:</strong> phone: (41) 3082-3358</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-49.2726553,-25.4248976],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>ImperioTango</strong><p><strong>address:</strong> Alameda Dr. Muricy, 1111 - Centro, Curitiba - PR, 80010-120, Brazil<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> R$ 8<br /><strong>contact:</strong> phone: 3078-1804 / 3289-850</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-49.2744371,-25.4279904],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Milonga</strong><p><strong>address:</strong> R. Raquel Prado, 18 - Mercês, Curitiba - PR, 80510-360, Brazil<br /><strong>time:</strong> t21:00 - 23:15 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (41) 9927-8393 / 229-6127 - email: intertango@intertango.com.br</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-49.286104,-25.4217881],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Agytu's</strong><p><strong>address:</strong> Av. Borges de Medeiros, 701 - Leblon, Rio de Janeiro - RJ, 22410-010, Brazil<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 9923-0475 / 8132-3600 / 9665-4489</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.2153164,-22.9804203],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Las Morochas</strong><p><strong>address:</strong> Rua Djalma Ulrich, 154 - Copacabana, Rio de Janeiro - RJ, 22071-020, Brazil<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 9987-0189 / 9303-6161 - email: tango2004@globo.com</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.1909598,-22.978647],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Los Mareados</strong><p><strong>address:</strong> R. São Clemente, 409 - Botafogo, Rio de Janeiro - RJ, 22260-001, Brazil<br /><strong>time:</strong> 21:30 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 2286-2648 - email: tnaccar@yahoo.com.br</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.195915,-22.9524385],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Tango Gala</strong><p><strong>address:</strong> R. Barata Ribeiro, 271 - Copacabana, Rio de Janeiro - RJ, 22040-001, Brazil<br /><strong>time:</strong> 22:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 2547-0861</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.1823325,-22.966583],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Xangô</strong><p><strong>address:</strong> R. Cosme Velho, 599 - Cosme Velho, Rio de Janeiro - RJ, 22241-090, Brazil<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 2525-8473 - email: praujo@osite.com.br</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-43.1992204,-22.9402327],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tangos e Milongas</strong><p><strong>address:</strong> R. São Clemente, 155 - Botafogo, Rio de Janeiro - RJ, 22260-001, Brazil<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 8121-0938</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.1871107,-22.9495789],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Conectango</strong><p><strong>address:</strong> R. Barão de Mesquita, 482 - Tijuca, Rio de Janeiro - RJ, 20540-003, Brazil<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 2288-1173 / 9618-0734 - email: mv-tango@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.2417789,-22.923971],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Bardetango e Choros</strong><p><strong>address:</strong> Rua Pacheco Leão, 2038 - Jardim Botânico, Rio de Janeiro - RJ, Brazil<br /><strong>time:</strong> 16:00 - 20:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 2274-1429 - email: neyhomero@bardetango.com.br</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.2392448,-22.967509],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>EsTANGOstoso</strong><p><strong>address:</strong> R. do Catete, 112 - Glória, Rio de Janeiro - RJ, 22220-001, Brazil<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 2264-6044 / 24-3930</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.1769482,-22.9246521],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga del Domingo</strong><p><strong>address:</strong> Rua Conde de Baependi, 62 - Flamengo, Rio de Janeiro - RJ, 22231-140, Brazil<br /><strong>time:</strong> 19:00 - 23:00 <br /><strong>price:</strong> R$20,00<br /><strong>contact:</strong> phone: (005521) - 2556-7765 - email: auryapires@yahoo.com.br - website: http://www.bobcunhayauryapires.com/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-43.1797671,-22.9326149],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Trasnochando y Bailongo</strong><p><strong>address:</strong> R. Cosme Velho, 60 - Cosme Velho, Rio de Janeiro - RJ, 22241-090, Brazil<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 9987-0189 - email: mscayres@uol.com.br</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-43.19627,-22.9375868],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Baile Tango</strong><p><strong>address:</strong> Av. Venâncio Aires, 866 - Cidade Baixa, Porto Alegre - RS, 90040-191, Brazil<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (51) 3333-2044</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-51.2121254,-30.03854],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Noite Portenha</strong><p><strong>address:</strong> Av. Venâncio Aires, 866 - Cidade Baixa, Porto Alegre - RS, 90040-191, Brazil<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (51) 3333-2044</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-51.2121254,-30.03854],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Porteña</strong><p><strong>address:</strong> Av. Mal. Rondon, 1960, Cachoeirinha - RS, Brazil<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: (19) 3241-0937 - email: wagneraxerodrigues@hotmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-51.06799,-29.9145261],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Villa Porteña</strong><p><strong>address:</strong> R. Canário, 408 - Moema, São Paulo - SP, 04521-001, Brazil<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 3858-2783 / 7124.2374 - website: http://www.tangoepaixao.com/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-46.665612,-23.5995925],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tango B'aires</strong><p><strong>address:</strong> R. Dr. Amâncio de Carvalho, 23 - Vila Mariana, São Paulo - SP, Brazil<br /><strong>time:</strong> 22:00 - 03:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 5575-6646 / 9258 5270 - email: omarfortetango@hotmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-46.6491849,-23.5816142],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Café Piu-Piu</strong><p><strong>address:</strong> R. Treze de Maio, 1234 - Bela Vista, São Paulo - SP, Brazil<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 2258-8066 / 5521-7495 / 7228-8118 - email: contato@depuroguapos.com.br</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-46.6457475,-23.5641136],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tanghetto</strong><p><strong>address:</strong> R. Joaquim Floriano, 1063 - Itaim Bibi, São Paulo - SP, 04534-014, Brazil<br /><strong>time:</strong> 20:30 - 01:30 <br /><strong>price:</strong> R$ 25<br /><strong>contact:</strong> phone: 55-11-3289-8502 - email: moacirmcastilho@yahoo.com.br - website: http://www.dancata.art.br/</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-46.6802607,-23.5861706],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Milonga Shine</strong><p><strong>address:</strong> Yukarı Bahçelievler Mahallesi, 60. Sk., 06490 Çankaya/Ankara, Turkey<br /><strong>time:</strong> 21:30 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +90 312 213 20 89 - website: http://www.shinedans.com/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [32.8241636,39.9239588],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Evita</strong><p><strong>address:</strong> Fatih Sultan Mehmet Blv., Nilüfer/Bursa, Turkey<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> Free<br /><strong>contact:</strong> phone: 5068985151 - email: oscarpepe@yahoo.com - website: http://www.tangoevita.com/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [28.9783283,40.220865],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga del Levante</strong><p><strong>address:</strong> Pichincha 120, S2002 Rosario, Santa Fe, Argentina<br /><strong>time:</strong> 23:00 - 04:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: 0054341 4724390 - email: milongael_levante@yahoo.com</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-60.6609351,-32.9341013],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tango Brillante</strong><p><strong>address:</strong> 7300 MacArthur Blvd, Glen Echo, MD 20812, USA<br /><strong>time:</strong> 20:30-23:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 302-588-9053 - email: ciardo8130@gmail.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.1385602,38.9662327],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Jacqueline's Practica</strong><p><strong>address:</strong> 4710 N Chambliss St, Alexandria, VA 22312, USA<br /><strong>time:</strong> 16:30 - 19:30 <br /><strong>price:</strong> $2<br /><strong>contact:</strong> phone: 703-576-2463</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.1438876,38.8210503],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>O'Brien's Milonga</strong><p><strong>address:</strong> 113 Main St, Annapolis, MD 21401, USA<br /><strong>time:</strong> 18:30 - 21:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: www.annapolistango.com</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.4882196,38.9773258],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>guided practica</strong><p><strong>address:</strong> <br>29 S Front St, Baltimore, MD 21202, USA<br /><strong>time:</strong> 19:30 - 20:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: http://www.tangoestanoche.com/to-contact-us/ </p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.6045799,39.2891163],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Zandunga</strong><p><strong>address:</strong> <br>7800 Wisconsin Ave, Bethesda, MD 20814, USA<br /><strong>time:</strong> 21:15 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 571-309-8957 - email: rpyreddy@umich.edu - facebook: https://www.facebook.com/groups/113994638646040/</p>",
"business":"milonga",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0954769,38.98847],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Practica 873 Grace St</strong><p><strong>address:</strong> 873 Grace St, Herndon, VA 20170, USA<br /><strong>time:</strong> 19:00 - 21:30 <br /><strong>price:</strong> free<br /><strong>contact:</strong> phone: 703-464-6200 - email: nshimp@verizon.net</p>",
"business":"practica",
"day": "Monday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.3891875,38.9739111],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Practice</strong><p><strong>address:</strong> <br>26 W Market St, Leesburg, VA 20176, USA<br /><strong>time:</strong> 21:15 - 23:00 <br /><strong>price:</strong> $20<br /><strong>contact:</strong> facebook: https://www.facebook.com/DanceKingStudios/</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.56586,39.115954],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tuesday Practica @ DAC</strong><p><strong>address:</strong> 210 E Centre St, Baltimore, MD 21202, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 970-581-5653 - email: trowell@gmail.com</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.6118468,39.2970162],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tango Niche Practilonga</strong><p><strong>address:</strong> <br>1813 M St NW, Washington, DC 20036, USA<br /><strong>time:</strong> 20:30 - 23:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 703-981-5778</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0423689,38.9059083],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga de Los Amigos</strong><p><strong>address:</strong> <br>3518 Decatur Ave, Kensington, MD 20895, USA<br /><strong>time:</strong> 21:00 - 00:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: http://www.caminitoamigo.com/events/</p>",
"business":"milonga",
"day": "Wednesday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-77.0710843,39.0337568],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Eastern Market Tango Club</strong><p><strong>address:</strong> 225 7th St SE, Washington, DC 20003, USA<br /><strong>time:</strong> 21:00 - 00:30 <br /><strong>price:</strong> $12<br /><strong>contact:</strong> phone: (703) 946-8895 - email: spatz@tangoDC.com - facebook: https://www.facebook.com/groups/122428374626593/</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.9964821,38.8866126],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Eastern Market Tango Club</strong><p><strong>address:</strong> 225 7th St SE, Washington, DC 20003, USA<br /><strong>time:</strong> 22:00 - 02:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> phone: (703) 946-8895 - email: spatz@tangoDC.com - facebook: https://www.facebook.com/groups/122428374626593/</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-76.9964821,38.8866126],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 3107 N Charles St, Baltimore, MD 21218, USA<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: http://www.tangoestanoche.com/sched/ </p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.6170371,39.3263392],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tango Mercurio</strong><p><strong>address:</strong> <br>1111 14th St NW, Washington, DC 20005, USA<br /><strong>time:</strong> 21:00 - 22:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: info@tangomercurio.org - website: http://www.tangomercurio.org.</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0317299,38.9043019],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga de Luz General</strong><p><strong>address:</strong> <br>1615 14th St NW, Washington, DC 20009, USA<br /><strong>time:</strong> 21:30 - 01:00 <br /><strong>price:</strong> $15<br /><strong>contact:</strong> email: masha@tangomercurio.org - facebook: https://www.facebook.com/groups/189720131182132/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-77.0315286,38.9116307],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>MIX Practica</strong><p><strong>address:</strong> <br>8514 Veterans Hwy, Millersville, MD 21108, USA<br /><strong>time:</strong> 20:00 - 22:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> email: annapolistango@gmail.com - website: www.annapolistango.com</p>",
"business":"practica",
"day": "Friday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-76.6267404,39.0975624],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Maleva</strong><p><strong>address:</strong> <br>5207 Wisconsin Ave NW, Washington, DC 20015, USA<br /><strong>time:</strong> 21:00 - 02:00 <br /><strong>price:</strong> $16<br /><strong>contact:</strong> email: jvalencia_09@yahoo.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-77.083775,38.957541],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>El otro Beso</strong><p><strong>address:</strong> <br>811 Cathedral St, Baltimore, MD 21201, USA<br /><strong>time:</strong> 21:30 - 01:30 <br /><strong>price:</strong> $14<br /><strong>contact:</strong> website: www.TheOtherKiss.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-76.6170011,39.2992787],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>TEA practica</strong><p><strong>address:</strong> B, 5818 Seminary Rd, Bailey's Crossroads, VA 22041, USA<br /><strong>time:</strong> 15:30 - 17:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> website: https://foreverdancing.sites.zenplanner.com/calendar.cfm</p>",
"business":"practica",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-77.1271761,38.8471027],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La Divina</strong><p><strong>address:</strong> <br>B, 5818 Seminary Rd, Bailey's Crossroads, VA 22041, USA<br /><strong>time:</strong> 21:00 - 01:00 <br /><strong>price:</strong> $14<br /><strong>contact:</strong> phone: 703-824-3555 - website: https://foreverdancing.sites.zenplanner.com/calendar.cfm </p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-77.1271761,38.8471027],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Library Practica</strong><p><strong>address:</strong> <br>514 19th St NW, Washington, DC 20052, USA<br /><strong>time:</strong> 14:30 - 17:00 <br /><strong>price:</strong> free<br /><strong>contact:</strong> email: librarypractica@gmail.com - facebook: https://www.facebook.com/groups/140239088911/</p>",
"business":"practica",
"day": "Saturday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-77.0438744,38.8966801],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Danzarin</strong><p><strong>address:</strong> Thessalonikis 17, Athina 118 51, Greece<br /><strong>time:</strong> 20:00 - 01:00 <br /><strong>price:</strong> 5€<br /><strong>contact:</strong> phone: 6937106972 - email: carlos_dall@yahoo.com.ar - website: www.danzarin.net</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [23.7136105,37.9755774],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>La milonga de Siempre</strong><p><strong>address:</strong>Carrer de Pons i Gallarza, 22, 07004 Palma, Illes Balears, España<br /><strong>time:</strong> 22:00 - 01:30 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +34600401177 - email:tango-mallorca@hotmail.com - facebook:/tangomallorcamilongas - website: www.tango-mallorca.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [2.6596547,39.5817325],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 1014 S Gilbert St, Iowa City, IA 52240, USA<br /><strong>time:</strong> 20:00 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 319-338-1723</p>",
"business":"practica",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-91.5315404,41.6487873],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tango in Ames</strong><p><strong>address:</strong> Memorial Union, Ames, IA 50011, USA<br /><strong>time:</strong> 17:00 - 19:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> email: v@vjw.biz</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-93.6459454,42.0236502],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 28 S Linn St, Iowa City, IA 52240, USA<br /><strong>time:</strong> 21:15 - 22:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 319-400-0423</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-91.5313564,41.6604033],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Cedar Rapids</strong><p><strong>address:</strong> 260 33rd Ave SW, Cedar Rapids, IA 52404, USA<br /><strong>time:</strong> 20:00 - 21:30 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 585.978.0909 - email: patriciavahanian@yahoo.com</p>",
"business":"practica",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-91.6680777,41.9455784],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Iowa City - Milonga</strong><p><strong>address:</strong> 28 S Linn St, Iowa City, IA 52240, USA<br /><strong>time:</strong> 20:00 - 23:00 <br /><strong>price:</strong> $5<br /><strong>contact:</strong> phone: 319-447-1445 - email: kljedgewood@gmail.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-91.5313564,41.6604033],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Cedar Rapids - Milonga</strong><p><strong>address:</strong> 260 33rd Ave SW, Cedar Rapids, IA 52404 USA<br /><strong>time:</strong> 19:30 - 00:30 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 585.978.0909 - email: patriciavahanian@yahoo.com</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-91.6680777,41.9455784],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Infinity Milonga</strong><p><strong>address:</strong> 5636 Johnson Dr, Mission, KS 66202, USA<br /><strong>time:</strong> 20:00 - 00:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 913-701-4540 - email: prbtango@gmail.com - facebook: https://www.facebook.com/InfinitybyPrbTango</p>",
"business":"milonga",
"day": "Saturday",
"frequency": "once a month"
},
"geometry": {
"coordinates": [-94.6511295,39.0224945],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>3P Practica</strong><p><strong>address:</strong> 5636 Johnson Dr, Mission, KS 66202, USA<br /><strong>time:</strong> 18:00 - 20:00 <br /><strong>price:</strong> $10<br /><strong>contact:</strong> phone: 913-701-4540 - email: prbtango@gmail.com - facebook: https://www.facebook.com/InfinitybyPrbTango</p>",
"business":"practica",
"day": "Saturday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [-94.6511295,39.0224945],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>En tus Brazos</strong><p><strong>address:</strong> Pila Seca 55, Centro, Zona Centro, 37700 San Miguel de Allende, Gto., Mexico<br /><strong>time:</strong> 17:30 - 20:30 <br /><strong>price:</strong> $50 MXN<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Thursday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-100.7491144,20.9130993],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Fat Boy Bar & Grill - Milonga</strong><p><strong>address:</strong> 5 de Mayo 5, Centro, Allende, 37760 San Miguel de Allende, Gto., Mexico<br /><strong>time:</strong> 18:00 - 21:00 <br /><strong>price:</strong> $50 pesos<br /><strong>contact:</strong> n/a</p>",
"business":"milonga",
"day": "Sunday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-100.7487573,20.9037456],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Practica</strong><p><strong>address:</strong> 413 Watling St, Radlett WD7 7JG, UK<br /><strong>time:</strong> 19:15 - 22:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> n/a</p>",
"business":"practica",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [-0.3158295,51.6812985],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga</strong><p><strong>address:</strong> Landstraße 31, 4020 Linz, Austria<br /><strong>time:</strong> 20:30 - 23:00 <br /><strong>price:</strong> 32<br /><strong>contact:</strong> website: http://www.tangolinz.at/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [14.2899403,48.302223],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Milonga Club Corazón</strong><p><strong>address:</strong> Heilhamer Weg 2a, 4040 Linz, Austria<br /><strong>time:</strong> 21:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> website: http://www.tangolinz-corazon.at/</p>",
"business":"milonga",
"day": "Tuesday",
"frequency": "once a week"
},
"geometry": {
"coordinates": [14.2941684,48.3193583],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"description": "<strong>Tango Atelier</strong><p><strong>address:</strong> Eschelberg 1, 4112 St. Gotthard im Mühlkreis, Austria<br /><strong>time:</strong> 16:00 - 23:00 <br /><strong>price:</strong> n/a<br /><strong>contact:</strong> phone: +43 (0)7234 87 306 - website: http://tango.eschelberg.net/</p>",
"business":"milonga",
"day": "Friday",
"frequency": "twice a month"
},
"geometry": {
"coordinates": [14.118303,48.3815498],
"type": "Point"
}
},

{
"name": "Feature",
"properties": {
"business":"event",
"eventYear": "2018",
"eventImage": "AdrianayD_flyer.jpg",
"eventName": "Tenerife Tango meeting 2018",
"eventInfo": "May 31 - June 1-2-3 y 5",
"eventAddress":"Santa Cruz de Tenerife, Spain",
"eventContact": "Phone: +34 657 20 47 53 - Facebook: https://fr-fr.facebook.com/people/Sylvain-Tango/100022866621936",
"eventContactWebsite": "https://www.tenerifetangomeeting.com/es/venues/",
"eventContactMail": "info@tenerifetangomeeting.com"
},
"geometry": {
"coordinates": [-16.2518467,28.4636296],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"business":"event",
"eventYear": "2018",
"eventImage": "ComunidadValenciana.jpg",
"eventName": "XXII Encuentro Nacional de Asociaciones de Tango",
"eventInfo": "May 31 to June 03",
"eventAddress":"Carrer Mestre Serrano, 3, 46120 Alboraia, Valencia, España",
"eventContact": "n/a",
"eventContactWebsite": "n/a",
"eventContactMail": "n/a"
},
"geometry": {
"coordinates": [-0.353535,39.495677],
"type": "Point"
}
},
{
"name": "Feature",
"properties": {
"business":"event",
"eventYear": "2018",
"eventImage": "tango_cartagena.jpg",
"eventName": "Encuentro de Tango Primavera 2018 - Cambalache",
"eventInfo": "11, 12 y 13 de Mayo",
"eventAddress":"Carrer Mestre Serrano, 3, 46120 Alboraia, Valencia, España",
"eventContact": "Phone: 639613502, 660383113, 661421483",
"eventContactWebsite": "n/a",
"eventContactMail": "cambalachetangocaragena@gmail.com"
},
"geometry": {
"coordinates": [-0.9965839,37.6256827],
"type": "Point"
}
}]
};
    res.send(result);
});

const server = app.listen(9090, function() {
  console.log('Server running at http://127.0.0.1:9090/');
});